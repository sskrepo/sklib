<?php

/*This class is depenedent on simple_form_html from http://simplehtmldom.sourceforge.net/
 *********
 * Usage *
 *********
 <?php
include ('/var/www/otestus/lib/form.php');//this file
include ('/var/www/otestus/lib/simple_html_dom.php');
include ('/var/www/otestus/lib/safeinput.php');

$form = new sk_form;
$form->loadForm('/var/www/otestus/file2.html'); Loads entire HTML file and loads validatecrit validation criteria(see below for exampl validatecrit div)

if(isset($_REQUEST['from_form'])) //any input field from your form which can tell you if form is submitted or form is being loaded for the first time
$form->validate(); //validates form using conditions mentioned in $validatecrit div in the same HTML, see below for HTML example
//For elements for which validation fails, validate funciton adds, below HTML to that element
$t->style = "border:solid 2px orange";
$t->outertext .= "<label for='".$t->name."'><font color='red'>".$error.";</font></label>";

You can alter it according to your requirement, on how to display elements with failed validation.


if($form->validate_error_count != 0) //if errors are there display form(this will automatically show errors against each field which failed validation as per validatecrit conditions mentioned in html form being loaded(see example below)
$form->display(); //echo entire Form HTML with errors if any;
else
echo 'Form validation passed';


?>
 * 
and HTML File(file2.html) will be in below format
 * 
 * Any HTML content with forms and with below div element with id "validatecrit" having validation criteria in comments section of that div
 * *Note this div will be completely removed while displaying the form for users, so no problem in placing it in the HTML file
 *         <div id="validatecrit">
            <!--
            PAN_NUM|isalphanum:Only Alphanumerics Please !!,isnotempty:This is Mandatory field;
            PAN_EMAIL|isemail:Please enter a valid email address,isnotempty:This is Mandatory field;
            PAN_TEL_NUM|isphone#+CC#10:Please enter valid phone number in +91XXXXXXXXX format;
            PAN_RO1_ZIP|iszipcode#IN:Please enter valid India Zipcode,isnotempty:This is Mandatory field;
            TESTPAN_NUM|isalphanum:Only Alphanumerics Please !!,isnotempty:This is Mandatory field;
            -->
        </div>
 * Syntax is ELEMENTNAME|Cirteria1:Error1,Criteria2:Error2,Criteria3:Error3;
 * size,seccode etc are the element names (not ids) as displayed in form.
 * 
 * For more syntax and criteria format, check safeinput.php
 */
namespace sskrepo\sklib\form;
/*Below constants are for simple_html_dom to work, as defined in 
Website: http://sourceforge.net/projects/simplehtmldom/
 * Additional projects that may be used: http://sourceforge.net/projects/debugobject/
 * Acknowledge: Jose Solorzano (https://sourceforge.net/projects/php-html/)
 *  */
define('HDOM_TYPE_ELEMENT', 1);
define('HDOM_TYPE_COMMENT', 2);
define('HDOM_TYPE_TEXT',	3);
define('HDOM_TYPE_ENDTAG',  4);
define('HDOM_TYPE_ROOT',	5);
define('HDOM_TYPE_UNKNOWN', 6);
define('HDOM_QUOTE_DOUBLE', 0);
define('HDOM_QUOTE_SINGLE', 1);
define('HDOM_QUOTE_NO',	 3);
define('HDOM_INFO_BEGIN',   0);
define('HDOM_INFO_END',	 1);
define('HDOM_INFO_QUOTE',   2);
define('HDOM_INFO_SPACE',   3);
define('HDOM_INFO_TEXT',	4);
define('HDOM_INFO_INNER',   5);
define('HDOM_INFO_OUTER',   6);
define('HDOM_INFO_ENDSPACE',7);
define('DEFAULT_TARGET_CHARSET', 'UTF-8');
define('DEFAULT_BR_TEXT', "\r\n");
define('DEFAULT_SPAN_TEXT', " ");
define('MAX_FILE_SIZE', 600000);
class form
{
    private $data;
    private $uniq_input_elements = array();
    private $request;
    private $fulldata = array();
    private $inputelementlist = array("input","textarea","select");
    private $safeinput;
    public $validation_json; //this will have validation json from <validatecrit> comment section,to be used when adding javascript validation to form.
    public $validation_json_array; //parsed $this->validation_json data in the form of array
    public $js_validation = 'FALSE';//Made true by add_js_validation() and used by display() , so that it wont remove validatecrit from HTML
    public $manual_error_count = 0;//Used by add_error_to_element
    public $validate_error_count = 0;//Used by validate()/validate_element();
    public $error_count = 0;//Sum of manual_error_count and validate_error_count;
    public $currentform; //Form for which current data is submitted.
    public $formevents_validation_array;//contains only onsubmit selector/criteria
    public $is_json_valid;//0 if json is invalid or no json at all, 1 if valid json
    public $element_event_validation_array;//contains element specific validation array
    
    
    public function __construct()
    {
        $this->safeinput = new \sskrepo\sklib\safeinput;
    }

    public function __get($varName)
    {

      if (!array_key_exists($varName,$this->data)){
          //this attribute is not defined!
          //do nothing
      }
      else return $this->data[$varName];

    }

    public function __set($varName,$value)
    {
      $this->data[$varName] = $value;
    }


    public function loadForm($formfilepath,$view = NULL,$load='withdata') //$view will take a view object supposed to have all values referenced in html form
    {
        //$load can take two values ,withdata or withoutdata, if withoutdata it will just load the form without any data from $_REQUEST
        //form html file may contain php code for action etc, so we need to execute form content so that php code gets executed first
        //and then load the form, becuase then it will be only html and text
        $form_after_php = $this->getscriptoutput($formfilepath,$view);
        $this->validate_error_count = '-1'; //to differentiate if we validated a form or not, if validate was called then this will be set to 0 and remains that way if validation pass,  and if validate has errors this will be 1 or more
        /*Load HTML*/
        $this->domobj = $this->str_get_html($form_after_php);
        
        $this->add_from_form_id();
       
        if($load == 'withdata')//this is default, user has to explicitly set load to withoutdata for not loading data as below
        {
            $request = $_REQUEST;

            /******************************************************/
            //Make a list of Unique input element names in HTML Forms, so that we can use that list to fill the form with user submitted data
            $this->uniq_input_elements = array();
            if(isset($_REQUEST['from_form-id-1824']))
            {
                $formid = explode("form-id-1824",$_REQUEST['from_form-id-1824']);
                $formid = $formid[0];
                $forms = $this->domobj->find('form');
                foreach($forms as $form)
                {
                   if($form->id == $formid)
                   {
                       $this->currentform = $form;
                
                        foreach ($this->inputelementlist as $ielement)
                        {
                            $inputelements = $this->currentform->find($ielement);
                            //we store element names as $this->uniq_input_elements['elementname'] = 0 or 1, 0 if not array 1 if array
                            foreach ($inputelements as $t)
                            {
                                if(isset($t->name))
                                {
                                    $el = explode('[',$t->name);
                                    if(count($el) == 1) //not array and there is no [ in element name
                                    {
                                        $this->uniq_input_elements[$el[0]]['isarray'] = 0;
                                        $this->uniq_input_elements[$el[0]][$t->name] = 1;
                                        // when request is not set(form loading first time), and if input element is having value attriute set, then we should not override it, if $val is NULL because REQUEST is not set yet.
                                        $val = isset($_REQUEST[$t->name])?$_REQUEST[$t->name]:NULL;

                                        if($ielement == 'input')
                                        {   

                                            if($t->type == 'checkbox' or $t->type == 'radio')  //if it is checkbox
                                            {
                                                //for checkbox value is always hardcoded in HTML without any submit. So we should not overwrite it, and also for checkbox if submitted value == existing value then we should set "checked" attribute to "checked"
                                                if($t->value == $val)
                                                $t->checked = "checked";

                                            }
                                            else 
                                            $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);

                                        }
                                        if($ielement == 'textarea')//if it is textarea, textarea element is not having value attribute so have to add it to innertext
                                        {                       
                                            $t->innertext .= isset($val)?$val:NULL;//no htmlspecchars needed as even if you have <html> inside textarea tag browser show <html> only it wont consider it as <html>
                                        }
                                        if($ielement == 'select')
                                        {   //for select elements $_REQUEST['selectelementname'] is coming from <option> elements inside it.
                                            //So if we want to preserve which option user selected from drop down, then we need to set "selected" attribute of that option element to "selected"(.i.e. <option selected="selected>)
                                            //if <select> element name is dept and we got $_REQUEST['dept'] as 2, then we need to find <option value=2> and make it <option value=2 selected="selected">
                                            $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                            $selectvalue = $t->value; 
                                            foreach($t->find('option') as $t1)
                                            {
                                                $inner = $t1->innertext;
                                                $optionvalue = $t1->value; 

                                                if($optionvalue == $selectvalue)
                                                {
                                                   $t1->selected = 'selected';
                                                }
                                            }
                                        }

                                    }

                                    if(count($el) > 1) // there is [ in element name
                                    {
                                        $this->uniq_input_elements[$el[0]]['isarray'] = 1;
                                        $this->uniq_input_elements[$el[0]][$t->name] = 1;

                                        $elementname = $el[0];
                                        $arraypart = preg_match("/(\[.*)$/",$t->name,$match);
                                        $arraypart = $match[0];
                                        $elarr = explode('[]',$arraypart);

                                        if(count($elarr) > 1) //atleast one blank braces are there in element name
                                        {   

                                            if(!isset($this->uniq_input_elements[$el[0]]['count']))
                                            $this->uniq_input_elements[$el[0]]['count'] = 0;

                                            $i = $this->uniq_input_elements[$el[0]]['count'];


                                            $j = count($elarr)-1;
                                            $count = 0;
                                            $postnamearray = NULL;
                                            while($count <= $j)
                                            {   
                                                if($count == 0)
                                                $index = $i;
                                                else
                                                $index = 0;

                                                if($count != $j)
                                                $postnamearray .= $elarr[$count].'['.$index.']';
                                                else
                                                $postnamearray .= $elarr[$count];
                                                $count++;

                                            }

                                            $reqstring = '['.$el[0].']'.$postnamearray;
                                            preg_match_all("/\[(.*?)\]/",$reqstring,$matchs);
                                            $val = $this->get_array_value($_REQUEST,$matchs[1]);

                                            if($ielement == 'input')
                                            {   

                                                if($t->type == 'checkbox' or $t->type == 'radio') //if it is checkbox
                                                {
                                                    //for checkbox value is always hardcoded in HTML without any submit. So we should not overwrite it, and also for checkbox if submitted value == existing value then we should set "checked" attribute to "checked"
                                                    if($t->value == $val)
                                                    {
                                                    $t->checked = "checked";
                                                    $this->uniq_input_elements[$el[0]]['count']++;//increment count only if checkbox is actually ticked. Only ticked checkboxes are submitted unlike other input types
                                                    //for example if you have following checkboxes in html file
                                                    /*
                                                     * <li>JavaScript: <input type="checkbox" name="checkin[]" id="javascript" value="javascript" /></li>
                                                        <li>CSS: <input type="checkbox" name="checkin[]" id="css" value="css"/></li>
                                                        <li>PHP: <input type="checkbox" name="checkin[]" id="php" value="php"/></li>
                                                        <li>Ruby: <input type="checkbox" name="checkin[]" id="ruby" value="ruby"/></li>
                                                        <li>Python: <input type="checkbox" name="checkin[]" id="python" value="python"/></li>

                                                     *  in this case when user checks say javascript and php only and submits
                                                     * then submit string will be checkin[]=javascript&checkin[]=php
                                                     * so $_REQUEST will be
                                                     * $_REQUEST[0] = 'javascript' and $_REQUEST[1]='php';
                                                     * For other input types like text all of them gets submitted with empty values
                                                     * if it is a text type then submit string will be
                                                     * $_REQUEST[0]='javascript',$_REQUEST[1]='', $_REQUEST[2]='php',$_REQUEST[3]=''
                                                     * 
                                                     * So in checkbox case count should be incremented only when checkbox is submitted.
                                                     * otherwise we will endup checking wrong boxes in output after loading and displaying                                */
                                                    }

                                                }
                                                else
                                                {
                                                    $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                    $this->uniq_input_elements[$el[0]]['count']++;                               
                                                }

                                            }   
                                            if($ielement == 'textarea')//if it is textarea, textarea element is not having value attribute so have to add it to innertext
                                            {                       
                                                $t->innertext .= isset($val)?$val:NULL;//no htmlspecchars needed as even if you have <html> inside textarea tag browser show <html> only it wont consider it as <html>
                                                $this->uniq_input_elements[$el[0]]['count']++;

                                            }
                                            if($ielement == 'select')
                                            {   //for select elements $_REQUEST['selectelementname'] is coming from <option> elements inside it.
                                                //So if we want to preserve which option user selected from drop down, then we need to set "selected" attribute of that option element to "selected"(.i.e. <option selected="selected>)
                                                //if <select> element name is dept and we got $_REQUEST['dept'] as 2, then we need to find <option value=2> and make it <option value=2 selected="selected">
                                                $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                $selectvalue = $t->value; 
                                                foreach($t->find('option') as $t1)
                                                {
                                                    $inner = $t1->innertext;
                                                    $optionvalue = $t1->value; 

                                                    if($optionvalue == $selectvalue)
                                                    {
                                                       $t1->selected = 'selected';
                                                    }
                                                }
                                                $this->uniq_input_elements[$el[0]]['count']++;
                                            }





                                        }
                                        else  //element name is having [ but no [] means we can get POST/GET value exactly using its name
                                        {
                                            $requestname = '['.$elementname.']'.trim($arraypart);

                                            preg_match_all("/\[(.*?)\]/",$requestname,$matchs);
                                            $val = $this->get_array_value($_REQUEST,$matchs[1]);

                                            if($ielement == 'input')
                                            {   

                                                if($t->type == 'checkbox' or $t->type == 'radio') //if it is checkbox
                                                {
                                                    //for checkbox value is always hardcoded in HTML without any submit. So we should not overwrite it, and also for checkbox if submitted value == existing value then we should set "checked" attribute to "checked"
                                                    if($t->value == $val)
                                                    {
                                                    $t->checked = "checked";
                                                    preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                    $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                    $this->uniq_input_elements[$el[0]]['count']++;;//increment count only if checkbox is actually ticked. Only ticked checkboxes are submitted unlike other input types
                                                    //for example if you have following checkboxes in html file
                                                    /*
                                                     * <li>JavaScript: <input type="checkbox" name="checkin[]" id="javascript" value="javascript" /></li>
                                                        <li>CSS: <input type="checkbox" name="checkin[]" id="css" value="css"/></li>
                                                        <li>PHP: <input type="checkbox" name="checkin[]" id="php" value="php"/></li>
                                                        <li>Ruby: <input type="checkbox" name="checkin[]" id="ruby" value="ruby"/></li>
                                                        <li>Python: <input type="checkbox" name="checkin[]" id="python" value="python"/></li>

                                                     *  in this case when user checks say javascript and php only and submits
                                                     * then submit string will be checkin[]=javascript&checkin[]=php
                                                     * so $_REQUEST will be
                                                     * $_REQUEST[0] = 'javascript' and $_REQUEST[1]='php';
                                                     * For other input types like text all of them gets submitted with empty values
                                                     * if it is a text type then submit string will be
                                                     * $_REQUEST[0]='javascript',$_REQUEST[1]='', $_REQUEST[2]='php',$_REQUEST[3]=''
                                                     * 
                                                     * So in checkbox case count should be incremented only when checkbox is submitted.
                                                     * otherwise we will endup checking wrong boxes in output after loading and displaying                                */
                                                    }
                                                }
                                                else
                                                {
                                                    $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                    preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                    $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                    $this->uniq_input_elements[$el[0]]['count']++;                             
                                                }

                                            }  
                                            if($ielement == 'textarea')//if it is textarea, textarea element is not having value attribute so have to add it to innertext
                                            {                       
                                                $t->innertext .= isset($val)?$val:NULL;//no htmlspecchars needed as even if you have <html> inside textarea tag browser show <html> only it wont consider it as <html>
                                                preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                $this->uniq_input_elements[$el[0]]['count']++;  

                                            }
                                            if($ielement == 'select')
                                            {   //for select elements $_REQUEST['selectelementname'] is coming from <option> elements inside it.
                                                //So if we want to preserve which option user selected from drop down, then we need to set "selected" attribute of that option element to "selected"(.i.e. <option selected="selected>)
                                                //if <select> element name is dept and we got $_REQUEST['dept'] as 2, then we need to find <option value=2> and make it <option value=2 selected="selected">
                                                $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                $selectvalue = $t->value; 
                                                foreach($t->find('option') as $t1)
                                                {
                                                    $inner = $t1->innertext;
                                                    $optionvalue = $t1->value; 

                                                    if($optionvalue == $selectvalue)
                                                    {
                                                       $t1->selected = 'selected';
                                                    }
                                                }
                                                preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                $this->uniq_input_elements[$el[0]]['count']++;  
                                            }

                                        }
                                    }
                               }
                            }
                        }
                     }
                }
                
            }
            //Validation criteria will be in the same HTML file with div id='validatecrit' element as a Comment <!-- -->

            /*********************Load Validate Criteria**********************************/

                $validatecrit = $this->domobj->find('#validatecrit');
                if(isset($validatecrit))
                {
                    $this->validation_json = $this->get_validation_criteria($validatecrit);
                    //parse json
                    if(isset($this->validation_json))
                    {
                        
                        $this->validation_json_array = json_decode($this->validation_json, true);     
                        //only if valid json
                        if(json_last_error() == JSON_ERROR_NONE and isset($this->validation_json_array) and is_array($this->validation_json_array))
                        {
                            $this->is_json_valid = 1;//valid json
                            //load only onsubmit selector/condition criteria
                            foreach($this->validation_json_array as $formid => $val )
                            {
                                $selector_array = $this->validation_json_array[$formid];

                                foreach($selector_array as $selector => $condition_array)
                                {
                                   foreach($condition_array as $condition => $cond_array)
                                   {
                                        if(isset($cond_array['events']))
                                        {
                                            $selector_events = explode(',',trim($cond_array['events']));
                                            if(isset($selector_events))
                                            {

                                                foreach($selector_events as $sevent => $vale)
                                                {   //remove any uneccessary spaces
                                                    $selector_events[$sevent] = trim($selector_events[$sevent]);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //No events specifies for a condition, so Use default events as onchange and onsubmit
                                            $selector_events = array('onchange','onsubmit');
                                        }
                                        foreach($selector_events as $event)
                                        {
                                            if($event == 'onsubmit')
                                            {

                                                $this->formevents_validation_array[$formid][$selector][$condition] = $cond_array;        

                                            }
                                        }
                                   }
                                }                                                   
                            }
                            if(isset($this->currentform))//only if a form is submitted, then load element specific events for elements in that form/used in validate_element for "required" condition
                            $this->prepare_element_event_validation_array();//generate event specific validation array
                        }
                        else//validate crit is set but invalid json
                        {
                            $this->is_json_valid = 0;
                            throw new \Exception('Invalid JSON specified for validation criteria - JSON validation failed with :'.$this->json_last_error_msg().' when loading HTML:'.$formfilepath);
                        }
                    }
                    else//invalid json or no json at all
                    $this->is_json_valid = NULL;
                
                    
                }
                



                 /* foreach ($validatecrit as $crit)
                    {
                        $crit = trim($crit);
                        if(!empty($crit))
                        {
                        $crit = explode("|",$crit);
                        $elementname = $crit[0];
                        $cond = $crit[1];
                        $cond = explode(",",$cond);

                        $validatearray[$elementname]['cond'] = $cond;

                        }
                    }

                    $this->validatearray = $validatearray;
                    //remove vaidatecrit comments from HTML page so that it wont be part of HTML sent to user(even though it is a comment and anyway not displayed even if exists)
                    foreach($this->domobj->find('#validatecrit') as $t)
                    {
                        $t->outertext = '';
                    }
                }*/

        }
    }
    
    public function get_array_value($array,$keys)
    {
        $key = array_shift($keys);
        while(isset($key))
        {
            $array = isset($array[$key])?$array[$key]:NULL;
            $key = array_shift($keys);
        }
        return isset($array)?$array:NULL;
    }
    public function validate()
    {
        $errormessage_stack = array();
        $this->validate_error_count=0;
        if(isset($this->formevents_validation_array) and isset($this->currentform) and $this->is_json_valid)
        {
            foreach($this->formevents_validation_array as $formid => $form_validation_array)
            {
                if($formid == $this->currentform->id)
                {
                    foreach($form_validation_array as $selector => $cond_array)
                    {
                        $elements = $this->currentform->find($selector);
                        
                        foreach($elements as $domelement)
                        {
                            
                            foreach($cond_array as $op => $cond)
                            {
                                $eachcond['op'] = $op;
                                $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                                $err = isset($cond['error'])?$cond['error']:NULL;           
                                
                                $result = $this->validate_element($domelement,$eachcond,$err);
                                $result_status = $result['status'];
                                if(isset($result['errormessage']))
                                $errormessage = $result['errormessage'];
                                
                                if(!$result_status)
                                {   
                                    if(isset($errormessage_stack[$domelement->id]))
                                    $errormessage_stack[$domelement->id] = $errormessage_stack[$domelement->id].' || '.$errormessage;
                                    else
                                    $errormessage_stack[$domelement->id] = $errormessage;
                                }
                            }
                            
                        }
                    }
                    if(is_array($errormessage_stack))
                    {
                        foreach($errormessage_stack as $elementid => $errormessage)
                        {
                            $ele = $this->domobj->find('#'.$elementid);
                            foreach($ele as $e)//find always returns a array even if find with id;
                            $this->add_error_message($e,$errormessage);
                        }
                    }
                }
            }
        $this->error_count = $this->manual_error_count + $this->validate_error_count;
        if($this->validate_error_count)
        return 0;//validation failed
        else
        return 1;
        }
        else
        {
            if(is_null($this->is_json_valid))//there is no validatecrit at all
            return 1;//nothing to validate so returns 1;
            else
            return 0;//means $this->is_json_valid is 0 and it is due to invalid json
        }
        
        
    }
    public function validate_element($ielement,$cond,$error = null)
    {
        if($ielement->tag != 'textarea')
        $val = $ielement->value;
        else
        $val = $ielement->innertext;
        
        $arg = isset($cond['arg'])?$cond['arg']:NULL;
        
        if($cond['op'] == 'compare_element')//to compare two elements
        {
            if(isset($arg['elementid']) and $arg['operator'])
            {
                $el_search_string = '#'.$arg['elementid'];
                $elem = $this->currentform->find($el_search_string);
                foreach($elem as $e)
                {
                    if($e->tag != 'textarea')
                    $operand2 = $e->value;
                    else
                    $operand2 = $e->innertext;
                }
            }
            elseif(isset($arg['elementname']) and $arg['operator'])
            {
                $el_search_string = '[name='.$arg['elementname'].']';
                $elem = $this->currentform->find($el_search_string);
                foreach($elem as $e)
                {
                    if($e->tag != 'textarea')
                    $operand2 = $e->value;
                    else
                    $operand2 = $e->innertext;
                }

            }
            $arg1['comparewith'] = $operand2;
            $arg1['operator'] = $arg['operator'];
            $validation_result = $this->safeinput->validate('compare',$val,$arg1,$error);
        }
        elseif($cond['op'] == 'required')
        {
            //handled required condition, used for dependency between elements, example, validate Y if X is valid or Xis checked or X is selected etc
            //
            /*input :
             *      valid
             *checkbox:
             *      checked
             *select:
             *      selected
             *textarea:
             *      valid
             *      
             * valid implementation
             *     validate the element without adding any errors and check if validation_result is true or false
             *      
             */
            if(isset($arg))
            {
                $req_elements_err = 0;
                $req_errorstack = null;
                foreach($arg as $req_element_id => $req_element_condition)
                {
                    
                    $curr_element_err = 0;
                    if(isset($req_element_id))
                    {
                            //condition can be valid, checked,selected
                            
                            $req_element = $this->currentform->find('#'.$req_element_id);
                            foreach($req_element as $e)
                            $req_element = $e;
                            if(!is_array($req_element_condition))//if not array 
                            {
                                    $req_arg =  explode('|',$req_element_condition);//arg format "elementid":"valid|errormessage if not valid"
                                    $req_arg_cond = trim($req_arg[0]);
                                    $req_errormessage = isset($req_arg[1])?$req_arg[1]:null;

                                    if(isset($req_element))//if element exists
                                    {
                                        if($req_arg_cond == 'valid' and $req_element->tag == 'input' )
                                        {
                                            foreach($this->element_event_validation_array[$this->currentform->id][$req_element_id] as $op => $cond)
                                            {
                                                $eachcond['op'] = $op;
                                                $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                                                $err = isset($cond['error'])?$cond['error']:'error';           

                                                $req_result = $this->validate_element($domelement,$eachcond,$err);
                                                if(!$req_result['status'])
                                                {
                                                    $curr_element_err++;
                                                    break;//break at first validation failure for a element, no need to check further as anyway it is invalid

                                                }

                                            }

                                        }
                                        elseif($req_arg_cond == 'checked') 
                                        {
                                            $req_element = $this->currentform->find('#'.$req_element_id);
                                            if(isset($req_element[0]))//above find return array[0] set to null if no element found
                                            {   
                                                foreach($req_element as $e)
                                                $req_element = $e;
                                                if($req_element->tag == 'input' and $req_element->type == 'checkbox')//if element itself is a checkbox
                                                {
                                                    if($req_element->checked != 'checked')
                                                    $curr_element_err++;//checkbox is not checked
                                                }                                           
                                                elseif($req_element->tag == 'fieldset')
                                                {   //if element is a fieldset which has a set of checkboxes inside it, atleast one checkbox should have been checked for this condition to be valid
                                                    $checkbox_array = $req_element->find('input[type=checkbox]');
                                                    $atleast_one_checkbox_checked = NULL;
                                                    if(isset($checkbox_array))
                                                    {
                                                        foreach($checkbox_array as $checkbox)
                                                        {
                                                             if($checkbox->checked == 'checked')
                                                             {
                                                                $atleast_one_checkbox_checked = TRUE;
                                                                break;//break at first checked checkbox
                                                             }

                                                        }

                                                    }
                                                    if($atleast_one_checkbox_checked != TRUE)
                                                    {
                                                        $curr_element_err++;
                                                    }

                                                }
                                            }
                                        }
                                        elseif($req_arg_cond == 'selected' and $req_element->tag == 'select')
                                        {   $option_is_selected = NULL;
                                            foreach($req_element->find('option') as $t1)
                                            {
                                                if($t1->selected = 'selected')
                                                {
                                                   $option_is_selected = TRUE;
                                                }
                                            }
                                            if($option_is_selected != TRUE)
                                            {
                                                $curr_element_err++;
                                            } 
                                        }
                                        else
                                        {
                                            //do nothing
                                        }

                                    }


                            }
                            else//if it is array
                            {
                                //each required element condition is again specified with multiple validations.
                                //required:
                                /*{
                                 *      "elementid_1":
                                 *      {
                                 *          "isint":
                                 *          {
                                 *              "error":"elementid_1 is not valid integer"                        *              
                                 *          }
                                 *      }

                                }*/
                                $req_element_validation_array = $req_element_condition;
                                if(isset($req_element_validation_array))
                                {
                                    foreach($req_element_validation_array as $op => $cond)
                                    {
                                        $eachcond['op'] = $op;
                                        $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                                        $err = isset($cond['error'])?$cond['error']:'error';           
                                        if($eachcond['op'] != 'required')//nested 'required's are not supported//just ignore nested required
                                        {
                                            $req_result = $this->validate_element($req_element,$eachcond,$err);
                                            if(!$req_result['status'])
                                            {
                                                $curr_element_err++;
                                                if(isset($req_errormessage))
                                                $req_errormessage = $req_errormessage.' || '.$req_result['errormessage'];
                                                else
                                                $req_errormessage = $req_result['errormessage'];
                                            }
                                        }
                                    }
                                }                        
                            }
                            if($curr_element_err != 0)
                            {
                                $req_elements_err++;
                                if(isset($req_errormessage) and $req_errorstack != NULL )
                                $req_errorstack = $req_errorstack.' || '.$req_errormessage;
                                elseif(isset($req_errormessage))
                                $req_errorstack = $req_errormessage;

                            }
                    }
                }
            }
            if($req_elements_err != 0)
            {
                $validation_result['status'] = FALSE;
                $validation_result['errormessage'] = $req_errorstack;
            }
            else
            {
                $validation_result['status'] = TRUE;
                
            }
        }
        else
        {   //append error label to the element and make border of the element to red if validation fails for that element
            $validation_result = $this->safeinput->validate($cond['op'],$val,$arg,$error);
        }
        if(!$validation_result['status'])
        {   //append error label to the element and make border of the element to red if validation fails for that element
            $this->validate_error_count++;
            //$ielement->style = "border:solid 1px red";
            //$ielement->outertext .= "<label for='".$ielement->name."'><font color='red' size=2>".$error." || </font></label>";           
        }
        return $validation_result;
        //returns validation_result array with 'status' and 'errormessage';
    }
    public function prepare_element_event_validation_array()
    {
        $errormessage_stack = array();
        $this->validate_error_count=0;
        $this->element_event_validation_array = array();
        if(isset($this->formevents_validation_array) and $this->is_json_valid)
        {
            foreach($this->formevents_validation_array as $formid => $form_validation_array)
            {
                foreach($form_validation_array as $selector => $cond_array)
                {
                    $elements = $this->currentform->find($selector);

                    foreach($elements as $domelement)
                    {
                        foreach($cond_array as $op => $cond)
                        {
                            $eachcond['op'] = $op;
                            $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                            $err = isset($cond['error'])?$cond['error']:'error';           

                            $this->element_event_validation_array[$formid][$domelement->id] = $cond_array;
                            
                        }
                    }
                }   
            }
        }
    }
    
    public function display()
    {
        //with or without javascript, we are not going to send validatecrit comment to client
        //if using javascript, we set cookie with validation json data, so that javascript can use it
        //if($this->js_validation != 'TRUE')
        //{
        foreach($this->domobj->find('#validatecrit') as $t)
        {
            $t->outertext = '';
        }
       //}
        echo $this->domobj;
    }
    public function file_get_html($url, $use_include_path = false, $context=null, $offset = -1, $maxLen=-1, $lowercase = true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT, $defaultSpanText=DEFAULT_SPAN_TEXT)
    {
        
	// We DO force the tags to be terminated.
	$dom = new \sskrepo\sklib\form\simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $stripRN, $defaultBRText, $defaultSpanText);
	// For sourceforge users: uncomment the next line and comment the retreive_url_contents line 2 lines down if it is not already done.
	$contents = file_get_contents($url, $use_include_path, $context, $offset);
	// Paperg - use our own mechanism for getting the contents as we want to control the timeout.
	//$contents = retrieve_url_contents($url);
	if (empty($contents) || strlen($contents) > MAX_FILE_SIZE)
	{
		return false;
	}
	// The second parameter can force the selectors to all be lowercase.
	$dom->load($contents, $lowercase, $stripRN);
	return $dom;
    }

    // get html dom from string
    public function str_get_html($str, $lowercase=true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT, $defaultSpanText=DEFAULT_SPAN_TEXT)
    {
            $dom = new \sskrepo\sklib\form\simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $stripRN, $defaultBRText, $defaultSpanText);
            if (empty($str) || strlen($str) > MAX_FILE_SIZE)
            {
                    $dom->clear();
                    return false;
            }
            $dom->load($str, $lowercase, $stripRN);
            return $dom;
    }

    // dump html dom tree
    public function dump_html_tree($node, $show_attr=true, $deep=0)
    {
            $node->dump($node);
    }
    public function getscriptoutput($path, $view)
    { 
        //phpscriptoutput_to_variable
        ob_start();

        if( is_readable($path) && $path )
        {
            include $path;
        }
        else
        {
            return FALSE;
        }

        
            return ob_get_clean();
    }
    public function add_element($parentelement,$elementcode)
    {
        //add $elementcode to $parentelement
    }
    
    public function get_validation_criteria($validate_div_element)
    {
        //takes validate_div_element and extracts validation json from comment section and returns the same
        foreach($validate_div_element as $vc)
        {
            $comm = $this->str_get_html($vc);
            $comm1 = $comm->find('comment');
        }
        if(isset($comm1))
        {
            //comment tags need to be stripped out and also they may have white space around so trim it
            foreach ($comm1 as $comm2)
            {
                $comm2 = str_replace('<!--','',$comm2->innertext);
                $comm2 = str_replace('-->','',$comm2);
                $comm2 = trim($comm2);
                //comment is in the form of <form element name>:<validation condition>:<error message incase validation fails>;
                //each line will have validation condition for each element. same element may have multiple validations too in multiple lines.

            }
            return $comm2;
        }
    }
    
    public function add_js_validation($form_js_path)
    {
        if($this->is_json_valid)      //only if json is valid
        {
        $headelement = $this->domobj->find('head');
        if(!empty($headelement))//Not proper HTML, as it is without <head> tag So we just need to add js include somewhere
        {
            $includejs = '<script src="http://code.jquery.com/jquery-1.11.3.js"></script>';
            $includejs .= '<script type="text/javascript" src="'.$form_js_path.'"></script>';
            
            foreach($headelement as $head)
            {
                 $head->innertext = $head->innertext.$includejs;
            }
        }
        else 
        {
            $forms = $this->domobj->find('form');
            foreach($forms as $someform)
            {}//just to get some randon form in the HTML , so that we can append <script> code next to it only if <head> is not available
            
            $includejs = '<script src="https://code.jquery.com/jquery-1.11.3.js"></script>';
            $includejs .= '<script type="text/javascript" src="'.$form_js_path.'"></script>';
            $someform->outertext = $someform->outertext.$includejs;//just adding js include next to last accessed Form element
            
            
        }
        $this->set_json_cookie($this->validation_json_array);
        $this->js_validation = 'TRUE'; //USed by display(), so that it wont remove validatecrit from HTML
            return 1;
    }
        else
        return 0;
    }
    public function find_forms($form_name_array)
    {
        //$form_name_array can be 'all' or a array of form names or a single form name
        //return a array of form elements
        if(is_array($form_name_array))
        {
            $i = 0;
            $forms = $this->domobj->find('form');
            foreach($forms as $form)
            {
                if(in_array($form->name,$form_name_array))
                {
                    //add event to form;

                    $form_elements[$i] = $form;
                    $i++;
                }

            }

        }
        elseif($form_name_array == 'all')
        {
            $form_elements = $this->domobj->find('form');

        }
        elseif(!empty($form_name_array))
        {
            $form_elements = $this->domobj->find('form');
           foreach($forms as $form)
            {
                if($form->name == $form_name_array)
                {
                    $form_elements[$i] = $form;

                }

            }
        }
        else
        {
            return 0;//no forms were provided to set the event
        }
        return $form_elements;

    }
    
    public function add_error_to_element($element,$errormessage,$element_type='name')//$type defaults to name , it can "id" as well
    {
        //adds error label to elements with given name
        //increments $this->manual_error_count 
        //$element_type can be 'name' or 'id', referes to if $element value to be considered as element name or element id
        if($element_type == 'name' or $element_type == 'id')
        {
            if($element_type == 'name')
            $el_search_string = '[name='.$element.']';
            elseif($element_type == 'id')
            $el_search_string = '#'.$element;

            $domelement = $this->domobj->find($el_search_string);
            foreach($domelement as $el)
            {
                $this->add_error_message($el,$errormessage);
                $this->manual_error_count++;
            }
        }
        elseif($element_type = 'element')
        {
            $this->add_error_message($element,$errormessage);
            $this->manual_error_count++;
        }
        
        return $this->manual_error_count;
        
    }
    public function add_error_message($domelement,$errormessage_stack)
    {
        
        $domelement->style = "border:solid 1px red";
        $domelement->outertext .= "<label id='formerror--label' for='".$domelement->name."'><font color='red' size=2> || ".$errormessage_stack."</font></label>";
        
    }
    public function add_from_form_id()
    {
        $forms = $this->domobj->find('form');
        foreach($forms as $form)
        {
            $inputel = '<input name="from_form-id-1824" id="from_form-id-1824" type="hidden" value="'.$form->id.'form-id-1824"/>';
            //Using $form->innertext will cause problems. when displaying form if innertext is modified once, it will take only inntertext 
            //for that element, for exmaple if we do $form->innertext = $form->innertext.$inputel, it will set INNER text modified flag in
            //form node, and when displaying form , if there is innertext set, it will not look for individual child node outertext.
            //so if we have modified any input element to have error label , that will not be considered as innertext of parent form.
            //so never use innertext if you plan to modify its child nodes.
            //$form->innertext = $form->innertext.$inputel;
            
            $i=0;
            foreach($form->nodes as $e) //find any one element inside form and add out input element next to it
            {
                if($i < 1)
                {
                    $e->outertext = $e->outertext.$inputel;
                    $i++;
                }
                else
                break;
            }
            
        }
    }
    public function set_json_cookie($json_array)
    {
        //cookie to set validatecrit json so that we dont have to send validatecrit comment to client, javascript can use the cookie to get validatecrit
        $json_string = json_encode($json_array);
        setcookie('validate_crit_json3',$json_string);
        //echo $json_string;
        
    }
    public function is_form_submitted()
    {
        if(isset($this->currentform))
        {
            return $this->currentform->id;
        }
        else
        return 0;
    }
    public function json_last_error_msg() {//json_last_error_msg available only from PHP 5.5. or later so this will help irrespective of PHP version
        static $errors = array(
            JSON_ERROR_NONE             => null,
            JSON_ERROR_DEPTH            => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH   => 'Underflow or the modes mismatch',
            JSON_ERROR_CTRL_CHAR        => 'Unexpected control character found',
            JSON_ERROR_SYNTAX           => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8             => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );
        $error = json_last_error();
        return array_key_exists($error, $errors) ? $errors[$error] : "Unknown error ({$error})";
    }

    /*public function test($formfilepath)
    {
        $form_after_php = $this->getscriptoutput($formfilepath);
        $this->validate_error_count = '-1'; //to differentiate if we validated a form or not, if validate was called then this will be set to 0 and remains that way if validation pass,  and if validate has errors this will be 1 or more
        //Load HTML
        $this->domobj = $this->str_get_html($form_after_php);
        $el_search_string = '[name=secode]';
        $el = $this->domobj->find($el_search_string);
        $tagname = $el->tag;
        
    }*/
    
        
}
?>