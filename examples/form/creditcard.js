jsvalidator.prototype.creditcard = (function()
{
    var my = {};    
    my.$result = new Array();
    my.$cardsarray = new Array();
    
    my.loadcard_list = function()
    {
       my.$cardsarray['American Express'] = {  
              'length' : '15', 
              'prefixes' : '34,37',
              'checkdigit' : true
            };
       my.$cardsarray['Diners Club Carte Blanche'] ={
              'length' : '14', 
              'prefixes' : '300,301,302,303,304,305',
              'checkdigit' : true
            };
       my.$cardsarray['Diners Club'] ={
              'length' : '14,16',
              'prefixes' : '36,38,54,55',
              'checkdigit' : true
            };
       my.$cardsarray['Discover'] ={ 
              'length' : '16', 
              'prefixes' : '6011,622,64,65',
              'checkdigit' : true
            };
       my.$cardsarray['Diners Club Enroute'] ={ 
              'length' : '15', 
              'prefixes' : '2014,2149',
              'checkdigit' : true
            };
       my.$cardsarray['JCB'] = { 
              'length' : '16', 
              'prefixes' : '35',
              'checkdigit' : true
            };
       my.$cardsarray['Maestro'] = {
              'length' : '12,13,14,15,16,18,19', 
              'prefixes' : '5018,5020,5038,6304,6759,6761,6762,6763',
              'checkdigit' : true
            };
       my.$cardsarray['MasterCard'] = {
              'length' : '16', 
              'prefixes' : '51,52,53,54,55',
              'checkdigit' : true
            };
       my.$cardsarray['Solo'] = { 
              'length' : '16,18,19', 
              'prefixes' : '6334,6767',
              'checkdigit' : true
            };
       my.$cardsarray['Switch'] = {
              'length' : '16,18,19', 
              'prefixes' : '4903,4905,4911,4936,564182,633110,6333,6759',
              'checkdigit' : true
            };
       my.$cardsarray['VISA'] = {
              'length' : '16', 
              'prefixes' : '4',
              'checkdigit' : true
            };
       my.$cardsarray['VISA Electron'] = {
              'length' : '16', 
              'prefixes' : '417500,4917,4913,4508,4844',
              'checkdigit' : true
            };
       my.$cardsarray['LaserCard'] = {
              'length' : '16,17,18,19', 
              'prefixes' : '6304,6706,6771,6709',
              'checkdigit' : true
          };
    };
    
    my.is_supported_cardtype = function($cardtype)
    {
          var $type = -1,$ctype;
          my.loadcard_list();
          for($ctype in my.$cardsarray ) 
          {
              var $cprops = my.$cardsarray[$ctype];
            // See if it is this card (ignoring the case of the string)
            if ($cardtype.toLowerCase() == $ctype.toLowerCase())
            {
              $type = $ctype;
              break;
            }
          }
          // If card type not found, report an error
          if ($type == -1) 
          return false;
          else
          return true;
    };
    
    my.check_modulus10 = function($cardnumber)
    {
        
        if(typeof $cardnumber !== 'undefined')
        {
            var $checksum = 0;                                  // running checksum total
            var $mychar = "";                                   // next char to process
            var $j = 1,$i;                                         // takes value of 1 or 2

            // Process each digit one by one starting at the right
            for ($i = ($cardnumber.length) - 1; $i >= 0; $i--) 
            {

              // Extract the next digit and multiply by 1 or 2 on alternative digits.      
              var $calc = Number($cardnumber.charAt($i)) * $j;

              // If the result is in two digits add 1 to the checksum total
              if ($calc > 9) {
                $checksum = $checksum + 1;
                $calc = $calc - 10;
              }

              // Add the units element to the checksum total
              $checksum = $checksum + $calc;

              // Switch the value of j
              if ($j ==1) {$j = 2;} else {$j = 1;};
            } 

            // All done - if checksum is divisible by 10, it is a valid modulus 10.
            // If not, report an error.
            if ($checksum % 10 != 0) {
             return false;
            }
            else
            return true;
        }
        else
        return false;

    };
    my.get_cardtype = function()
    {
        //Card specific Checks
        if(isset(my.$cardnumber))
        {
            if(!is_array(my.$cardsarray))
            {
                my.loadcard_list();
            }
            var $cardtype;
            for($cardtype in my.$cardsarray )
            {
                   // Load an array with the valid prefixes for this card
                   var $cardproperties = my.$cardsarray[$cardtype];
                  var $prefix = $cardproperties['prefixes'].split(',');

                  // Now see if any of them match what we have in the card number  
                  var $PrefixValid = false;
                  var $i;
                  for ($i=0; $i< $prefix.length ; $i++) {
                    var $exp = '/^'+$prefix[$i]+'/';
                    if (preg_match($exp,my.$cardnumber)) {
                      $PrefixValid = true;
                      break;
                    }
                  }

                  // If it isn't a valid prefix there's no point at looking at the length
                  if ($PrefixValid) 
                  {
                      // See if the length is valid for this card
                      var $LengthValid = false,$j;
                      var $lengths = $cardproperties['length'].split(',');
                      for ($j=0; $j < $lengths.length ; $j++) {
                        if (my.$cardnumber.length == $lengths[$j]) {
                          $LengthValid = true;
                          break;
                        }
                      }

                      // See if all is OK by seeing if the length was valid. 
                      if ($LengthValid) {
                         return $cardtype;
                      }; 
                  }    
            }
            return null;//Tried matching against all supported cardtypes but failed to match, so return null;
        }
        return null;
    };
    my.validate_card = function($cardnumber,$cardtype_to_match)
    {
        $cardtype_to_match = (typeof $cardtype_to_match !== 'undefined')?$cardtype_to_match:null;
        var $result = new Array();
        my.loadcard_list();
        if(my.numberOfKeysinObject(my.$cardsarray))//is array
        {
            if(typeof $cardtype_to_match !== 'undefined')//if card type to match is not provided, we match it with all possible cardtypes
            {
                if(!my.is_supported_cardtype($cardtype_to_match))//if cardtype_to_match is provided then it must be one of the supported ones
                {
                    $result['status'] = false;
                    $result['error'] = 'Card type give to match is not one of the Supported Card types';
                    $result['card_number'] = null;
                    $result['card_type'] = 'Invalid';
                    return $result;
                }
                my.$cardtype_to_match = $cardtype_to_match;
            }
            
            
            //Check for Numerics and min and max length of any card possible(13-19)
            if(typeof $cardnumber !== 'undefined')
            {
                
                // Remove any spaces or hyphens from the credit card number
                $cardnumber = $cardnumber.replace (/-/g, "");
                $cardnumber = $cardnumber.replace (/\s/g, "");
                
                var cardexp = /^[0-9]{13,19}$/;
                if (!cardexp.exec($cardnumber))
                {
                    $result['status'] = false;
                    $result['error'] = 'Card number provided is not having right length or is having non-numeric characters';
                    $result['card_number'] = my.$cardnumber;
                    $result['card_type'] = 'Invalid';
                    return $result;
                }
                my.$cardnumber = $cardnumber;
            }
            else
            {
                $result['status'] = false;
                $result['error'] = 'Card Number Cannot be empty';
                $result['card_number'] = null;
                $result['card_type'] = 'Invalid';
                return $result;
            }
            
            //Check for modulus10 of card is valid
            if(!my.check_modulus10(my.$cardnumber))
            {
                $result['status'] = false;
                $result['error'] = 'Cardnumber failed Modulus10 verfication';
                $result['card_number'] = my.$cardnumber;
                $result['card_type'] = 'Invalid';
                return $result;
            }
            else//passed modulus10 verification
            {
                var $cardtyp = my.get_cardtype();
                if($cardtyp == null)
                {
                    $result['status'] = false;
                    $result['error'] = 'Cardnumber doesnt match any supported cardtype';
                    $result['card_number'] = my.$cardnumber;
                    $result['card_type'] = 'Invalid';
                    return $result;
                }
                else
                {    
                    if(!my.hasOwnProperty($cardtype_to_match)) 
                    {
                        $result['status'] = true;
                        $result['error'] = null;                       
                    }
                    else if(my.$cardtype_to_match.toLowerCase() == $cardtyp.toLowerCase())
                    {
                        $result['status'] = true;
                        $result['error'] = null;
                    }
                    else
                    {
                        $result['status'] = false;
                        $result['error'] = 'Cardnumber is a valid creditcard number, but not the same type as '+my.$cardtype_to_match;                 
                    }
                    $result['card_number'] = my.$cardnumber;
                    $result['card_type'] = $cardtyp;
                    return $result;
                    
                }
                
            }
            
        }
    };
    
    my.numberOfKeysinObject = function(obj)//equivalent to array.length for associative arrays(arrays with strings as keys instead of numbers)
    {//http://stackoverflow.com/questions/126100/how-to-efficiently-count-the-number-of-keys-properties-of-an-object-in-javascrip
        if (!Object.keys) 
        {
            Object.keys = function (obj) {
                var keys = [],
                    k;
                for (k in obj) {
                    if (Object.prototype.hasOwnProperty.call(obj, k)) {
                        keys.push(k);
                    }
                }
                return keys;
            };
        }
        return Object.keys(obj).length;
    }
    
    
    return my;
}());


