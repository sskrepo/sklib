function form_js_validation(event,caller)
{
    var validation_criteria_string,validatecrit_array,form_error_count ;
    event.preventDefault();
    validation_criteria_string = "PAN_NUM|isalphanum:Only Alphanumerics Please !!,isnotempty:This is Mandatory field;PAN_EMAIL|isemail:Please enter a valid email address,isnotempty:This is Mandatory field;PAN_TEL_NUM|isphone#+CC#10:Please enter valid phone number in +91XXXXXXXXX format;PAN_RO1_ZIP|iszipcode#IN:Please enter valid India Zipcode,isnotempty:This is Mandatory field;TESTPAN_NUM|isalphanum:Only Alphanumerics Please !!,isnotempty:This is Mandatory field;TESTPAN_EMAIL|isemail:Please enter a valid email address,isnotempty:This is Mandatory field;TESTPAN_TEL_NUM|isphone#+CC#10:Please enter valid phone number in +91XXXXXXXXX format;TESTPAN_RO1_ZIP|iszipcode#IN:Please enter valid India Zipcode,isnotempty:This is Mandatory field;TEST1PAN_NUM|isalphanum:Only Alphanumerics Please !!,isnotempty:This is Mandatory field;TEST1PAN_EMAIL|isemail:Please enter a valid email address,isnotempty:This is Mandatory field;TEST1PAN_TEL_NUM|isphone#+CC#10:Please enter valid phone number in +91XXXXXXXXX format;TEST1PAN_RO1_ZIP|iszipcode#IN:Please enter valid India Zipcode,isnotempty:This is Mandatory field;TEST2PAN_NUM|isalphanum:Only Alphanumerics Please !!,isnotempty:This is Mandatory field;TEST2PAN_EMAIL|isemail:Please enter a valid email address,isnotempty:This is Mandatory field;TEST2PAN_TEL_NUM|isphone#+CC#10:Please enter valid phone number in +91XXXXXXXXX format;TEST2PAN_RO1_ZIP|iszipcode#IN:Please enter valid India Zipcode,isnotempty:This is Mandatory field;";
    //split validation crit string into array
    //
    validatecrit_array = load_validate_crit();
    form_error_count = validate_form(validatecrit_array,caller);
    if(!form_error_count)
 	{
 		//There are no errors in form validation, so need to submit form
 		caller.submit();
 	}
    
}

function load_validate_crit()
{
    
    //var validatecrit_element = document.getElementById("validatecrit");
    //var validation_json = validatecrit_element.childNodes[1].textContent; //comment inside div #validatecrit contains valdation json
    //var validation_json_array = JSON.parse(validation_json);
    return cookie_to_validatecrit();
    /*var crit,elementname,cond,validatearray;
    validatearray = new Array([]);
    
    if(isset(validatecrit))
    {
        validatecrit = validatecrit.split(';');
    }


    for(crit in validatecrit)
    {
        crit = validatecrit[crit].trim();
        if(!empty(crit))
        {
            crit = crit.split("|");
            elementname = crit[0];
            cond = crit[1];
            cond = cond.split(",");

            validatearray[elementname] = cond;

        }
    }

        return validatearray;*/
        

 }
 
function empty(str) {
    return (!str || 0 === str.length);
}


function validate_form(validate_crit_array,caller)
{
    var error_count=0,condition_array,element,condition,cond,errormessage,errorlabel,font,elementslist,callerelement,sameelement_errorcount,callerelementindex,errorhtml;
    var i,y,x;
	var validation_type_array = ['validate_by_name','validate_by_id'];

    /*Validate elements that are mentioned in #validatecrit element comment which is loaded in $this->data['validatearray'] already by $this->LoadHTML()*/

    if(isset(validate_crit_array)) //only if validate criteria is set proceed else return
    {
    	//caller is a list of all input/select/textarea elements
    	
    	for (x=0 ; x < caller.length ; x++)
    	{
    		callerelement = caller[x];
                var validation_type;
            sameelement_errorcount = 0;
            var formtovalidate = caller.id;
            
            if(isset(validate_crit_array[formtovalidate]))
            {   
                var form_validation_array = validate_crit_array[formtovalidate]
                for(v in validation_type_array)
                {	validation_type = validation_type_array[v];
                    if(validation_type == 'validate_by_name')
                    var element = callerelement.name;
                    else if(validation_type == 'validate_by_id')
                    var element = callerelement.id;
                    if(isset(form_validation_array[validation_type]))//to prevent exceptions in javascript validation if validate_by_id is not used at all in json
                    {
                        if(isset(form_validation_array[validation_type][element]))
                        {
                            condition_array = form_validation_array[validation_type][element];


                            for (i in condition_array )
                            {

                                condition = condition_array[i];
                                cond = condition['cond'];
                                errormessage = condition['error'].trim();

                                //formelem = findParentForm(caller[0]); 
                                if(!validate_element(cond,callerelement.value))
                                {
                                    sameelement_errorcount++;
                                    error_count++;
                                    callerelement.style = "border:solid 1px red";
                                    var errorlabel = document.createElement('label');
                                    var font = document.createElement('font')

                                    font.color = 'red';
                                    font.size = '2';
                                    font.innerHTML = errormessage;

                                    errorlabel.for = callerelement.name;
                                    errorlabel.id = "formerror--label";                          
                                    errorlabel.appendChild(font);                            
                                    //callerelement.parentNode.insertBefore(errorlabel,callerelement);
                                    //callerelement.outerHTML = callerelement.outerHTML.errorlabel;

                                    //ns = callerelement.nextSibling.tagName;
                                    //ns = callerelement.nextSibling.id;
                                    if(sameelement_errorcount >1 )//for a element this is second error count, so we need to append new error message to existing one
                                    {
                                        if(callerelement.nextSibling != null)//to avoid exception of accessing null.tagName
                                        {
                                            if(callerelement.nextSibling.tagName == 'LABEL' && callerelement.nextSibling.id == "formerror--label")
                                            {	//means already we inserted errorlabel once in prior executions , so we should reuse it instead of adding new label again
                                                callerelement.nextSibling.childNodes[0].innerHTML = callerelement.nextSibling.childNodes[0].innerHTML + "||" + errormessage; //child node 0 will be <font> and we need to add text to that font element
                                            }

                                            else
                                            {	//first time we are adding error label to this field;
                                                callerelement.nextSibling.parentNode.insertBefore(errorlabel,callerelement.nextSibling);
                                            }
                                        }
                                        else
                                        {
                                            callerelement.parentNode.insertBefore(errorlabel,callerelement.nextSibling);
                                        }


                                    }
                                    else
                                    {
                                        if(callerelement.nextSibling != null)//to avoid exception of accessing null.tagName
                                        {
                                            if(callerelement.nextSibling.tagName == 'LABEL' && callerelement.nextSibling.id == "formerror--label")
                                            {	//means already we inserted errorlabel once in prior executions , so we should reuse it instead of adding new label again
                                                callerelement.nextSibling.childNodes[0].innerHTML = errormessage; //child node 0 will be <font> and we need to add text to that font element
                                            }

                                            else
                                            {	//first time we are adding error label to this field;
                                                callerelement.nextSibling.parentNode.insertBefore(errorlabel,callerelement.nextSibling);
                                            }
                                        }
                                        else
                                        {
                                            callerelement.parentNode.insertBefore(errorlabel,callerelement.nextSibling);
                                        }
                                    }

                                }
                                else 
                                {
                                    //if validation succceds for one of the condition, then remove if corresponding error message exists in formerror--lable in its sibling
                                    //remove only that error messages corresponding to the condition, there  may be other conditions for the same element, which may fail, so dont remove them
                                    if(callerelement.nextSibling != null)//to avoid exception of accessing null.tagName
                                    {
                                        if(callerelement.nextSibling.tagName == 'LABEL' && callerelement.nextSibling.id == "formerror--label")
                                        {
                                            errorhtml = callerelement.nextSibling.childNodes[0].innerHTML.split("||");
                                            for(y=0 ; y < errorhtml.length ; y++)
                                            {
                                                if(errorhtml[y] == errormessage)
                                                {
                                                    delete errorhtml[y];
                                                }
                                            }
                                            var notemtpycount = 0;
                                            var actualerrorhtml = new Array();
                                            for(y=0 ; y < errorhtml.length ; y++) //to know how many elements are still not empty in array
                                            {
                                                if(!empty(errorhtml[y]))
                                                {
                                                    actualerrorhtml[notemtpycount] = errorhtml[y];
                                                    notemtpycount++;
                                                }
                                            }
                                            if(actualerrorhtml.length > 1 )//implode wiht || only if atleast two errors are there
                                            callerelement.nextSibling.childNodes[0].innerHTML = implode("||",errorhtml);
                                            else if(actualerrorhtml.length == 1)
                                            callerelement.nextSibling.childNodes[0].innerHTML = actualerrorhtml;
                                            else //no errors so set the box border also to black
                                            {
                                                    callerelement.nextSibling.childNodes[0].innerHTML = actualerrorhtml;
                                                    callerelement.style = "border:solid 1px black";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    return error_count;
}

    
function validate_element(element_cond,element_value)
{
    //return 1;
    //if element_cond['op'] is compare_element, then we need to get the value of other element and the call jsvalidate.validate('compare',element_value,$secondelement_value);
    var jsvalidate = new jsvalidator;
    var arg = isset(element_cond['arg'])?element_cond['arg']:null;
    if(isset(element_cond['op']) && element_cond['op'] == 'compare_element')
    {
        if(isset(arg['elementid']))
        {
            var el = document.getElementById(arg['elementid']);
            if(el.tagName != 'textarea' )
            {
                var operand2 = el.value;
            }
            else
            {
                var operand2 = el.innertext;
            }
        }
        else if(isset(arg['elementname']))
        {
            var el = document.getElementsByName(arg['elementname']);
            var k;
            for (k = 0; k < el.length ; k++)
            {
                if(el[k].tagName != 'TEXTAREA' )
                {
                    var operand2 = el[k].value;
                }
                else
                {
                    var operand2 = el[k].innertext;
                }
            }
        }
        var arg1 = Array();
        arg1['operator'] = arg['operator'];
        arg1['comparewith'] = operand2;
        return jsvalidate.validate('compare',element_value,arg1);
    }
    else
    {
        return jsvalidate.validate(element_cond['op'],element_value,arg);
    
    }
}

function in_array(needle, haystack, argStrict) 
{
  //  discuss at: http://phpjs.org/functions/in_array/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: vlado houba
  // improved by: Jonas Sciangula Street (Joni2Back)
  //    input by: Billy
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
  //   returns 1: true
  //   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
  //   returns 2: false
  //   example 3: in_array(1, ['1', '2', '3']);
  //   example 3: in_array(1, ['1', '2', '3'], false);
  //   returns 3: true
  //   returns 3: true
  //   example 4: in_array(1, ['1', '2', '3'], true);
  //   returns 4: false

  var key = '',
    strict = !! argStrict;

  //we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] == ndl)
  //in just one for, in order to improve the performance 
  //deciding wich type of comparation will do before walk array
  if (strict) {
    for (key in haystack) {
      if (haystack[key] === needle) {
        return true;
      }
    }
  } else {
    for (key in haystack) {
      if (haystack[key] == needle) {
        return true;
      }
    }
  }

  return false;
}

function isset() 
{
  //  discuss at: http://phpjs.org/functions/isset/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: FremyCompany
  // improved by: Onno Marsman
  // improved by: Rafał Kukawski
  //   example 1: isset( undefined, true);
  //   returns 1: false
  //   example 2: isset( 'Kevin van Zonneveld' );
  //   returns 2: true

  var a = arguments,
    l = a.length,
    i = 0,
    undef;

  if (l === 0) {
    throw new Error('Empty isset');
  }

  while (i !== l) {
    if (a[i] === undef || a[i] === null) {
      return false;
    }
    i++;
  }
  return true;
}

function findParentForm(elem)
{ 
var parent = elem.parentNode; 
if(parent && parent.tagName != 'FORM'){
parent = findParentForm(parent);
}
return parent;
}

function implode(glue, pieces) {
  //  discuss at: http://phpjs.org/functions/implode/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Waldo Malqui Silva
  // improved by: Itsacon (http://www.itsacon.net/)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
  //   returns 1: 'Kevin van Zonneveld'
  //   example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
  //   returns 2: 'Kevin van Zonneveld'

  var i = '',
    retVal = '',
    tGlue = '';
  if (arguments.length === 1) {
    pieces = glue;
    glue = '';
  }
  if (typeof pieces === 'object') {
    if (Object.prototype.toString.call(pieces) === '[object Array]') {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}

var jsvalidator = function(){}

jsvalidator.prototype.validate = function(cond,value,arg)
{
        //var cond1 = cond.split("#");         //for date,zipcode etc we get extra string with # for example isdate#DDMMYYY#-
        
         switch (cond)
         {
             case 'isint':
                 return this.isint(value);
             case 'isfloat':
                 return this.isfloat(value);
             case 'isemail':
                 return this.isemail(value);
             case 'isip':
                 return this.isip(value);
             case 'ismac':
                 return this.ismac(value);
             case 'isurl':
                 return this.isurl(value);
             //if data then it comes in format of isdate#<Data format DDMMYYYY or DDMONYYYY>#<Separator - or / or .>
             case 'isdate': //allowed formats #DDMMYYYY#- or #DDMMYYYY#/ or#DDMMYYYY#. or #DDMONYYYY#- or #DDMON-YYYY#/ or #DDMONYYYY#. where - , / , . are separators
                     if(isset(arg['format']) && isset(arg['separator']))
                     return this.isdate(value,arg['format'],arg['separator']);
                     else
                     return 0;    
                   return this.isdate(value,cond1[1],cond1[2]);
             case 'isphone': //allowed formats #+CC#10 or #CC#10 or #10 where 10 is number of digits in phone number exluding countrycode. Country code is supported from 1 to 3 characters max
                    if(isset(arg['ccformat']) && isset(arg['digits']))
                     {   
                         if(arg['ccformat'] == '+CC' || arg['ccformat'] == 'CC')
                         return this.isphone(value,arg['ccformat'],arg['digits']);
                         else
                         return 0;
                     }
                    else
                    return 0;                       
             case 'isalphanum':
                 return this.isalphanum(value);
             case 'iszipcode'://#CC where CC is two CHARACTER country code for which zip needs to be validated
                 if(isset(arg['countrycode']))
                 return this.iszipcode(value,arg['countrycode']);
                 else
                 return 0;     
             case 'isnotempty':
                 return this.isnotempty(value);
             case 'isempty':
                 return this.isempty(value);
             case 'compare':
                 return this.compare(value,arg['comparewith'],arg['operator']);
             case 'regex':
                 return this.regexp(arg['expr'],value);
 
         }
    }
    
jsvalidator.prototype.compare = function compare(operand1,operand2,operator)
 {
    if(isset(operand1) && isset(operand2) && isset(operator))
    {
        switch (operator)
        {
             case '==':
                 return (operand1 == operand2)?1:0;
             case '!=':
                 return (operand1 != operand2)?1:0;
             case '>':
                 return (operand1 > operand2)?1:0;
             case '<':
                 return (operand1 < operand2)?1:0;
             case '>=':
                 return (operand1 >= operand2)?1:0;
             case '<=':
                 return (operand1 <= operand2)?1:0;
             case '===':
                 return (operand1 === operand2)?1:0;
             case '!==':
                 return (operand1 === operand2)?1:0;

        }
    }
    else
    return 0;
 }
jsvalidator.prototype.regexp = function(regex,value)
{
	//regex = "/"+regex+"/";
	var thisregex = new RegExp(regex);
    return thisregex.test(value);
    
}

jsvalidator.prototype.isdate = function(value,dateformat,separator)
{       
        var regex;
	switch (dateformat)
        {
            case 'DDMMYYYY':
                regex = "^(?:(?:31(\\"+separator+")(?:0?[13578]|1[02])))|(?:(?:29|30)(\\"+separator+")(?:0?[1,3-9]|1[0-2]))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\"+separator+")(?:0?2|(?:Feb))(\\"+separator+")(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\"+separator+")(?:(?:0?[1-9])|(?:1[0-2]))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})\$";
                break;
            case 'DDMONYYYY':
                regex = "^(?:(?:31(\\"+separator+")(?:Jan|Mar|May|Jul|Aug|Oct|Dec))|(?:(?:29|30)(\\"+separator+")(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\"+separator+")(?:Feb)(\\"+separator+")(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\"+separator+")((?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep)|(?:Oct|Nov|Dec))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})\$";
                break;        
        }
        return this.regexp(regex,value);
	
}

jsvalidator.prototype.isphone = function(value,ccformat,digits)
{
     var regex = '';//dont set it to null or leave it undefined ,becuase javascript is stupid enough to add "null" or "undefined" as string to regex when we do regex = "some value" + regex;
     if(ccformat == '+CC')
     {
        regex = '[\\+][0-9]{1,3}'+regex;
     }
     else if(ccformat == 'CC')
     {
        regex = '[0-9]{1,3}'+regex;
     }
     if(!empty(regex))
     regex = '^'+regex+'[0-9]{'+digits+'}$';
     else
     regex = '^[0-9]{'+digits+'}$';

     return this.regexp(regex,value);;
}

jsvalidator.prototype.iszipcode = function(value,country)
{
    switch(country)
    {
        case 'IN':
        var regex = "^[1-9]{1}[0-9]{5}";//6 digit numeric code not starting with 0
        return this.regexp(regex,value);
    }
}

jsvalidator.prototype.isalphanum = function(value)
{
 var regex = "^[0-9a-zA-Z ,\\.\\-_\\\s\\?\\!]+$";
 return this.regexp(regex,value);
}

jsvalidator.prototype.isempty = function(value)
{
 return empty(value);
}

jsvalidator.prototype.isnotempty = function(value)
{
 return !empty(value);
}

jsvalidator.prototype.isemail = function(value) {
    var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return regex.test(value);
}
jsvalidator.prototype.isip = function(value)
{
    var regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/i;
    return regex.test(value);
}
jsvalidator.prototype.isint = function(value) 
{
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}
jsvalidator.prototype.isfloat = function(value)
{
    var val = parseFloat(value);
    if(isNaN(val))
    return 0;
    else
    return 1;

}
jsvalidator.prototype.isurl = function(value)
{
   return 1; 
}
//document.getElementById("seccodeform").addEventListener("submit", fps_option_altemail_verify_seccode(event));
//function createCookie(name,value,days) {
//    if (days) {
//        var date = new Date();
///        date.setTime(date.getTime()+(days*24*60*60*1000));
//        var expires = ", expires="+date.toGMTString();
//    }
 //   else var expires = "";
 //   //var cookie = name+"="+value+",path=/"+",domain="+window.location.host.toString()+expires;
 //   var cookie = [name, '=', JSON.stringify(value), '; domain=.', window.location.host.toString(), '; path=/;'].join('');
 //   document.cookie = cookie;
//}
function bake_json_cookie(name, value) {
  var cookie = [name, '=', JSON.stringify(value), '; domain=.', window.location.host.toString(), '; path=/;'].join('');
  document.cookie = cookie;
}

function read_json_cookie(name) {
 var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
 var result_decoded = decodeURIComponent(result[1].replace(/\+/g, ' '));//browser replaces spaces with + while creating ccokies, so need to replace + with space and also browser does URI encode, so we need to URIdecode
 var result_json = JSON.parse(result_decoded);
 return result_json;
}

function delete_cookie(name) {
  document.cookie = [name, '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/; domain=.', window.location.host.toString()].join('');
}

/*function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}*/

//window.onload = validatecrit_to_cookie;

function validatecrit_to_cookie()
{
    var validatecrit_element = document.getElementById("validatecrit");
    var validation_json = validatecrit_element.childNodes[1].textContent; //comment inside div #validatecrit contains valdation json
    //var json_validate_string = validation_json;
    validation_json = JSON.parse(validation_json);
    bake_json_cookie('validate_crit_json',validation_json);
    validatecrit_element.childNodes[1].textContent = '';
    //alert("Your Cookie : " + document.cookie);
}
function cookie_to_validatecrit()
{
    var validation_json_array = read_json_cookie('validate_crit_json3');
    
    return validation_json_array;
}
function stripslashes(str) {
  //       discuss at: http://phpjs.org/functions/stripslashes/
  //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      improved by: Ates Goral (http://magnetiq.com)
  //      improved by: marrtins
  //      improved by: rezna
  //         fixed by: Mick@el
  //      bugfixed by: Onno Marsman
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //         input by: Rick Waldron
  //         input by: Brant Messenger (http://www.brantmessenger.com/)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //        example 1: stripslashes('Kevin\'s code');
  //        returns 1: "Kevin's code"
  //        example 2: stripslashes('Kevin\\\'s code');
  //        returns 2: "Kevin\'s code"

  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
}
