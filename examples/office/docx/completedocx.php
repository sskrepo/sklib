<?php
/*
Accept a docx file and "tobereplaced string" and "replacewith string" as input
replace given strings to desired strings in docx file and save it

DEPENDENCIES classes
sk_office_docx
sk_file_fileobj

USAGE
In a HTML file upload script, give this file name "completedocx.php" as action script
this will handle one file or multiple files, but only docx files

<html>
<head>
<title>Administration - upload new files</title>
</head>
<body>
<h1>Upload new news files</h1>
<form action="completedocx.php" method="post" enctype="multipart/form-data"/>
<div>
<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
<label for="userfile">Upload a file:</label>
<input type="file" multiple name="userfile[]" id="userfile"/>
<input type="submit" value="Send File"/>
</div>
</form>
</body>
</html

*/
include(getenv("APP_INCLUDE_GLOBAL"));
$filex = new \sskrepo\sklib\file\fileobj();
$result = $filex->save_files("D:\DOC");

if($result == 0)
{ if($filex->multiupload == 'FALSE')
  {   
      $doc = new \sskrepo\sklib\office\docx();
      $doc->loaddocx($filex->filename);
      $doc->isdocx();
      $doc->replacestring("this","Changed this");
  }
  else
  { for($i=0;$i < $filex->numfiles; $i++)
    {   
        $doc[$i] = new \sskrepo\sklib\office\docx();
        $doc[$i]->loaddocx($filex->filename[$i]);
        $doc[$i]->isdocx();
        $doc[$i]->replacestring("this","Changed this");   //replace all occurences of "this" with "Changed this"
    }
  
  }
}
else
{ echo "Error : $result";
  exit;
}
?>