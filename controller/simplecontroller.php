<?php

namespace sskrepo\sklib\controller;

class simplecontroller
{
    public $safeinput,$form;
    
    public function __construct()
    {
        $this->safeinput = new \sskrepo\sklib\safeinput;
        $this->form = new \sskrepo\sklib\form\form;
        $this->view = new \sskrepo\sklib\form\view;
    }
}

