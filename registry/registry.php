<?php
/*
$t = \sk\registry::getInstance();
$t1 = \sk\registry::getInstance();
$t->myclass = 'job';
echo $t->myclass;*/
namespace sskrepo\sklib\registry;
class registry{

   private static $_instance = null;
   private $data;

   private function  __construct() {}
   private function  __clone(){}
   public static function getInstance()
   {   
        if( !is_object(self::$_instance) )  //or if( is_null(self::$_instance) ) or if( self::$_instance == null ) 
        self::$_instance = new \sskrepo\sklib\registry\registry();  
        return self::$_instance;
   }
   public function __get($varName){

      if (!array_key_exists($varName,$this->data)){
          //this attribute is not defined!
         return NULL;
      }
      else return $this->data[$varName];

   }

   public function __set($varName,$value){
      $this->data[$varName] = $value;
   }

}

?>