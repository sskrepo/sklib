<?php                                                     
//Always install such a way that public/home/ is your document root.

//GLOBAL PARAMETERS
 //in reference to DOCUMENTROOT of domain URL. If installing in DOCUMENT ROOT , then use / for $INSTALL_HOME
//it is not recommended to change $INSTALL_HOME to anything other than /, if you have to change it
//that means you are installing such a way that views and other folders are coming inside document root.
//this is not recommended at all, as views are not protected and can be accessed directly.
//So Always install such a way that public/home/ is your document root so thata except /public/home, everything else stays out of Document root

$registry = \sk\registry\registry::getInstance();



//global  $INSTALL_HOME;
//$INSTALL_HOME="/";
$registry->install_home = "/";


//global $INSTALL_HOME_INC;//Complete path of Installation directory.
//$INSTALL_HOME_INC="/var/www/newsso";
$registry->install_home_inc = "/var/www/newsso";

//global $VIEWS_INC;
//$VIEWS_INC = $INSTALL_HOME_INC."/views";
$registry->views_inc = $registry->install_home_inc."/views";

//global $SESS_TIMEOUT; 
//$SESS_TIMEOUT = 600; // session timeout in seconds
$registry->sess_timeout = 600;

//global $salt;
$registry->salt = 'secretsauce';   //for md5 password before storing passwd into database

//global $uniquekey_expire_time;
$registry->uniquekey_expire_time = 60;  //time in seconds

//global $ERROR_LOG;
$registry->ERROR_LOG = $registry->install_home_inc."/log/sso_error.log";

//global $sso_silent_mode;
$registry->sso_silent_mode = 'enabled';

//includes all .php files from lib
/*$directory = new RecursiveDirectoryIterator($registry->install_home_inc.'/lib/');
$recIterator = new RecursiveIteratorIterator($directory);
$regex = new RegexIterator($recIterator, '/\.php$/i');

foreach($regex as $item) {
    include $item->getPathname();
}

require_once '/var/www/PHPMailer/PHPMailerAutoload.php';
R::setup( 'mysql:host=letterbox.in;dbname=newssodb',
        'ssouser', 'ssouser' );*/

$registry->safeinput = new \sskrepo\sklib\safeinput;
$registry->form = new \sskrepo\sklib\form\form;

?>
