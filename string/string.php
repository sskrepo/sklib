<?php

namespace sskrepo\sklib\string;
class string
{
    public function get_random_ssl_string($string_size)
    {
        return bin2hex(openssl_random_pseudo_bytes($string_size));
    }

    public function get_random_string($length, $valid_chars)
    {

        //http://stackoverflow.com/questions/853813/how-to-create-a-random-string-using-php

        //usage
        //$original_string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //$random_string = get_random_string($original_string, 10);
        if(!isset($valid_chars))
        $valid_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        // start with an empty random string
        $random_string = "";

        // count the number of chars in the valid chars string so we know how many choices we have
        $num_valid_chars = strlen($valid_chars);

        // repeat the steps until we've created a string of the right length
        for ($i = 0; $i < $length; $i++)
        {
            // pick a random number from 1 up to the number of valid chars
            $random_pick = mt_rand(1, $num_valid_chars);

            // take the random character out of the string of valid chars
            // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
            $random_char = $valid_chars[$random_pick-1];

            // add the randomly-chosen char onto the end of our string so far
            $random_string .= $random_char;
        }

        // return our finished random string
        return $random_string;
    }
    public function isstringhavingHTML($string)
    {
        if($string != strip_tags($string)) 
        return 1;// contains HTML
        else
        return 0;

    }

}

?>