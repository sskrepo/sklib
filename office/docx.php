<?php
//USAGE to replace a word "this" to "Changed this" in a DOCX file

//$doc = new sk_office_docx();
//$doc->loaddocx("D:/apache/htdocs/doc/doc3.docx");
//$doc->isdocx();
//$doc->replacestring("this","Changed This");

//include("D:\Apache\htdocs\gincludes\lib\zip.php");
namespace sskrepo\sklib\office;
class docx
{ public $source;
  public $doc;
  public $zipfile;
  public $validdoc;
  public $zip;
  //methods
  
  public function __construct()
  {
      $this->zip = new \sskrepo\sklib\file\zipit();
  }
  function loadXML($file)
  { $this->doc = new DOMDocument();
    $result= $this->doc->load($file);
    //echo $this->doc->nodeName;
    
  }
  function loaddocx($file)
  { //preg_match("/^(.+)\.docx$/",$file,$match); //these are commented becuase we may not have .docx in the file name but it still can be a docx file for example in unix machines or other machines where ext is not important
    //$filenoext = $match[1];
    if(rename($file,$file.".zip"))
    {
    $this->zipfile = $file.".zip";
    //echo "zipfile".$this->zipfile;
    $this->source=$file;
    }
  }
  function isdocx()
  { if($this->zipfile)
    { 
      //echo "entered zip";
      $templocation = 'D:/apache/htdocs/temp';
      //echo $templocation;
      $result = $this->zip->zipper("unzip",$this->zipfile,$templocation);
      //echo $result;
      if(is_file("$templocation/word/document.xml"))
      { $this->validdoc = "TRUE";
      }
    }
    else
    { $result="Error: You need to loaddocx before using isdocx";
      return $result;
    }
  }
  function replacestring($tobereplaced,$replacewith)
  { //echo "entered replace string ".$this->source;
    if($this->validdoc)
    {  
      //echo "entered zip";
      $templocation = 'D:/apache/htdocs/temp/'.time();
      mkdir($templocation);
      //echo $templocation;
      $result = $this->zip->zipper("unzip",$this->zipfile,$templocation);
      $this->loadXML("$templocation/word/document.xml");
      $this->findnode($this->doc,"none",$tobereplaced,$replacewith);
      $this->doc->save("$templocation/word/document.xml");
      $this->zip->zipper("zip",$templocation,$this->source);
      rrmdir($templocation);               //rrmdir is correct this is no rmdir, this is custom written in rmdir.php, this will recursively delete all folders and files inside specified directory
      unlink($this->source.".zip");         //we renamed original file to file.zip and then created a new doc with same name , so cleanup .zip file that is left out
    }
    else
    { $result = "Error:Need to use isdoc() to validdate doc before replacing, Document is not valid";
    }
  }
  
  function findnode($node,$parentnode,$tobereplaced,$replacewith) 
  { if($parentnode == "w:t")         //This is to find the parent of #text is w:t text element or not. If text element then we need to edit this node text to change the text in Word document.
    { $node->nodeValue = str_replace($tobereplaced,$replacewith,$node->nodeValue);
      //$node->nodeValue = $node->nodeValue."Changed line";
    }
    if(isset($node->childNodes))
    {
    for ($i = 0; $i < ($node->childNodes->length); $i++)
    { $this->findnode($node->childNodes->item($i),$node->nodeName,$tobereplaced,$replacewith);
    }
    }

  }
}

?>