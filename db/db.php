<?php
namespace sskrepo\sklib\db;
class db{
  public  $dblink;
  public  $errlog;     //For error logging
  public  $prepare;
  public  $wal;
  
  public function __construct()
  {
      $this->errlog = new sk_error();
      $this->wal = new sk_wallet();
  }

  public function connect($dbhost,$dbuser,$dbn,$from)
  { 
    if(empty($from))
    {     $from = "dbconnect";
    }
    $this->dblink = new mysqli($dbhost,$dbuser,$this->wal->getkeys($dbuser),$dbn);
    if ($this->dblink->connect_errno)
    {
      $error = 'Unable to connect to the MYSQL SERVER: '.$this->dblink->connect_error;
      $this->errlog->error($error,$from);
      //errorlog($from,$output,"Error");
      exit();
    }
    
    #Set characteret for session
    
    if (!$this->dblink->set_charset("utf8"))
    {
      $error = 'Unable to set database characterset.';
      $this->errlog->error($error,$from);
      exit();
    }
    
    #Select database to use
    
    if (!$this->dblink->select_db($dbn))
    {
      $error = "Unable to use the ".$dbn." database.";
      $this->errlog->error($error,$from);
      exit();
    }
    return $this->dblink;
  }
  #Connect to database
  public function query($query,$from)
  { 
      
    #set password based on user
    if(empty($from))
    {     $from = "dbquery";
    }
    
    $result = $this->dblink->query($query);
    
    if (!$result)
    {
      $error = 'Query failed with error :'.$query.$this->dblink->error;
      $this->errlog->error($error,$from);
      exit();
    }
    
    return $result;
  }
  public function refValues($arr){
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;
  }

//dbprepquery will take sql statement and bind param values and will prepare the statement, bind the params to that statement and execute and return the result
  public function dbprepquery($query,$type,$param,$from)
  { 
    if(empty($from))
    {     $from = "dbprepquery";
    }
    
    $prepstmt = $this->dblink->stmt_init();
    if(!($this->prepare = $this->dblink->prepare($query)))
    {
      $error = 'Failed to prepare statement :'.$query.$this->dblink->error;
      $this->errlog->error($error,$from);
      exit();
    }
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {   $refs = array();
        foreach($param as $key => $value)
            $refs[$key] = &$param[$key];
    }
    else
    { $refs = $param;
    }
    
    if(!call_user_func_array(array($this->prepare,'bind_param'), array_merge(array($type),$refs)))
    { $error = 'Failed to Bind values for query :'.$query.$this->dblink->error;
      $this->errlog->error($error,$from);
      exit();
    }
    //mysqli_stmt_bind_param($prepstmt,'s',$param);
    $this->prepare->execute();
    $result = $this->prepare->get_result();
    
    if (!$result)
    {
      $error = 'Query failed with error :'.$query.$this->dblink->error;
      $this->errlog->error($error,$from);
      exit();
    }
    
    return $result;
  }
  
    public function resultToArray($result)
    {
        /*// Usage
        $query = 'SELECT DISTINCT $fields FROM `posts` WHERE `profile_user_id` = $user_id LIMIT 4';
        $result = $mysqli->query($query);
        $rows = resultToArray($result);
        var_dump($rows); // Array of rows
        $result->free();*/
        $rows = array();
        while($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }
    
    public function resultastable($result)
    {
        echo "<table>";
        echo "<tr>";

        //prepare table header(column names);
        while($column = mysqli_fetch_field($result))
        {
        $columnname[]= $column->name;
        echo "<th>".$column->name."</th>";
        }
        echo "</tr>";

        //prepare rows data
        while($data = mysqli_fetch_array($result))
        { echo "<tr>";

                $columnnumber = 0;
        //each row's column data. example $data[column1] $data[column2] in a row;
        //	while($col = $columnname[$columnnumber]) -- This while is repalced with for to avoid warning "undefined offset"  
        for($columnnumber;isset($columnname[$columnnumber]);$columnnumber++)
          {  $col = $columnname[$columnnumber];
                echo "<td>".$data[$col]."</td>";
                //$columnumber = $columnnumber++;
                }
          echo "</tr>";
        }
        echo "</table>";

        return;
    }
   
}

?>