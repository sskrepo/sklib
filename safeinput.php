<?php

//usage
/*
<?php
include ('safeinput.php');
$safeinput = new sk_safeinput;

$safeiput->validate($condition,$variabletobevalidated); //validation
OR
$safeinput->filter($filter_type,$var_type,$var,$db='NULL'); //sanitizing
OR
$safeinput->isemail($var_type,$var); //validation

?>
 * 
 * this is used Mainly for validating any variable against a condition or sanitizing Input/output data.
 * ***************
 * For validation:
 * ***************
 * $safeinput->validate($condition,$variabletobevalidated);
 * 
 * RETURN $variabletobevalidated as is on successfull validation, returns NULL on failed validation
 * 
 * $variabletobevalidated : can be any variable that needs validation.
 * $condition : will be in below format.
 * <validationfunction>#<additional format input for that validation function>
 ********************************
 Validationfunctions supported:
 ********************************

'isint' : //is int or not, No additioan format needed
'isfloat'://is float or not, No additioan format needed
'isemail'://is email or not, No additioan format needed
'isip'://is ip or not, No additioan format needed
'ismac'://is ismac or not, No additioan format needed
'isurl'://is isurl or not, No additioan format needed
'isdate': //isdate#<Date format>#<Separator - or / or .>

    //example isdate#DDMMYYYY#-
    //allowed formats #DDMMYYYY#- or #DDMMYYYY#/ or#DDMMYYYY#. or #DDMONYYYY#- or #DDMON-YYYY#/ or #DDMONYYYY#. where - , / , . are separators

'isphone': isphone#<phone formats>
    example isphone#+CC#10
    //allowed phone formats #+CC#10 or #CC#10 or #10 where 10 is number of digits in phone number exluding countrycode. Country code is supported from 1 to 3 characters max

'isalphanum':
'iszipcode'://#CC
     CC is two CHARACTER country code for which zip needs to be validated
     example iszipcode#IN for india zipcode validation
 * 
 * ***************
 * For sanitizing
 * ***************
 * 
 *  $safeinput->filter($filter_type,$var_type,$var,$db='NULL')
 * 
 * $filter_type Supported values: 
raw
int
float
email
url
htmlspecchars
mysqlescstring
 * 
 * $var_type,$var
 * $var_type can be get,post,request,cookie and $var can be index key to $_GET,$_POST,$_REQUEST,$_COOKIE respectively
 * 
 * if you want to filter any variable , then $var_type should be 'VAR' and $var should variable to be filtered

$db is only valid if filter_type is mysqlescstring, $db should be a valid mysqli db string

*/
namespace sskrepo\sklib;
class safeinput
{
    public function isint($var_type,$var)
    {
        
        /*switch ($var_type)
        {
            case 'get':
                return isset($_GET['var'])?filter_input(INPUT_GET, $var, FILTER_VALIDATE_INT):FALSE;          
            case 'post':
                return isset($_POST['var'])?filter_input(INPUT_POST, $var, FILTER_VALIDATE_INT):FALSE;        
            case 'request':
                return isset($_REQUEST['var'])?filter_input(INPUT_REQUEST, $var, FILTER_VALIDATE_INT):FALSE;        
            case 'cookie':
                return isset($_COOKIE['var'])?filter_input(INPUT_COOKIE, $var, FILTER_VALIDATE_INT):FALSE;          
            case 'var':
                return filter_var($var,FILTER_VALIDATE_INT);      
        }*/
        $filter_flag = FILTER_VALIDATE_INT;
        return $this->validate_builder($var_type,$var,$filter_flag);
    }
    public function isfloat($var_type,$var)
    {
        $filter_flag = FILTER_VALIDATE_FLOAT;
        return $this->validate_builder($var_type,$var,$filter_flag);
    }
    public function isemail($var_type,$var)
    {
        $filter_flag = FILTER_VALIDATE_EMAIL;
        return $this->validate_builder($var_type,$var,$filter_flag);
    }
    public function isip($var_type,$var)
    {
        $filter_flag = FILTER_VALIDATE_IP;
        return $this->validate_builder($var_type,$var,$filter_flag);
    }
    public function ismac($var_type,$var)
    {
        $filter_flag = FILTER_VALIDATE_MAC;
        return $this->validate_builder($var_type,$var,$filter_flag);
    }
    public function isurl($var_type,$var)
    {
        $filter_flag = FILTER_VALIDATE_URL;
        return $this->validate_builder($var_type,$var,$filter_flag);
    }
     public function isdate($var_type,$var,$dateformat,$separator)
     {
        switch ($dateformat)
        {
            case 'DDMMYYYY':
                $regex = "^(?:(?:31(\\".$separator.")(?:0?[13578]|1[02])))|(?:(?:29|30)(\\".$separator.")(?:0?[1,3-9]|1[0-2]))(\\".$separator.")(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\\".$separator.")(?:0?2|(?:Feb))(\\".$separator.")(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\\".$separator.")(?:(?:0?[1-9])|(?:1[0-2]))(\\".$separator.")(?:(?:1[6-9]|[2-9]\d)?\d{2})\$";
                break;
            case 'DDMONYYYY':
                $regex = "^(?:(?:31(\\".$separator.")(?:Jan|Mar|May|Jul|Aug|Oct|Dec))|(?:(?:29|30)(\\".$separator.")(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)))(\\".$separator.")(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\\".$separator.")(?:Feb)(\\".$separator.")(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\\".$separator.")((?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep)|(?:Oct|Nov|Dec))(\\".$separator.")(?:(?:1[6-9]|[2-9]\d)?\d{2})\$";
                break;     
        }
        return $this->isregex($var_type,$var,$regex);
     }
     public function intl_phone($phonenumber,$cc)
     {
         //Thanks to James Nonnemaker for publishing well prepared list of country code to length mapping on moo.net
        $intl_phone_criteria = array(
            '93' => array('min_length' => '9', 'max_length' => '9'), //Afghanistan
            '355' => array( 'min_length' => '3', 'max_length' => '9'), //Albania
            '213' => array( 'min_length' => '8', 'max_length' => '9'), //Algeria
            '376' => array( 'min_length' => '6', 'max_length' => '9'), //Andorra
            '244' => array( 'min_length' => '9', 'max_length' => '9'), //Angola
            '54' => array( 'min_length' => '10', 'max_length' => '10'), //Argentina
            '374' => array( 'min_length' => '8', 'max_length' => '8'), //Armenia
            '297' => array( 'min_length' => '7', 'max_length' => '7'), //Aruba
            '61' => array( 'min_length' => '5', 'max_length' => '15'), //Australia
            '672' => array( 'min_length' => '6', 'max_length' => '6'), //Australian External Territories
            '43' => array( 'min_length' => '4', 'max_length' => '13'), //Austria
            '994' => array( 'min_length' => '8', 'max_length' => '9'), //Azerbaijan
            '973' => array( 'min_length' => '8', 'max_length' => '8'), //Bahrain
            '880' => array( 'min_length' => '6', 'max_length' => '10'), //Bangladesh
            '375' => array( 'min_length' => '9', 'max_length' => '10'), //Belarus
            '32' => array( 'min_length' => '8', 'max_length' => '9'), //Belgium
            '501' => array( 'min_length' => '7', 'max_length' => '7'), //Belize
            '229' => array( 'min_length' => '8', 'max_length' => '8'), //Benin
            '975' => array( 'min_length' => '7', 'max_length' => '8'), //Bhutan
            '591' => array( 'min_length' => '8', 'max_length' => '8'), //Bolivia (Plurinational State of)
            '599' => array( 'min_length' => '7', 'max_length' => '7'), //Bonaire Sint Eustatius and Saba
            '387' => array( 'min_length' => '8', 'max_length' => '8'), //Bosnia and Herzegovina
            '267' => array( 'min_length' => '7', 'max_length' => '8'), //Botswana
            '55' => array( 'min_length' => '10', 'max_length' => '10'), //Brazil
            '673' => array( 'min_length' => '7', 'max_length' => '7'), //Brunei Darussalam
            '359' => array( 'min_length' => '7', 'max_length' => '9'), //Bulgaria
            '226' => array( 'min_length' => '8', 'max_length' => '8'), //Burkina Faso
            '257' => array( 'min_length' => '8', 'max_length' => '8'), //Burundi
            '855' => array( 'min_length' => '8', 'max_length' => '8'), //Cambodia
            '237' => array( 'min_length' => '8', 'max_length' => '8'), //Cameroon
            '238' => array( 'min_length' => '7', 'max_length' => '7'), //Cape Verde
            '236' => array( 'min_length' => '8', 'max_length' => '8'), //Central African Rep.
            '235' => array( 'min_length' => '8', 'max_length' => '8'), //Chad
            '56' => array( 'min_length' => '8', 'max_length' => '9'), //Chile
            '86' => array( 'min_length' => '5', 'max_length' => '12'), //China
            '57' => array( 'min_length' => '8', 'max_length' => '10'), //Colombia
            '269' => array( 'min_length' => '7', 'max_length' => '7'), //Comoros
            '242' => array( 'min_length' => '9', 'max_length' => '9'), //Congo
            '682' => array( 'min_length' => '5', 'max_length' => '5'), //Cook Islands
            '506' => array( 'min_length' => '8', 'max_length' => '8'), //Costa Rica
            '225' => array( 'min_length' => '8', 'max_length' => '8'), //C?te d'Ivoire
            '385' => array( 'min_length' => '8', 'max_length' => '12'), //Croatia
            '53' => array( 'min_length' => '6', 'max_length' => '8'), //Cuba
            '599' => array( 'min_length' => '7', 'max_length' => '8'), //Cura?ao
            '357' => array( 'min_length' => '8', 'max_length' => '11'), //Cyprus
            '420' => array( 'min_length' => '4', 'max_length' => '12'), //Czech Rep.
            '850' => array( 'min_length' => '6', 'max_length' => '17'), //Dem. People's Rep. of Korea
            '243' => array( 'min_length' => '5', 'max_length' => '9'), //Dem. Rep. of the Congo
            '45' => array( 'min_length' => '8', 'max_length' => '8'), //Denmark
            '246' => array( 'min_length' => '7', 'max_length' => '7'), //Diego Garcia
            '253' => array( 'min_length' => '6', 'max_length' => '6'), //Djibouti
            '593' => array( 'min_length' => '8', 'max_length' => '8'), //Ecuador
            '20' => array( 'min_length' => '7', 'max_length' => '9'), //Egypt
            '503' => array( 'min_length' => '7', 'max_length' => '11'), //El Salvador
            '240' => array( 'min_length' => '9', 'max_length' => '9'), //Equatorial Guinea
            '291' => array( 'min_length' => '7', 'max_length' => '7'), //Eritrea
            '372' => array( 'min_length' => '7', 'max_length' => '10'), //Estonia
            '251' => array( 'min_length' => '9', 'max_length' => '9'), //Ethiopia
            '500' => array( 'min_length' => '5', 'max_length' => '5'), //Falkland Islands (Malvinas)
            '298' => array( 'min_length' => '6', 'max_length' => '6'), //Faroe Islands
            '679' => array( 'min_length' => '7', 'max_length' => '7'), //Fiji
            '358' => array( 'min_length' => '5', 'max_length' => '12'), //Finland
            '33' => array( 'min_length' => '9', 'max_length' => '9'), //France
            '262' => array( 'min_length' => '9', 'max_length' => '9'), //French Departments and Territories in the Indian Ocean
            '594' => array( 'min_length' => '9', 'max_length' => '9'), //French Guiana
            '689' => array( 'min_length' => '6', 'max_length' => '6'), //French Polynesia
            '241' => array( 'min_length' => '6', 'max_length' => '7'), //Gabon
            '220' => array( 'min_length' => '7', 'max_length' => '7'), //Gambia
            '995' => array( 'min_length' => '9', 'max_length' => '9'), //Georgia
            '49' => array( 'min_length' => '6', 'max_length' => '13'), //Germany
            '233' => array( 'min_length' => '5', 'max_length' => '9'), //Ghana
            '350' => array( 'min_length' => '8', 'max_length' => '8'), //Gibraltar
            '30' => array( 'min_length' => '10', 'max_length' => '10'), //Greece
            '299' => array( 'min_length' => '6', 'max_length' => '6'), //Greenland
            '590' => array( 'min_length' => '9', 'max_length' => '9'), //Guadeloupe
            '502' => array( 'min_length' => '8', 'max_length' => '8'), //Guatemala
            '224' => array( 'min_length' => '8', 'max_length' => '8'), //Guinea
            '245' => array( 'min_length' => '7', 'max_length' => '7'), //Guinea-Bissau
            '592' => array( 'min_length' => '7', 'max_length' => '7'), //Guyana
            '509' => array( 'min_length' => '8', 'max_length' => '8'), //Haiti
            '504' => array( 'min_length' => '8', 'max_length' => '8'), //Honduras
            '852' => array( 'min_length' => '4', 'max_length' => '9'), //Hong Kong China
            '36' => array( 'min_length' => '8', 'max_length' => '9'), //Hungary
            '354' => array( 'min_length' => '7', 'max_length' => '9'), //Iceland
            '91' => array( 'min_length' => '7', 'max_length' => '10'), //India
            '62' => array( 'min_length' => '5', 'max_length' => '10'), //Indonesia
            '870' => array( 'min_length' => '9', 'max_length' => '9'), //Inmarsat SNAC
            '800' => array( 'min_length' => '8', 'max_length' => '8'), //International Freephone Service
            '882' => array( 'min_length' => '0', 'max_length' => '0'), //International Networks shared code
            '883' => array( 'min_length' => '0', 'max_length' => '0'), //International Networks shared code
            '979' => array( 'min_length' => '9', 'max_length' => '9'), //International Premium Rate Service (IPRS)
            '808' => array( 'min_length' => '8', 'max_length' => '8'), //International Shared Cost Service (ISCS)
            '98' => array( 'min_length' => '6', 'max_length' => '10'), //Iran (Islamic Republic of)
            '964' => array( 'min_length' => '8', 'max_length' => '10'), //Iraq
            '353' => array( 'min_length' => '7', 'max_length' => '11'), //Ireland
            '972' => array( 'min_length' => '8', 'max_length' => '9'), //Israel
            '39' => array( 'min_length' => '1', 'max_length' => '11'), //Italy
            '81' => array( 'min_length' => '5', 'max_length' => '13'), //Japan
            '962' => array( 'min_length' => '5', 'max_length' => '9'), //Jordan
            '7' => array( 'min_length' => '10', 'max_length' => '10'), //Kazakhstan
            '254' => array( 'min_length' => '6', 'max_length' => '10'), //Kenya
            '686' => array( 'min_length' => '5', 'max_length' => '5'), //Kiribati
            '82' => array( 'min_length' => '8', 'max_length' => '11'), //Korea (Rep. of)
            '965' => array( 'min_length' => '7', 'max_length' => '8'), //Kuwait
            '996' => array( 'min_length' => '9', 'max_length' => '9'), //Kyrgyzstan
            '856' => array( 'min_length' => '8', 'max_length' => '10'), //Lao P.D.R.
            '371' => array( 'min_length' => '7', 'max_length' => '8'), //Latvia
            '961' => array( 'min_length' => '7', 'max_length' => '8'), //Lebanon
            '266' => array( 'min_length' => '8', 'max_length' => '8'), //Lesotho
            '231' => array( 'min_length' => '7', 'max_length' => '8'), //Liberia
            '218' => array( 'min_length' => '8', 'max_length' => '9'), //Libya
            '423' => array( 'min_length' => '7', 'max_length' => '9'), //Liechtenstein
            '370' => array( 'min_length' => '8', 'max_length' => '8'), //Lithuania
            '352' => array( 'min_length' => '4', 'max_length' => '11'), //Luxembourg
            '853' => array( 'min_length' => '7', 'max_length' => '8'), //Macao China
            '261' => array( 'min_length' => '9', 'max_length' => '10'), //Madagascar
            '265' => array( 'min_length' => '7', 'max_length' => '8'), //Malawi
            '60' => array( 'min_length' => '7', 'max_length' => '9'), //Malaysia
            '960' => array( 'min_length' => '7', 'max_length' => '7'), //Maldives
            '223' => array( 'min_length' => '8', 'max_length' => '8'), //Mali
            '356' => array( 'min_length' => '8', 'max_length' => '8'), //Malta
            '692' => array( 'min_length' => '7', 'max_length' => '7'), //Marshall Islands
            '596' => array( 'min_length' => '9', 'max_length' => '9'), //Martinique
            '222' => array( 'min_length' => '7', 'max_length' => '7'), //Mauritania
            '230' => array( 'min_length' => '7', 'max_length' => '7'), //Mauritius
            '52' => array( 'min_length' => '10', 'max_length' => '10'), //Mexico
            '691' => array( 'min_length' => '7', 'max_length' => '7'), //Micronesia
            '373' => array( 'min_length' => '8', 'max_length' => '8'), //Moldova (Republic of)
            '377' => array( 'min_length' => '5', 'max_length' => '9'), //Monaco
            '976' => array( 'min_length' => '7', 'max_length' => '8'), //Mongolia
            '382' => array( 'min_length' => '4', 'max_length' => '12'), //Montenegro
            '212' => array( 'min_length' => '9', 'max_length' => '9'), //Morocco
            '258' => array( 'min_length' => '8', 'max_length' => '9'), //Mozambique
            '95' => array( 'min_length' => '7', 'max_length' => '9'), //Myanmar
            '264' => array( 'min_length' => '6', 'max_length' => '10'), //Namibia
            '674' => array( 'min_length' => '4', 'max_length' => '7'), //Nauru
            '977' => array( 'min_length' => '8', 'max_length' => '9'), //Nepal
            '31' => array( 'min_length' => '9', 'max_length' => '9'), //Netherlands
            '687' => array( 'min_length' => '6', 'max_length' => '6'), //New Caledonia
            '64' => array( 'min_length' => '3', 'max_length' => '10'), //New Zealand
            '505' => array( 'min_length' => '8', 'max_length' => '8'), //Nicaragua
            '227' => array( 'min_length' => '8', 'max_length' => '8'), //Niger
            '234' => array( 'min_length' => '7', 'max_length' => '10'), //Nigeria
            '683' => array( 'min_length' => '4', 'max_length' => '4'), //Niue
            '47' => array( 'min_length' => '5', 'max_length' => '8'), //Norway
            '968' => array( 'min_length' => '7', 'max_length' => '8'), //Oman
            '92' => array( 'min_length' => '8', 'max_length' => '11'), //Pakistan
            '680' => array( 'min_length' => '7', 'max_length' => '7'), //Palau
            '507' => array( 'min_length' => '7', 'max_length' => '8'), //Panama
            '675' => array( 'min_length' => '4', 'max_length' => '11'), //Papua New Guinea
            '595' => array( 'min_length' => '5', 'max_length' => '9'), //Paraguay
            '51' => array( 'min_length' => '8', 'max_length' => '11'), //Peru
            '63' => array( 'min_length' => '8', 'max_length' => '10'), //Philippines
            '48' => array( 'min_length' => '6', 'max_length' => '9'), //Poland
            '351' => array( 'min_length' => '9', 'max_length' => '11'), //Portugal
            '974' => array( 'min_length' => '3', 'max_length' => '8'), //Qatar
            '40' => array( 'min_length' => '9', 'max_length' => '9'), //Romania
            '7' => array( 'min_length' => '10', 'max_length' => '10'), //Russian Federation
            '250' => array( 'min_length' => '9', 'max_length' => '9'), //Rwanda
            '247' => array( 'min_length' => '4', 'max_length' => '4'), //Saint Helena Ascension and Tristan da Cunha
            '290' => array( 'min_length' => '4', 'max_length' => '4'), //Saint Helena Ascension and Tristan da Cunha
            '508' => array( 'min_length' => '6', 'max_length' => '6'), //Saint Pierre and Miquelon
            '685' => array( 'min_length' => '3', 'max_length' => '7'), //Samoa
            '378' => array( 'min_length' => '6', 'max_length' => '10'), //San Marino
            '239' => array( 'min_length' => '7', 'max_length' => '7'), //Sao Tome and Principe
            '966' => array( 'min_length' => '8', 'max_length' => '9'), //Saudi Arabia
            '221' => array( 'min_length' => '9', 'max_length' => '9'), //Senegal
            '381' => array( 'min_length' => '4', 'max_length' => '12'), //Serbia
            '248' => array( 'min_length' => '7', 'max_length' => '7'), //Seychelles
            '232' => array( 'min_length' => '8', 'max_length' => '8'), //Sierra Leone
            '65' => array( 'min_length' => '8', 'max_length' => '12'), //Singapore
            '421' => array( 'min_length' => '4', 'max_length' => '9'), //Slovakia
            '386' => array( 'min_length' => '8', 'max_length' => '8'), //Slovenia
            '677' => array( 'min_length' => '5', 'max_length' => '5'), //Solomon Islands
            '252' => array( 'min_length' => '5', 'max_length' => '8'), //Somalia
            '27' => array( 'min_length' => '9', 'max_length' => '9'), //South Africa
            '211' => array( 'min_length' => '1', 'max_length' => '15'), //South Sudan
            '34' => array( 'min_length' => '9', 'max_length' => '9'), //Spain
            '94' => array( 'min_length' => '9', 'max_length' => '9'), //Sri Lanka
            '249' => array( 'min_length' => '9', 'max_length' => '9'), //Sudan
            '597' => array( 'min_length' => '6', 'max_length' => '7'), //Suriname
            '268' => array( 'min_length' => '7', 'max_length' => '8'), //Swaziland
            '46' => array( 'min_length' => '7', 'max_length' => '13'), //Sweden
            '41' => array( 'min_length' => '4', 'max_length' => '12'), //Switzerland
            '963' => array( 'min_length' => '8', 'max_length' => '10'), //Syrian Arab Republic
            '886' => array( 'min_length' => '8', 'max_length' => '9'), //Taiwan China
            '992' => array( 'min_length' => '9', 'max_length' => '9'), //Tajikistan
            '255' => array( 'min_length' => '9', 'max_length' => '9'), //Tanzania
            '888' => array( 'min_length' => '1', 'max_length' => '15'), //Telecommunications for Disaster Relief (TDR)
            '66' => array( 'min_length' => '8', 'max_length' => '9'), //Thailand
            '389' => array( 'min_length' => '8', 'max_length' => '9'), //The Former Yugoslav Republic of Macedonia
            '670' => array( 'min_length' => '7', 'max_length' => '7'), //Timor-Leste
            '228' => array( 'min_length' => '8', 'max_length' => '8'), //Togo
            '690' => array( 'min_length' => '4', 'max_length' => '4'), //Tokelau
            '676' => array( 'min_length' => '5', 'max_length' => '7'), //Tonga
            '991' => array( 'min_length' => '1', 'max_length' => '15'), //Trial of a proposed new international service shared code
            '216' => array( 'min_length' => '8', 'max_length' => '8'), //Tunisia
            '90' => array( 'min_length' => '10', 'max_length' => '10'), //Turkey
            '993' => array( 'min_length' => '8', 'max_length' => '8'), //Turkmenistan
            '688' => array( 'min_length' => '5', 'max_length' => '6'), //Tuvalu
            '256' => array( 'min_length' => '9', 'max_length' => '9'), //Uganda
            '380' => array( 'min_length' => '9', 'max_length' => '9'), //Ukraine
            '971' => array( 'min_length' => '8', 'max_length' => '9'), //United Arab Emirates
            '44' => array( 'min_length' => '7', 'max_length' => '10'), //United Kingdom
            '1' => array( 'min_length' => '10', 'max_length' => '10'), //United States / Canada / Many Island Nations
            '878' => array( 'min_length' => '1', 'max_length' => '15'), //Universal Personal Telecommunication (UPT)
            '598' => array( 'min_length' => '4', 'max_length' => '11'), //Uruguay
            '998' => array( 'min_length' => '9', 'max_length' => '9'), //Uzbekistan
            '678' => array( 'min_length' => '5', 'max_length' => '7'), //Vanuatu
            '39' => array( 'min_length' => '1', 'max_length' => '11'), //Vatican
            '379' => array( 'min_length' => '1', 'max_length' => '11'), //Vatican
            '58' => array( 'min_length' => '10', 'max_length' => '10'), //Venezuela (Bolivarian Republic of)
            '84' => array( 'min_length' => '7', 'max_length' => '10'), //Viet Nam
            '681' => array( 'min_length' => '6', 'max_length' => '6'), //Wallis and Futuna
            '967' => array( 'min_length' => '6', 'max_length' => '9'), //Yemen
            '260' => array( 'min_length' => '9', 'max_length' => '9'), //Zambia
            '263' => array( 'min_length' => '5', 'max_length' => '10') //Zimbabwe
            );
        if(isset($phonenumber) and isset($cc) and is_numeric($cc) and is_numeric($phonenumber))
        {
            if(array_key_exists($cc,$intl_phone_criteria))
            {
                if(strlen($phonenumber) >= $intl_phone_criteria[$cc]['min_length'] and strlen($phonenumber) <= $intl_phone_criteria[$cc]['max_length'])
                {
                    return true;
                }
            }            
        }
        return false;
        
     }
     public function iszipcode($var_type,$var,$country)
     {
            switch($country)
            {
                case 'IN':
                $regex = "^[1-9]{1}[0-9]{5}$";//6 digit numeric code not starting with 0
                return $this->isregex($var_type,$var,$regex);
            }
     }
     public function alphanum($var_type,$var)
     {
         $regex = "^[0-9a-zA-Z ,\\.\\-_\\\s\\?\\!]+$";
         return $this->isregex($var_type,$var,$regex);
     }
     public function isempty($var_type,$var)
     {
         $filter_flag = FILTER_UNSAFE_RAW;
         
     }
     public function isnotempty($var_type,$var)
     {
         $filter_flag = FILTER_UNSAFE_RAW;
         $val = $this->validate_builder($var_type,$var,$filter_flag);
         return !empty($val);
         
     }
     public function isregex($var_type,$var,$regex)
     {
        /*switch ($var_type)
        {   case 'var':
                return filter_var($var, FILTER_VALIDATE_REGEXP, array("options"=> array("regexp"=>'/'.$regex.'/')));
        }*/
        $filter_flag = FILTER_VALIDATE_REGEXP;
        $filter_flag_arg = array("options"=> array("regexp"=>'!'.$regex.'!i'));
        return $this->validate_builder($var_type,$var,$filter_flag,$filter_flag_arg);
     }
     public function compare($operand1,$operand2,$operator)
     {
        if(isset($operand1) and isset($operand2) and isset($operator))
        {
            switch ($operator)
            {
                 case '==':
                     return ($operand1 == $operand2)?1:0;
                 case '!=':
                     return ($operand1 != $operand2)?1:0;
                 case '>':
                     return ($operand1 > $operand2)?1:0;
                 case '<':
                     return ($operand1 < $operand2)?1:0;
                 case '>=':
                     return ($operand1 >= $operand2)?1:0;
                 case '<=':
                     return ($operand1 <= $operand2)?1:0;
                 case '===':
                     return ($operand1 === $operand2)?1:0;
                 case '!==':
                     return ($operand1 === $operand2)?1:0;

            }
        }
        else
        return 0;
     }
     public function length_validate($var,$minlength,$maxlength)
     {
        $err = 0;
        if(isset($var))
        $var_len = strlen($var);
        
        if(isset($var_len))
        {
            if(isset($minlength))
            {   $minlength = $minlength;
                if($var_len < $minlength)
                {
                    $err = 1;
                }
            }
            if(isset($maxlength))
            {   $maxlength = $maxlength;
               if($var_len > $maxlength)
                {
                    $err = 1;
                }  
            }
            if(!$err)
            return true;
            else
            return false;
        }
     }
     public function password_validate($password,$validate_cond)
     {
            $reg = array();
            $result = array();
            $validation_status = array();
            $reg['uppercase_count'] = '/[A-Z]/';
            $reg['lowercase_count'] = '/[a-z]/';
            $reg['digit_count'] = '/[0-9]/';
            //https://kb.wisc.edu/page.php?id=4073 allowed special characters for passwords
            $reg['spec_chars_count'] = '/[!#\$\&%\'\"\(\)*+-.\/:;<=>?@\^_`{|}~]/';
           
            foreach($reg as $cond => $regex)
            {
                if(array_key_exists($cond,$validate_cond))
                {
                    $res = preg_match_all($regex,$password,$matcharray);
                    if(is_array($matcharray))
                    {
                        if(count($matcharray) < $validate_cond[$cond])
                        {
                            $validation_status[$cond] = false;
                        }
                        else 
                        $validation_status[$cond] = true;
                    }
                    else
                    $validation_status[$cond] = false;
                    
                }
                
                
            }
            return $validation_status;

     }
    
    public function validate_builder($var_type,$var,$filter_flag,$filter_flag_arg = NULL)
    {
        
        if($var_type != 'VAR')
        {   //$tmp_var_type = "$_".$var_type."[".$var."]";
            //if(isset($$tmp_var_type))
            //{
            $var_type = 'INPUT_'.strtoupper($var_type);
            if($var_type != 'INPUT_REQUEST') //Currently INPUT_REQUEST is not supported in filter_input so below workaround
            {
                $var_type = constant('INPUT_'.strtoupper($var_type));
                return $filter_flag_arg?filter_input(constant($var_type), $var, $filter_flag,$filter_flag_arg):filter_input($var_type, $var, $filter_flag);
            }
            
            else
            {
                if(isset($_GET[$var]))
                {
                    return $filter_flag_arg?filter_input(INPUT_GET, $var, $filter_flag,$filter_flag_arg):filter_input(INPUT_GET, $var, $filter_flag);
                }
                else
                {
                    return $filter_flag_arg?filter_input(INPUT_POST, $var, $filter_flag,$filter_flag_arg):filter_input(INPUT_POST, $var, $filter_flag);
                    
                }
                
            }
            
            //else
            //return FALSE;
        }
        else
        return $filter_flag_arg?filter_var($var,$filter_flag,$filter_flag_arg):filter_var($var,$filter_flag);
    }
    
    public function validate($cond,$var,$arg=NULL,$errormessage=NULL,$just_check_if_supported = false)
    {
        //if $just_check_if_supported = true , then just check if operation is supported by this validator, and return true or false
        //
        //$cond1 = explode('#',$cond);         //for date,zipcode etc we get extra string with # for example isdate#DDMMYYY#-
         $default_err_message = null;
        switch ($cond)
        {
             case 'isint':
                 if(!$just_check_if_supported)
                 {
                     $result['status'] = $this->isint('VAR',$var);
                     $default_err_message = "Please enter valid integer";
                     $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                     break;
                 }
                 else
                 return true;
                 
             case 'isfloat':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isfloat('VAR',$var);
                 $default_err_message = "Please enter valid Number";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'isemail':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isemail('VAR',$var);
                 $default_err_message = "Please enter valid EmailId";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'isip':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isip('VAR',$var);
                 $default_err_message = "Please enter valid IP Address";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
                 
             case 'ismac':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->ismac('VAR',$var);
                 $default_err_message = "Please enter valid MAC Address";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'isurl':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isurl('VAR',$var);
                 $default_err_message = "Please enter valid URL";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             //if date then it comes in format of isdate#<Data format DDMMYYYY or DDMONYYYY>#<Separator - or / or .>
             case 'isdate': //allowed formats #DDMMYYYY#- or #DDMMYYYY#/ or#DDMMYYYY#. or #DDMONYYYY#- or #DDMON-YYYY#/ or #DDMONYYYY#. where - , / , . are separators
                 if(!$just_check_if_supported)
                 {
                 if(isset($arg['format']) and isset($arg['separator']))
                 $result['status'] = $this->isdate('VAR',$var,$arg['format'],$arg['separator']);
                 else
                 $result['status'] = 0;
                 $default_err_message = 'Please enter valid date in '.$arg['format'].' format spearated by "'.$arg['separator'].'"';
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'intl_phone': //provide cc and phone number, optionally allowed cc list, this will validate the number for the country code and return true or false
                 if(!$just_check_if_supported)
                 {
                     if(isset($arg['input_cc']) and isset($var))
                     {   
                         
                        $allowed_cc_list = isset($arg['allowed_cc_list'])?$arg['allowed_cc_list']:NULL;
                        if(isset($allowed_cc_list))
                        {
                            if(!in_array($arg['input_cc'],$allowed_cc_list))//if provided cc is not part of allowed cc list
                            $result['status'] = 0;
                            else
                            $result['status'] = $this->intl_phone($var,$arg['input_cc']);
                        }           
                        else
                        $result['status'] = $this->intl_phone($var,$arg['input_cc']);
                         
                     }
                     else
                     $result['status'] = 0;  
                     $default_err_message = 'Please enter valid phone number';
                     $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                     break;
                 }
                 else
                 return true;
             case 'isalphanum':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->alphanum('VAR',$var);
                 $default_err_message = "Please enter valid Alphanumeric";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'iszipcode'://#CC where CC is two CHARACTER country code for which zip needs to be validated
                 if(!$just_check_if_supported)
                 {
                 if(isset($arg['countrycode']))
                 $result['status'] = $this->iszipcode('VAR',$var,$arg['countrycode']);
                 else
                 $result['status'] = 0; 
                 $default_err_message = 'Please enter valid zipcode/postalcode for country '.$arg['countrycode'];
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'isnotempty':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isnotempty('VAR',$var);
                 $default_err_message = "This is mandatory field";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'isempty':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isempty('VAR',$var);
                 $default_err_message = "This must be empty";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'compare_element'://only used if $just_check_if_supported
             case 'compare':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->compare($var,$arg['comparewith'],$arg['operator']);
                 $default_err_message = 'This value must be '.$arg['operator'].' '.$arg['comparewith'];
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'regex':
                 if(!$just_check_if_supported)
                 {
                 $result['status'] = $this->isregex('VAR',$var,$arg['expr']);
                 $default_err_message = "";//for regex default error message doesnt make sense
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'abslength':
                 if(!$just_check_if_supported)
                 {
                 $minlength = isset($arg['length'])?$arg['length']:null;
                 $maxlength = isset($arg['length'])?$arg['length']:null;
                 $result['status'] = $this->length_validate($var,$minlength,$maxlength);
                 $default_err_message = "Should be ".$arg['length']." characters";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'minlength':
                 if(!$just_check_if_supported)
                 {
                 $minlength = isset($arg['length'])?$arg['length']:null;
                 $maxlength = null;
                 $result['status'] = $this->length_validate($var,$minlength,$maxlength);
                 $default_err_message = "Should be atleast ".$arg['length']." characters";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'maxlength':
                 if(!$just_check_if_supported)
                 {
                 $maxlength = isset($arg['length'])?$arg['length']:null;
                 $minlength = null;
                 $result['status'] = $this->length_validate($var,$minlength,$maxlength);
                 $default_err_message = "Should be maximum of ".$arg['length']." characters";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'password':
                 if(!$just_check_if_supported)
                 {
                 if(isset($arg))
                 {
                    $res = $this->password_validate($var,$arg);//result is a array of different keys(like uppercase_count etc) with their validation status
                 }
                 if(is_array($res))
                 {
                     foreach($res as $cond => $status)
                     {
                         if(!$status)
                         {  
                             $err = false;
                             
                         }
                         //we show all the condition that are mentioned in json for password
                         //not just the ones that are failed
                         //becuase user need to know what all condition will make a valid password all at once
                         //if we show only ones where validation failed, user will end up getting different errors multiple times
                         //which will be annoying
                         if($default_err_message === null)
                         $default_err_message = "Password Must have :";
                         
                         switch ($cond)
                         {
                             case 'uppercase_count':
                                 $default_err_message .= "\n\tAtleast ".$arg[$cond]." UpperCase Letters";
                                 break;
                             case 'lowercase_count':
                                 $default_err_message .= "\n\tAtleast ".$arg[$cond]." LowerCase Letters";
                                 break;
                             case 'digit_count':
                                 $default_err_message .= "\n\tAtleast ".$arg[$cond]." Numbers";
                                 break;
                             case 'spec_chars_count':
                                 $default_err_message .= "\n\tAtleast ".$arg[$cond]." Special Characters from !#\$\&%&\'\"\(\)*+-.\/:;<=>?@\^_`{|}~";
                                 break;
                         }
                     }
                 }
                 if(!$err)
                 $result['status'] = true;
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
             case 'creditcard':
                 if(!$just_check_if_supported)
                 {
                 $creditcard = new \sskrepo\sklib\form\creditcard;
                 $cardnumber = $var;
                 $cardtype_to_match = isset($arg['cardtype_to_match'])?$arg['cardtype_to_match']:null;
                 $result = $creditcard->validate_card($cardnumber,$cardtype_to_match);
                 //creditcard_validate_card return a array with 'status','error','cardnumber','cardtype' keys
                 $default_err_message = "Invalid credit card number or Not matching with Credit card type selected";
                 $result['errormessage'] = isset($errormessage)?$errormessage:$default_err_message;
                 break;
                 }
                 else
                 return true;
            default:
                if(!$just_check_if_supported)
                 {
                 $result['status'] = true;//if operation is not supported just return true
                 $default_err_message = "Invalid credit card number or Not matching with Credit card type selected";
                 $result['errormessage'] = '';
                 break;
                 }
                 else
                 return false;
               
        }
        //if(!$just_check_if_supported)
        return $result;
    }
    
    public function filter($filter_type,$var_type,$var,$db='NULL')
    {
        switch ($filter_type)
        {
            case 'raw':
                $filter_flag = FILTER_UNSAFE_RAW;break;
            case 'int':
                $filter_flag = FILTER_SANITIZE_NUMBER_INT;break;
            case 'float':
                $filter_flag = FILTER_SANITIZE_NUMBER_FLOAT;break;
            case 'email':
                $filter_flag = FILTER_SANITIZE_EMAIL;break;
            case 'url':
                $filter_flag = FILTER_SANITIZE_URL;break;
            case 'htmlspecchars':
                $filter_flag = FILTER_SANITIZE_SPECIAL_CHARS;break;
            case 'mysqlescstring':
                return  mysqli_real_escape_string($db,$this->filter('raw',$var_type,$var_value));
        }   
        
        return $this->validate_builder($var_type,$var,$filter_flag);
       
    }
    /*$returnval =  filter_var($var, FILTER_VALIDATE_REGEXP, array("options"=> array("regexp"=>'!'.$regex.'!i'))) !== false;
            return($returnval);*/
    /*REGEXES
     *      'date' => "^[0-9]{4}[-/][0-9]{1,2}[-/][0-9]{1,2}\$",
            'amount' => "^[-]?[0-9]+\$",
            'number' => "^[-]?[0-9,]+\$",
            'alfanum' => "^[0-9a-zA-Z ,.-_\\s\?\!]+\$",
            'not_empty' => "[a-z0-9A-Z]+",
            'words' => "^[A-Za-z]+[A-Za-z \\s]*\$",
            'phone' => "^[+][0-9]{1,3}[0-9]{5,10}\$",
            'zipcode' => "^[1-9][0-9]{3}[a-zA-Z]{2}\$",
            'plate' => "^([0-9a-zA-Z]{2}[-]){2}[0-9a-zA-Z]{2}\$",
            'price' => "^[0-9.,]*(([.,][-])|([.,][0-9]{2}))?\$",
            '2digitopt' => "^\d+(\,\d{2})?\$",
            '2digitforce' => "^\d+\,\d\d\$",
            'anything' => "^[\d\D]{1,}\$",
            'username' => "^[\w]{3,32}\$"
     */
}
?>