<?php

namespace sskrepo\sklib;
class misc
{  
    public function getScriptOutput($path, $print = FALSE)
    { 
        //phpscriptoutput_to_variable
        ob_start();

        if( is_readable($path) && $path )
        {
            include $path;
        }
        else
        {
            return FALSE;
        }

        if( $print == FALSE )
            return ob_get_clean();
        else
            echo ob_get_clean();
    }
    
    public function include_allphp_indir($folder)
    {    
        foreach(glob("{$folder}/*.php") as $filename)
        {   
                include($filename);
        }
    }
}
?>