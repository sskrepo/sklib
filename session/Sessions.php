<?php

/*
 * Uses redbeanphp ORM for persistence functionality.
 * Uses Database for storing sessions , Doesnt use $_SESSION at all
 * Assumes below defaults
 * expiry_seconds of session defaults to 600 seconds , change it by passing desired value to create()
 * 
drop table sessions;
create table sessions(
id INT UNSIGNED NOT NULL AUTO_INCREMENT,
sessionid VARCHAR(100) UNIQUE NOT NULL,
status VARCHAR(10),
create_phptime INT,
expiry_seconds INT,
lastactive INT,
username INT,
sessdata VARCHAR(1000),
CONSTRAINT PK_DOMAINS_ID PRIMARY KEY(id)
)ENGINE=INNODB;
 * 
 * 
 * 
 * 
 * all session data is stored in sessdata as key value pairs in below format
 * 
 * $key1:):$value1:|:$key2:):$value2:|:$key3:):$value3 and so on 
 * 
 * ************
 * USAGE
 * ************
include ('/var/www/newsso/config/load_parameters.php');
$session = \R::dispense('sessions');
$session->create('SSOCOOKIE');
$session->setdata('username','sravans.hice@gmail.com');
$session->save();

if($exsession = $session->load('SSOCOOKIE'))
{
    $exsession->setdata('username','sskmie@gmail.com');
    $exsession->rmdata('role','user');
    $exsession->destroy();
    //$exsession->save();
    echo $exsession->getdata('username');
    echo $exsession->getdata('role');
    
}
 * ***********************************************
 */
namespace sskrepo\sso\model;
class Sessions extends \RedBeanPHP\SimpleModel
{
    public $_reg;
    public $data;
    public function __construct()
    {
       //$this->_reg = \sskrepo\sklib\registry\registry::getInstance();
        
    }
    public function create($sessioname,$expiryseconds = 600)
    {
        //generate random sessid
        /*
         * 
         * if(radmonsessid exists)
         * {generate new one and check again}
         * set cookie name as session name
         * set session id as cookie value
         */
        $newsessid = $this->generate_sessid();
        $existingsession = \R::find('sessions','sessionid = ?',[$newsessid]);
        
        while($newsessid == $existingsession->sesssionid)
        {
            $newsessid = $this->generate_sessid();
            $existingsession = \R::find('sessions','sessionid = ?',[$newsessid]);
        }
        $this->bean->sessionid = $newsessid;
        
        //set cookie , name as sessionname and value as sessid
        setcookie($sessioname, $this->sessionid);
        $this->bean->create_phptime = time();
        $this->bean->expiry_seconds = $expiryseconds;
        $this->bean->status = 'active';
        $this->bean->lastactive = time();
        \R::store($this->bean);

    }
    public function destroy()
    {
        
        \R::trash($this->bean); //deletes DB record and also makes $this->bean->id as 0
        //But without making id to NULL, if someone does $bean->save, after this, it will still save the data with a new id in database, which we should prevent
        //So setting the id to NULL, so that any save after this will only result in a error.
        $this->bean = NULL;
        
    }
    public function load($sessioname) //Checks if cookie with that name exists, if yes load the session, if not then check if $_REQUEST['sessioname'] exits and use it
    {
        if(isset($_COOKIE[$sessioname]))
        $sessionid = $_COOKIE[$sessioname];
        elseif(isset($_REQUEST[$sessioname]))
        $sessionid = $_REQUEST[$sessioname];
        
        if(!empty($sessionid))
        {
            $sessbean = \R::find('sessions','sessionid = ?',[$sessionid]); //it returns array so we have to use foreach
            foreach($sessbean as $s)
            {
                if($s->status == 'active')
                {
                    $this->bean->lastactive = time();
                    return $s;    
                }
            }
            return 0;
        }
        else
        return 0;
        
    }
   
    public function close(){}
    public function save()
    {
        \R::store($this->bean);
    }
    public function generate_sessid()
    {
        return \sskrepo\sklib\sec\sec::get_random_ssl_string(15);
    }
    
    public function issessionactive()
    {
        if(isset($this->sessionid))
	{	$lastactive = $this->bean->lastactive;
		if(empty($lastactive))
		{	return 0;
		}
		elseif(($lastactive + $this->bean->expiry_seconds < time()))       //session expiry validation)
		{	return 0;
		}
		else  //session is valid and NOT expired, // update lasactive everytime we access the session
		{	
                        $this->bean->lastactive = time();
                        $this->save();
			return 1;
		}
	}
        else
        {
           return 0;
        }
    }
    public function data_to_array()
    {
        if(isset($this->bean->sessdata))
        {
            $tempdata = explode(':|:',$this->bean->sessdata);
            foreach($tempdata as $value)
            {
                $keyvaluepair = explode(':):',$value);
                $data[$keyvaluepair[0]] = $keyvaluepair[1];
            }
            return $data;
        }
        else
        return array();//return emtpy array;
    }
    public function array_to_data($data)
    {
        if(is_array($data))
        {
            foreach($data as $key=>$value)
            {
                if(isset($sessdata))
                $sessdata = $sessdata.':|:'.$key.':):'.$value;
                else
                $sessdata = $key.':):'.$value;
                
            }
            return $sessdata;
        }
        
    }
    public function setdata($key,$value)
    {
        $data = $this->data_to_array();
        $data[$key] = $value;
        $this->bean->sessdata = $this->array_to_data($data);
    }
    
    public function rmdata($key,$value)
    {
        $data = $this->data_to_array();
        unset($data[$key]);
        $this->bean->sessdata = $this->array_to_data($data);
    }
    public function getdata($key = NULL)
    {
        $data = $this->data_to_array();
        if(!empty($key))
        return $data[$key];
        else
        return $data;
        
    }

}
?>