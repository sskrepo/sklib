<?php
namespace sskrepo\sklib\file;
class filedir
{
     public function rmdir($dir)            //remove directory and all its subdirectories and files recursively
      { 
       if (is_dir($dir)) { 
         $objects = scandir($dir); 
         foreach ($objects as $object) 
         { 
           if ($object != "." && $object != "..") 
           { 
             if (filetype($dir."/".$object) == "dir") 
             $this->rmdir($dir."/".$object); 
             else 
             unlink($dir."/".$object); 
           } 
         } 
         reset($objects); 
         rmdir($dir); 
       } 
     }
}



?>

