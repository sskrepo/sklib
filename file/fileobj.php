<?php
/*Find the name of "input type=file" element so that we can use that in $_FILES["elementname"]["name"] etc..so that this can work for
any fileupload html irrespective of what name they used in the HTML for imput element. This is done in constructor of this class

Check if single file upload OR multiple file upload
If multiple file upload repeat all actions on all the files uploaded
Check if file uplaod is having any error, if yes return appropriate message
Move the files uploaded from temp location to required location by taking this location as user input
While moving make sure moved file is as same name as source(as on client $_FILES[x]['name']
Function to return file type

DEPENDENCIES
sk_array_array - array_dim_count function
make sure below parameters in php.ini are set 
post_max_size  - greater than total files sizes being uploaded
upload_max_filesize - greater than total files sizes being uploaded

USAGE
$file = new fileobj();
$result = $file->save_uploaded_files("<location on server where file(s) need to be saved>");
echo $result;

Related Sample html:
<html>
<head>
<title>Administration - upload new files</title>
</head>
<body>
<h1>Upload new news files</h1>
<form action='/otest/filetest.php' method="post" enctype="multipart/form-data"/>
<div>
<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
<label for="userfile">Upload a file:</label>
<input type="file" multiple name="userfile[]" id="userfile"/>
<input type="submit" value="Send File"/>
</div>
</form>
</body>
</html
//include("D:\Apache\htdocs\gincludes\lib\libobj\array.php");
*/
namespace sskrepo\sklib\file;
class fileobj
{
    public $elemname,$multiupload,$numfiles,$filename,$arr;
    
    public function __construct()
    {
        $this->arr = new \sskrepo\sklib\arrays\arrays();
    }

    //methods
    public function numfiles() //For Upload
    { 
      if(!empty($_FILES))
      {
      $keys = array_keys($_FILES);
      $this->elemname = $keys[0]; //name attribute of input element name in HTML for type=file
      //echo "elemname".$this->elemname;
      
      if($this->arr->array_dim_count($_FILES) == 3)
      {   $this->multiupload = 'TRUE';
      }
      else
      {   $this->multiupload = 'FALSE';
      }
      //$this->multiupload = array_dim_count($_FILES); //to see if single file uplaod or multiple file uploads by calclating $_FILES dimenstion if 3 multiple files , if 2 single file
      //echo $this->multiupload;
      $this->numfiles = count($_FILES[$this->elemname]['name']);
      //echo $this->multiupload;
      }
    }

    public function save_uploaded_files($destlocation)   //For Upload
    { 
      if(!empty($_FILES))
      { 
      $this->numfiles();
      if($this->multiupload == 'TRUE')              //multiple file upload
      {
      for($filenum=0;$filenum<$this->numfiles;$filenum++)
      {
        if($_FILES[$this->elemname]['error'][$filenum] > 0)
        {     $actfilenum = $filenum + 1; //File number starts from 0 so while displaying we need to show +1 of that number for any error so that user understands
          switch ($_FILES[$this->elemname]['error'][$filenum])
          {
          case 1:  return "File $actfilenum exceeded upload_max_filesize";
          break;
          case 2:  return "File $actfilenum exceeded max_file_size";
          break;
          case 3:  return "File $actfilenum only partially uploaded";
          break;
          case 4:  return "No $actfilenum file uploaded";
          break;
          case 6:  return "Cannot upload file $actfilenum : No temp directory specified";
          break;
          case 7:  return "Upload failed $actfilenum : Cannot write to disk";
          break;
          }
        }
        $upfile = $destlocation.'/'.$_FILES[$this->elemname]['name'][$filenum] ;
        if (is_uploaded_file($_FILES[$this->elemname]['tmp_name'][$filenum]))
        {
            if (!move_uploaded_file($_FILES[$this->elemname]['tmp_name'][$filenum], $upfile))
            {
            return 'Problem: Could not move file to destination directory : '.$_FILES[$this->elemname]['name'][$filenum];
            }
        }
        else
        {
            return 'Problem: Possible file upload attack. Filename: '.$_FILES[$this->elemname]['name'][$filenum];
        }
        $this->filename[$filenum] = $upfile;
      }
      }
      else        //Single file upload
      {   //echo "in single file";
        if ($_FILES[$this->elemname]['error'] > 0)
        {     
          switch ($_FILES[$this->elemname]['error'])
          {
          case 1:  return "File exceeded upload_max_filesize";
          break;
          case 2:  return "File exceeded max_file_size";
          break;
          case 3:  return "File only partially uploaded";
          break;
          case 4:  return "No file uploaded";
          break;
          case 6:  return "Cannot upload file: No temp directory specified";
          break;
          case 7:  return "Upload failed : Cannot write to disk";
          break;
          }
        }
        $upfile = $destlocation.'/'.$_FILES[$this->elemname]['name'];
        if (is_uploaded_file($_FILES[$this->elemname]['tmp_name']))
        {
            if (!move_uploaded_file($_FILES[$this->elemname]['tmp_name'], $upfile))
            {
            return 'Problem: Could not move file to destination directory : '.$_FILES[$this->elemname]['name'];
            }
        }
        else
        {
            return 'Problem: Possible file upload attack. Filename: '.$_FILES[$this->elemname]['name'];
        }
        $this->filename = $upfile;
      }
      return 0;
      }

    }
      public function save_files($destlocation)    //Just Alias for save_uploaded_files for backward compatibility as many php files were using save_files
      { $this->save_uploaded_files($destlocation);
      }
      public function rmdir($dir)            //remove directory and all its subdirectories and file recursively
      { 
       if (is_dir($dir)) { 
         $objects = scandir($dir); 
         foreach ($objects as $object) 
         { 
           if ($object != "." && $object != "..") 
           { 
             if (filetype($dir."/".$object) == "dir") 
             $this->rmdir($dir."/".$object); 
             else 
             unlink($dir."/".$object); 
           } 
         } 
         reset($objects); 
         rmdir($dir); 
       } 
     }
}
?>