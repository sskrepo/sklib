<?php
//USAGE

//For ZIP
//$zi = new zipit();
//$result = $zi->zipper("zip","D:\Apache\htdocs\one","D:\Apache\htdocs\doc\DOC3.zip");  //zipper($action,$source,$dest)
//echo $result;

//For Unzip
//$zi = new zipit();
//$result = $zi->zipper("unzip","D:\Apache\htdocs\doc\DOC3.zip","D:\Apache\htdocs\one");  //zipper($action,$source,$dest)
//echo $result;
namespace sskrepo\sklib\file;
class zipit
{      //UNZIP is done by itself, but for zip this depends on HZIP class which is also defined in this file
    
    public function __construct()
    {
        $this->z = new \sskrepo\sklib\file\HZip();
    }

    public function zipper($action,$source,$dest)
    {
      //$action - zip/unzip
      //$source - Folder(if zip)/file name(if unzip)
      //$dest - location(directory/folder)
      if(isset($action,$source,$dest))
      {   
          if($action=="zip")
          {   
            if(is_dir($source))
             {   
                
                $this->z->zipDir($source,$dest);
                $result = 1;
                return $result; 
             }
             else
             {    
                  $result = "Error:When action is zip, source should be a folder and dest should be file";
                  return $result;
             }
          }
          elseif($action=="unzip")
          {   
            if(is_file($source) and is_dir($dest))
             {  
                $zip = new ZipArchive;
                $zip->open($source);
                //echo "file opened";
                $zip->extractTo($dest);
                $zip->close();
                $result = 1;
                return $result; 
             }
             else
             {  $result = "Error:When action is unzip, source should be a file and dest should be folder";
             }

          }
          else
          { $result = "Error: Unknown action value. Should be zip/unzip";
            return $result;
          }

      }
      else
      {
        $result = "Error: All three parameters should be passed - action,source,dest";
        return $result;

      }

    }
}

?>