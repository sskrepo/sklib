<?php
namespace sskrepo\sklib\file;
class HZip    //This class is only to ZIP folder and all its contents. This will not handle UNZIP, use sk_zipit instead for both functionality
{ 
  /** 
   * Add files and sub-directories in a folder to zip file. 
   * @param string $folder 
   * @param ZipArchive $zipFile 
   * @param int $exclusiveLength Number of text to be exclusived from the file path. 
   */ 
  private static function folderToZip($folder, &$zipFile, $exclusiveLength) { 
    $handle = opendir($folder); 
    while (false !== $f = readdir($handle)) { 
      if ($f != '.' && $f != '..') { 
        $filePath = "$folder/$f"; 
        // Remove prefix from file path before add to zip. 
        $localPath = substr($filePath, $exclusiveLength);
        if (is_file($filePath)) { 
          $zipFile->addFile($filePath, $localPath); 
        } elseif (is_dir($filePath)) { 
          // Add sub-directory. 
          $zipFile->addEmptyDir($localPath); 
          self::folderToZip($filePath, $zipFile, $exclusiveLength); 
        } 
      } 
    } 
    closedir($handle); 
  }
   public static function zipDir($sourcePath, $outZipPath) 
  { 
    $pathInfo = pathInfo($sourcePath); 
    $parentPath = $pathInfo['dirname']; 
    $dirName = $pathInfo['basename']; 

    $z = new ZipArchive(); 
    $z->open($outZipPath, ZIPARCHIVE::CREATE); 
    //$z->addEmptyDir($dirName); 
    self::folderToZip($sourcePath, $z, strlen("$sourcePath/"));   //This is to make sure we strip out $sourcepath from the patch we use in addFile in foldertozip function. This is to make sure that when we zip say D:\apache\one folder we dont see one folder in zip file instead we only see contents of one folder. This is required becuase we are zipping conents of docx unzip directory so that when we zip it we can just rename the resulting zip file to docx.
    $z->close(); 
  } 
}
?>