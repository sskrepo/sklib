<?php
namespace sskrepo\sklib\sec;
class sec
{
    public function get_random_ssl_string($string_size)
    {
        return bin2hex(openssl_random_pseudo_bytes($string_size));
    }

    public function make_ssl_request($url,$method,$data)
    {
        //url shoud have http or https sepcifically if it is https
        //method can either be POST or GET only
        //data is POST or GET data to be sent 
        //example data = 'user=ssk&role=none&publickey=39ed18e9475ca6f274b1&md5value=1a22c9cd78f44d97c4862e452e95281a';
      
        $contextOptions = array(
                'http'=> array( 
            'method'=>   "$method", 
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                        . "Content-Length: " . strlen($data) . "\r\n",
                    'content' => $data,
            'ssl' => array(
                'verify_peer'   => true,
                'verify_depth'  => 5
            )));
        $sslContext = stream_context_create($contextOptions);
        $result = file_get_contents($url, NULL, $sslContext);
        return $result;
    }
    
 

    public function encrypt_decrypt($action, $string,$secret_key,$secret_iv)
    {
        
        /* USAGE 
         * $secret_key and $secret_iv can be any random strings;
        $plain_txt = "This is my plain text";
        echo "Plain Text = $plain_txt\n";

        $encrypted_txt = encrypt_decrypt('encrypt', $plain_txt);
        echo "Encrypted Text = $encrypted_txt\n";

        $decrypted_txt = encrypt_decrypt('decrypt', $encrypted_txt);
        echo "Decrypted Text = $decrypted_txt\n";

        if( $plain_txt === $decrypted_txt ) echo "SUCCESS";
        else echo "FAILED";

        echo "\n";
         * 
         */
       if(isset($action) and isset($string) and isset($secret_key) and isset($secret_iv))
       {
            $output = false;

            $encrypt_method = "AES-256-CBC";
            //$secret_key = 'secretsauce';
            //$secret_iv = 'secretsauceiv';

            // hash
            $key = hash('sha256', $secret_key);

            // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
            $iv = substr(hash('sha256', $secret_iv), 0, 16);

            if( $action == 'encrypt' ) {
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
            }
            else if( $action == 'decrypt' ){
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }

            return $output;
       }
    }
}


?>