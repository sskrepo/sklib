
/**
 * Namespace SSKREPO_FORM_JS , Main Global Object that is used for Form Validation using Javascript
 * @namespace SSKREPO_FORM_JS
 * @requires jQuery1.11.3 or more
 * @requires js.cookie.js
 * This validator depends on jquery so please load jquery before this
 * @example
 * <script src="http://code.jquery.com/jquery-1.11.3.js"></script>
 * @author Sravan Kumar S
 */

var SSKREPO_FORM_JS = (function(){
    var my = {};
    
    /**
     * @member {Object} formerrors
     * Object to keep count of errors in the form being validated, stored in formerror.error_count 
     * Closure scope available for all objects within this namespace
     * @memberof SSKREPO_FORM_JS
     */
    var formerrors = new Array();                                                                                                                        
    formerrors.error_count = '0';//;
    /**
     * @member {array} element_event_array 
     * Array of events for each element, indexed by elementid
     * @memberof SSKREPO_FORM_JS
     */
    my.element_event_array = new Array();
    /**
     * @member {array} required_element_array 
     * Deprecated not being used any more
     * used to store elementid that is beind validated for 'required' condition, to prevent infinite loops between element's required conditions
     * @memberof SSKREPO_FORM_JS
     */
    my.required_element_array = new Array();
    /**
     * @member {array} refresh_element_array 
     * Deprecated Not being used any more
     * @memberof SSKREPO_FORM_JS
     */
    my.refresh_element_array = new Array()
    /**
     * @member {array} elements_that_I_depend_on 
     * Array of elements that a element id depending on, indexed with elementid
     * @memberof SSKREPO_FORM_JS
     */
    my.elements_that_I_depend_on = new Array();
    /**
     * @member {array} required_valid_element_array
     * Used to store "required=>"valid" elements list for specific element 
     * @memberof SSKREPO_FORM_JS
     */
    my.required_valid_element_array = new Array(); 
    /**
     * @member {array} elements_that_depend_onme
     * Array of elements that depend on a specific element, indexed with elementid
     * @memberof SSKREPO_FORM_JS
     */
    my.elements_that_depend_onme = new Array();
    my.cyclic_dependencies_array = new Array();//used to store cyclic dependency lists;
    /**
     * @member {array} element_error_array 
     * Array of errors for each element , indexed by elementid
     * @memberof SSKREPO_FORM_JS
     */
    my.element_error_array = new Array();
    /**
     * @member {array} plugins
     * Array of Plugins loaded by plugin_loader();This is used across application in different modules
     * @memberof SSKREPO_FORM_JS
     * @see SSKREPO_FORM_JS.plugin_loader
     */
    my.plugins = new Array();
    /**
     * @member {array} plugins_default
     * Default plugin objects for all plugins, if user is not providing external plugins for any plugin_type, default plugin objects are loaded from this array to my.plugins and used across application
     * @memberof SSKREPO_FORM_JS
     */
    my.plugins_default = new Array();
    /**
     * @member {array} plugins_supported
     * Array of all supported plugins
     * @memberof SSKREPO_FORM_JS
     */
    my.plugins_supported = new Array();
    
    //Supported Plugins
    my.plugins_supported.ERROR_HANDLER = 1;
    my.plugins_supported.VALIDATOR = 1;
    

/**
 * Loads validation criteria from my.params, Retruns validation json string
 * @function load_validate_crit
 * @memberof SSKREPO_FORM_JS
 * @returns {JSON} myparams.validate_crit_json3 
 */
function load_validate_crit()
{
    
    if(my.params.hasOwnProperty('validate_crit_json3'))
    {
        return my.params['validate_crit_json3'];
    }
}


/**
 * Server side(PHP) sends data like validation criteria, element errors etc in the form of parameters
 * This function loads that json parameter string either from cookie or from this.params_json string
 * so that this.params will have an array of paramteres passed from PHP
 * @function load_params
 * @memberof SSKREPO_FORM_JS
 * @returns {array} Returns params array and also sets this.params to array of parameters passed from Server side(PHP)
 */
my.load_params = function()
{
    
    
    if(!my.hasOwnProperty('params_json'))
    {
         if(Cookies.get('sskrepo_js_param') !== '')
         {
             my.params = SSKREPO_JSLIB.string.read_json_cookie('sskrepo_js_param');
         }
         //my.params = cookie_to_validatecrit('sskrepo_js_param');
    }
    else if(typeof my.params_json !== 'undefined')
    my.params = my.params_json;//even though we encoded JSON in PHP, when we assign it to a property of "my", it is stored as object, no need to parse it again
    else
    my.params = null;

    return my.params;//my.params will be array if valid json, else null;
  
}
/**
 * 
 * Read this.params and return value of param_name
 * @function get_param
 * @memberof SSKREPO_FORM_JS
 * @param {string} param_name
 * @returns {string|array} Value of a parameter reuested
 */
my.get_param = function(param_name)
{
    if(my.params !== null && SSKREPO_JSLIB.obj.isset(param_name))
    {
        if(my.params.hasOwnProperty(param_name))
        {
            return my.params.hasOwnProperty(param_name);
        }
        else
        return null;
    }
    else
    return null;
}

/**
 * Main function for processing validation JSON
 * Mapping selector level events to elements
 * creating event based validations for elements
 * Builds element dependencies
 * Builds Cyclic Dependencies
 * Checks for any errors from Server side(PHP) and call display plugin to display the errors if any
 * @function events_validator
 * @memberof SSKREPO_FORM_JS
 * @returns none
 */
my.events_validator = function()//main events validator function//accepts plugin name, if it is set
{
    my.load_params();
    my.plugin_loader();//load provided plugins if valid, else load default plugins
    
    var form,json_validation_array;
    json_validation_array = load_validate_crit();

    var selectorevents_validation_array = new Array();
    var formevents_validation_array = new Array();
    if(SSKREPO_JSLIB.obj.isset(json_validation_array))
    {
        for(form in json_validation_array)
        {
            var formid = '#'+form, event,selector_array,selector,condition_array,condition,selector_events,i;

            selector_array = SSKREPO_JSLIB.obj.isset(json_validation_array[form])?json_validation_array[form]:null;
            if(selector_array != null)
            {
                for(selector in selector_array)
                {
                    condition_array = selector_array[selector];
                    if(SSKREPO_JSLIB.obj.isset(condition_array))
                    {
                        for(condition in condition_array)
                        {
                            if(SSKREPO_JSLIB.obj.isset(condition_array[condition]['events']))
                            {
                                selector_events = condition_array[condition]['events'].trim().split(',');
                                if(SSKREPO_JSLIB.obj.isset(selector_events))
                                {
                                    var sevent;
                                    for (sevent in selector_events)
                                    {   //remove any uneccessary spaces
                                        selector_events[sevent] = selector_events[sevent].trim();
                                    }
                                }
                            }
                            else
                            {
                                //If No events specified for a condition,Use default events as onchange and onsubmit
                                selector_events = ["onchange","onsubmit"];
                            }
                            for(i in selector_events)
                            {   event = selector_events[i];
                                if(event != 'onsubmit')//onsubmit is form level event, not for individual selectors/elements
                                {
                                        var t_event = new Array()
                                        t_event[condition] = condition_array[condition];  
                                        var t_selector = new Array();
                                        t_selector[event] = t_event;
                                        var t_formid = new Array();
                                        t_formid[selector] = t_selector;

                                    if(SSKREPO_JSLIB.obj.isset(selectorevents_validation_array[formid]))
                                    {
                                        if(SSKREPO_JSLIB.obj.isset(selectorevents_validation_array[formid][selector]))
                                        {
                                            if(SSKREPO_JSLIB.obj.isset(selectorevents_validation_array[formid][selector][event]))
                                            selectorevents_validation_array[formid][selector][event][condition] = condition_array[condition];
                                            else
                                            selectorevents_validation_array[formid][selector][event] = t_event;
                                        }
                                        else                                    
                                        selectorevents_validation_array[formid][selector] = t_selector;
                                    }
                                    else
                                    {
                                        selectorevents_validation_array[formid] = t_formid;
                                    }                                           
                                }
                                else
                                {
                                   //for form level events like onsubmit, more other form level events can be added later
                                    var t_selector = new Array()
                                    t_selector[condition] = condition_array[condition];
                                    var t_event = new Array();
                                    t_event[selector] = t_selector;

                                    if(SSKREPO_JSLIB.obj.isset(formevents_validation_array[formid]))
                                    {
                                        if(SSKREPO_JSLIB.obj.isset(formevents_validation_array[formid][event]))
                                        {
                                            if(SSKREPO_JSLIB.obj.isset(formevents_validation_array[formid][event][selector]))
                                            {
                                               formevents_validation_array[formid][event][selector][condition] =  condition_array[condition];
                                            }
                                            else
                                            formevents_validation_array[formid][event][selector]= t_selector;
                                        }
                                        else
                                        {
                                            var t_formid = new Array();
                                            t_formid[event] = t_event;   
                                            formevents_validation_array[formid][event]= t_event;                                    
                                        }                                  
                                    }
                                    else
                                    {                                               
                                        var t_formid = new Array();
                                        t_formid[event] = t_event;   
                                        formevents_validation_array[formid]= t_formid;
                                    }

                                }
                            }

                        }
                    }
                }
            }  
        }
        my.selectorevents_validation_array = selectorevents_validation_array;
        my.formevents_validation_array = formevents_validation_array;
        //set selector level validation events like onblur, onchange etc
        if($.isArray(selectorevents_validation_array))
        {
            for(formid in selectorevents_validation_array)
            {

                var selector_array = selectorevents_validation_array[formid], selector;

                for(selector in selector_array)
                {
                    var selector_string = formid+' '+selector;
                    var selector_validation_array = selector_array[selector];
                    
                    $(selector_string).each(set_selector_validation_events(selector_validation_array));
                }
            }
        }
        //set form level validation events like onsubmit
        if($.isArray(formevents_validation_array))
        {
            for(formid in formevents_validation_array)
            {
                var form_event_array = formevents_validation_array[formid], form_event,form_event_selectors_array;
                var form_event_selector;
                for(form_event in form_event_array)
                {
                    form_event = form_event.split('on');//remove on from event names as in jquery onblur is set as .on('blur')
                    form_event = form_event['1'];
                   $(formid).on(form_event,formevents_validator(form_event,form_event_array));

                }

            }

        }
        //set required-Refresh events based on required-valid dependencies
        /*if for element A "required" condition is set as below, that B should be valid for A to be valid
        /*
         * A
         * {
         *      "required":
         *      {
         *          "B" : "valid"
         *      }
         * }
         *
         *Now in this case, we add a required->valid event to B, as whenever B changes, trigger a validation on A, so that A can be revalidated, with new state of B
         *
         * required event automatically added to B would be something like
         * B
         * {
         *      "required":
         *      {
         *          "A" : "refresh"
         *      }
         * } */
        //if B is already having a "required"->"valid" on A dont add refresh it will cause problems
        //if not then add refresh, below code checks that
        //below code checks for each required_element list of A, if any required_element is having a reverse
        //valid condition for A, if yes dont add refresh, if not add refresh
        //
        /*var el,req_el,req_elid;
        for(el in my.elements_that_I_depend_on)
        {
            for(req_elid in my.elements_that_I_depend_on[el])
            {

                if(my.elements_that_I_depend_on.hasOwnProperty(req_elid))
                {
                    my.required_valid_element_array[req_elid] = required_valid_element_list(my.elements_that_I_depend_on[req_elid])

                    if(my.required_valid_element_array[req_elid] !== false)
                    {
                        if(my.required_valid_element_array[req_elid].hasOwnProperty(el))
                        {
                            //req_el is already having "valid" event for el , so dont add a refresh for el again, it will go for infinite loops
                        }
                        else
                        {   //if B is having required elements and valid conditions, but none on A
                            var req_el = $('#'+req_elid);
                            if(req_el)
                            {
                                var req_cond = new Array(),req_arg = new Array();
                                req_cond['op'] = 'refresh';
                                req_arg[el] = 1;
                                req_cond['arg'] = req_arg;
                                req_el.on('change',validate_and_adderror(req_cond,null));//add on change event with required->valid condition
                            }
                        }
                    }
                    else
                    {   //if B is having required elements but no "valid" conditions in it
                        var req_el = $('#'+req_elid);
                        if(req_el)
                        {
                            var req_cond = new Array(),req_arg = new Array();
                            req_cond['op'] = 'refresh';
                            req_arg[el] = 1;
                            req_cond['arg'] = req_arg;
                            req_el.on('change',validate_and_adderror(req_cond,null));//add on change event with required->valid condition
                        }

                    }

                }
                else
                {   //If B is not having required elements at all 
                    var req_el = $('#'+req_elid);
                    if(req_el)
                    {
                        var req_cond = new Array(),req_arg = new Array();
                        req_cond['op'] = 'refresh';
                        req_arg[el] = 1;
                        req_cond['arg'] = req_arg;
                        req_el.on('change',validate_and_adderror(req_cond,null));//add on change event with required->valid condition
                    }
                }
            }

        }*/
       build_element_dependents_list();//builds my.elements_that_depend_onme array;
       var elemid;
       for(elemid in my.elements_that_depend_onme)
       {
           var elem = $('#'+elemid);
           if(SSKREPO_JSLIB.obj.isset(elem))
           {
                elem.on('change',refresh_dependent_elements(elemid,null));
           }
       }
       
       //build elements that I depend on for valid condition
       var elementid;
       for(elementid in my.elements_that_I_depend_on)
       {
          my.required_valid_element_array[elementid] = required_valid_element_list(my.elements_that_I_depend_on[elementid]);    
       }
       //build cyclic dependencies array
       
       find_cyclic_dependencies(my.required_valid_element_array,null);
       
       //Load validation errors from PHP if any
       load_validation_errors_from_php();
       var elid;
       if(SSKREPO_JSLIB.obj.isset(my.element_error_array))
       {
           if(SSKREPO_JSLIB.obj.numberOfKeysinObject(my.element_error_array) > 0)//if any errors from PHP, display them onload
           {
                for(elid in my.element_error_array)
                {
                    var elem = $('#'+elid);
                    my.on_error_change(my.element_error_array,elem);
                }
           }
        }
    }
};
/**
 * 
 * Load errors from Server side(PHP) to {@link SSKREPO_FORM_JS.element_error_array}
 * @function load_validation_errors_from_php
 * @memberof SSKREPO_FORM_JS
 * @returns none
 */

function load_validation_errors_from_php()
{
    if(my.params.hasOwnProperty('element_error_array_json'))//only if 'element_error_array_json' cookie is set
    {
        
        my.element_error_array = my.params['element_error_array_json'];
    }
}
/**
 * Validates Array of validations for all elements at form level, for example all validations for event "onsubmit"
 * Called during form level events
 * @function formevents_validator
 * @memberof SSKREPO_FORM_JS
 * @returns none
 */

function formevents_validator(form_event,form_event_array)
{
    //validation for all form level events
    var form_event = form_event;
    var form_event_array = form_event_array;

    return function(event)
    {
        var this_form = $(this);
        
        if(form_event == 'submit')//we remove on from onsubmit before, so use only event name without 'on';
        {
            form_onsubmit_validator(this_form,form_event_array,event);       
        }
    }

}
/**
 * 
 * Validator for processing all validations for onsubmit event
 * Calls validate_and_adderror for each element based on selectors for which validations are set
 * @function form_onsubmit_validator
 * @memberof SSKREPO_FORM_JS
 * @param {jQuery} form Form Object which is submitted 
 * @param {string} form_event Form elevel event, example onsubmit
 * @param {array} form_event_array array of all validations for tirggerred form level event
 * @returns none
 * 
 */
function form_onsubmit_validator(form,form_event_array,event)
{
    var form_event,selector_array,selector,error_count,condition,errormessage,cond;

    formerrors.error_count = 0;
    for(form_event in form_event_array)
    {
        selector_array = form_event_array[form_event];
        for(selector in selector_array)
        {
            var selector_string = '#'+form.attr('id') +' '+selector;
            var selector_validation_array = selector_array[selector];
            for (cond in selector_validation_array)
            {

               condition = new Array();
               condition['op'] = cond;
               if(SSKREPO_JSLIB.obj.isset(selector_validation_array[cond]['arg']))
               condition['arg'] = selector_validation_array[cond]['arg'];
           
               if(SSKREPO_JSLIB.obj.isset(selector_validation_array[cond]['error']))
               errormessage = selector_validation_array[cond]['error'];
               else
               errormessage = null;

                $(selector_string).each(validate_and_adderror(condition,errormessage));
            }
        }
    }

    if(formerrors.error_count != '0')
    {
        event.preventDefault();           
    }

}
/**
 *Based on selectors and their validations & events, set events accordingly for all elements matching the selector
 *Set at which event errors need to be displyed using 'ERROR_HANDLER' plugin. If no ERROR_HANDLER plugin is provided
 *by user, use default ERROR_HANDLER plugin.
 *Also Build elements_that_I_depend_on array for each element having validations, which is used later for resolving
 *cyclic dependencies and for handling dependencies effectively
 *
 *@function set_selector_validation_events
 *@memberof SSKREPO_FORM_JS
 *@param {array} s_array Selector validation array
 *@see SSKREPO_FORM_JS.elements_that_I_depend_on
 *
 */

function set_selector_validation_events(s_array)
{
    var selector_validation_array = s_array;
    return function(index,element)
    {
        var selector_event,condition,i,errormessage;
        
        if(!SSKREPO_JSLIB.obj.isset(my.element_event_array[this.id]))
        {
            my.element_event_array[this.id] = new Array();
        }
    
        for(selector_event in selector_validation_array)
        {
            var selector_event_array = selector_validation_array[selector_event],cond;
            selector_event = selector_event.split('on');//remove on from event names as in jquery onblur is set as .on('blur')
            selector_event = selector_event['1'];
            my.element_event_array[this.id][selector_event] = '1';//stores events for every element;
            for(cond in selector_event_array)
            {
               condition = new Array();
               condition['op'] = cond;
               if(SSKREPO_JSLIB.obj.isset(selector_event_array[cond]['arg']))
               condition['arg'] = selector_event_array[cond]['arg'];
               else
               condition['arg']= null;//to make sure previous iteration arg is not used if user didnt set any arg
           
               if(SSKREPO_JSLIB.obj.isset(selector_event_array[cond]['error']))
               errormessage = selector_event_array[cond]['error'];
               else
               errormessage = null;//to make sure previous iteration errormessage is not used if user didnt set any error message
           
               if(SSKREPO_JSLIB.obj.isset(condition))
               {    
                    $(this).on(selector_event,validate_and_adderror(condition,errormessage));
                    
                            //if plugin is having plugin_display_error_event set to a specific event, then use that event, else set it to hover event
                    $(this).on(my.plugins.ERROR_HANDLER.obj.display_error_event,my.display_error_at_event);
                        //}
                        //else*/
                        //$(this).hover(my.display_error_at_event);
                    /*}
                    else//if no plugin is set, then default is hover
                    $(this).hover(my.display_error_at_event);*/
                    

               }
               my.find_and_build_elements_that_I_dependon(this.id,cond,selector_event_array[cond]);
            
            }
        }
    }
}

/**
 * Find and build elements_that_I_depend_on array for maintaining Dependencies
 * @function find_and_build_elements_that_I_dependon
 * @memberof SSKREPO_FORM_JS
 * @param {string} elementid id of an element for which to find dependencies
 * @param {string} cond_name Validation condition operation
 * @param {array} cond array of arguments for a condition with 'arg' and 'error' as keys
 * @returns none Returns nothing, but builds elements_that_I_depend_on array for elementid provided
 */
my.find_and_build_elements_that_I_dependon = function(elementid,cond_name,cond)
{
    if(cond_name == 'required')
    {    //build for each element a array of its req elements
       if(cond.hasOwnProperty('arg'))
        {
            var req_elem,req_elem_array = new Array();
            for(dep_elem in cond['arg'])
            {
                if(!my.elements_that_I_depend_on.hasOwnProperty(elementid))
                my.elements_that_I_depend_on[elementid] = new Array();
                
                if(!my.elements_that_I_depend_on[elementid].hasOwnProperty(dep_elem))
                my.elements_that_I_depend_on[elementid][dep_elem] = new Array();
             
                if(!my.elements_that_I_depend_on[elementid][dep_elem].hasOwnProperty('required'))
                my.elements_that_I_depend_on[elementid][dep_elem]['required'] = new Array();
            
                if(!my.elements_that_I_depend_on[elementid][dep_elem]['required'].hasOwnProperty('arg'))
                my.elements_that_I_depend_on[elementid][dep_elem]['required']['arg'] = new Array();
            
                my.elements_that_I_depend_on[elementid][dep_elem]['required']['arg'][dep_elem] = cond['arg'][dep_elem];
          
            }
        }

    }
    else if(cond.hasOwnProperty('dependson'))
    {
        var dep_elem_list = new Array(),dep_elem,dep_array = new Array(),rev_cond = new Array();
        rev_cond['revalidate_self'] = new Array();
        dep_elem_list = cond['dependson'];
        for(dep_elem in dep_elem_list)
        {   
            rev_cond['revalidate_self'][cond_name] = cond;
            if(!my.elements_that_I_depend_on.hasOwnProperty(elementid))
            my.elements_that_I_depend_on[elementid] = new Array();
            
            if(!my.elements_that_I_depend_on[elementid].hasOwnProperty(dep_elem))
            my.elements_that_I_depend_on[elementid][dep_elem] = new Array();

            if(!my.elements_that_I_depend_on[elementid][dep_elem].hasOwnProperty('revalidate_self'))
            my.elements_that_I_depend_on[elementid][dep_elem]['revalidate_self'] = new Array();
        
            my.elements_that_I_depend_on[elementid][dep_elem]['revalidate_self'][cond_name] = cond;
        }
    }
    
}

/**
 * 
 * Builds required_valid_element_array which is , For each element, array of elements that depend on it for "Required"->"valid" condition
 * @function required_valid_element_list
 * @memberof SSKREPO_FORM_JS
 * @param {array} element_dependecies_array Same as elements_that_I_depend_on[elementid]
 * @returns {Array|Boolean} Returns array of elements that depend on elementid for valid condition, if none returns false
 */

function required_valid_element_list(element_dependecies_array)
{
    
    var elementid,valid_elements_array = null,dep_elem,arg_of_required = new Array();
    
    for(dep_elem in element_dependecies_array)
    {
        if(element_dependecies_array[dep_elem].hasOwnProperty('required'))
        {
            arg_of_required[dep_elem] = element_dependecies_array[dep_elem]['required']['arg'][dep_elem];
        }
    }
    for(elementid in arg_of_required)
    {
        if(typeof arg_of_required[elementid] == 'string')//if not array
        {
            var req_arg = arg_of_required[elementid].split('|');//arg format "elementid":"valid|errormessage if not valid"
            var req_arg_cond = req_arg[0];
            if(SSKREPO_JSLIB.obj.isset(req_arg_cond))
            {
                if(req_arg_cond == 'valid')
                {
                    if(valid_elements_array == null)
                    var valid_elements_array = new Array();
                
                    valid_elements_array[elementid] = SSKREPO_JSLIB.obj.isset(req_arg[1])?req_arg[1].trim():null;//build array of elementids for which valid event is set
                }
            } 
        }
        else//if array
        {
            var req_arg = arg_of_required[elementid], cond;
            for(cond in req_arg)
            {
                if(cond == 'valid')
                {
                    if(valid_elements_array == null)
                    var valid_elements_array = new Array();
                    
                    valid_elements_array[elementid] = req_arg[cond].hasOwnProperty('error')?req_arg[cond]['error'].trim():null;
                    
                }
            }
            
        }
    }
    if(valid_elements_array != null)
    {
        return valid_elements_array;
    }
    else
    return false;
}

/**
 * 
 * @function elements_that_I_depend_on
 * @memberof SSKREPO_FORM_JS
 * @param {string} element_id
 * @returns {array} my.elements_that_I_depend_on[element_id]
 */
function elements_that_I_depend_on(element_id)
{
    if(my.elements_that_I_depend_on.hasOwnProperty(element_id))
    return my.elements_that_I_depend_on[element_id];
    else
    return null;
}
/**
 * Build array of dependents on a specific element
 * @function build_element_dependents_list
 * @memberof SSKREPO_FORM_JS
 * @returns none
 */

function build_element_dependents_list()
{
    var el,req_elid;
    
    //Build array of dependents on a specific element
    
    //first extract all elements list with atleast on dependent
    if(SSKREPO_JSLIB.obj.isset(my.elements_that_I_depend_on))
    {
        for(el in my.elements_that_I_depend_on)
        {
            for(req_elid in my.elements_that_I_depend_on[el])
            {
                    my.elements_that_depend_onme[req_elid] = 1; //elements with atleast one dependent
            }
        }

        //Now find list of dependents for each elementid in my.elements_that_depend_onme
        var dep_elem;
        for(el in my.elements_that_depend_onme)
        {
            var dep_array = new Array();
           for (dep_elem in my.elements_that_I_depend_on)
           {
               if(my.elements_that_I_depend_on[dep_elem].hasOwnProperty(el))
               {
                  dep_array[dep_elem] = my.elements_that_I_depend_on[dep_elem][el];
               }
           }
           my.elements_that_depend_onme[el] = dep_array;
        }
     
    }
}
/**
 * Returns List of elements that depend on a sepcific element
 * @function elements_that_depend_onme
 * @memberof SSKREPO_FORM_JS
 * @param {string} elementid
 * @returns {array} my.elements_that_depend_onme
 */
function elements_that_depend_onme(elementid)
{
    if(my.elements_that_depend_onme.hasOwnProperty(elementid))
    {
        return my.elements_that_depend_onme[elementid];
    }
    else
    return null;
}
/**
 * 
 * Main function that takes a cond and element and validates it for that condition and add/remove corresponding error message accodringly
 * @param {array} cond Validation Condition in with keys 'op' and 'arg', op - specific condition to be evaluated and 'arg' - give parameters needed for the operation
 * @param {string} error Error message if validation fails
 * @param {jQuery} el jQuery element object that need to be validated
 * @returns {Function} Function that can be used as call back function in ".on" event functions
 * @see set_selector_validation_events
 * @see form_onsubmit_validator
 */
function validate_and_adderror(cond,error,el)
{
    var condition = cond;
    var errormessage = error;
    var elem = el;


    return function()
    {
        //var elem = $(this);
        elem = SSKREPO_JSLIB.obj.isset(elem)?elem:$(this);
        if(!SSKREPO_JSLIB.obj.isset(formerrors))
        {
            formerrors = null;
        }
        var validation_result = validate_element(condition,elem,errormessage);
        var errmessage = validation_result['errormessage'];
        
        if(!validation_result['status'])
        {
            if(SSKREPO_JSLIB.obj.isset(errmessage) && errmessage != "")
            add_error(elem,errmessage);
            if(SSKREPO_JSLIB.obj.isset(formerrors))
            formerrors.error_count++;
        }
        else
        {
            if(SSKREPO_JSLIB.obj.isset(errmessage) && errmessage != "")
            remove_error(elem,errmessage);
        }
        var err;
        if(validation_result.hasOwnProperty('errormessages_to_add') && validation_result.errormessages_to_add.length > 0)
        {
            for(err in validation_result.errormessages_to_add)
            {
               add_error(elem,validation_result.errormessages_to_add[err]); 
            }
        }
        if(validation_result.hasOwnProperty('errormessages_to_remove') && validation_result.errormessages_to_remove.length > 0)
        {
            for(err in validation_result.errormessages_to_remove)
            {
               remove_error(elem,validation_result.errormessages_to_remove[err]); 
            }
        }
    }
}

/**
 * Add error to a element in element_error_array
 * @function add_error
 * @memberof SSKREPO_FORM_JS
 * @param {jQuery} elem jQuery element object
 * @param {string} errormessage Errormessage to be added to element
 * @returns none
 */

function add_error(elem,errormessage)
{
      //Save all errors in error array
      errormessage = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage.trim():null;
      if(my.element_error_array.hasOwnProperty(elem.attr('id')))
      {
          if($.inArray(errormessage,my.element_error_array[elem.attr('id')]) === -1)//new error
          my.element_error_array[elem.attr('id')][my.element_error_array[elem.attr('id')].length] = errormessage;
      }
      else
      {
          var err = new Array();
          err[0] = errormessage;
          my.element_error_array[elem.attr('id')] = err;
      }
      my.on_error_change(my.element_error_array,elem);//executes plugin_on_error_change plugin if plugin_name is set, else native plugin function
      
}
/**
 * Remove error from a element in element_error_array
 * @function remove_error
 * @memberof SSKREPO_FORM_JS
 * @param {jQuery} elem jQuery element object
 * @param {string} errormessage Errormessage to be removed from element
 * @returns none
 */
function remove_error(elem,errormessage)
{
    errormessage = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage.trim():null;
    if(my.element_error_array.hasOwnProperty(elem.attr('id')))
    {
          var err_position = $.inArray(errormessage,my.element_error_array[elem.attr('id')])
          if(err_position !== -1)//errormessage is already there
          {
              my.element_error_array[elem.attr('id')].splice(err_position,1);//remove errormessage from array;
          }     
    }
    my.on_error_change(my.element_error_array,elem);//executes plugin_on_error_change plugin if plugin_name is set, else native plugin function
}

/**
 * Wrapper function to use plugin ERROR_HANDLER for displaying errors for a event on a specific element. Used by set_selector_validation_events.
 * @param {event} event Event object result of event triggerred in Browser DOM
 * @returns none
 * @see set_selector_validation_events
 */
my.display_error_at_event = function(event)
{
      var element_error_array = my.element_error_array;
      
        var elem = $('#'+event.target.id);
        if(my.plugins !== null)//if plugins are loaded
        {
            if(my.plugins.hasOwnProperty('ERROR_HANDLER'))
            {
                my.plugins.ERROR_HANDLER.obj.display_error(element_error_array,elem);
            }
            
        }       
}
/**
 * Wrapper function to use plugin ERROR_HANDLER for displaying errors if errorstack of any element changes. Used by add_error and remove_error.
 * Also fired when loading all the events , to display if any errors for any elements , sent by server side PHP(result of server side validation)
 * @param {array} element_error_array Same as element_error_array
 * @param {jQuery} elem jQuery element for which error stack has changed
 * @returns none
 * @see set_selector_validation_events
 * @see element_error_array
 */
my.on_error_change = function(element_error_array,elem)
{   
    return function()
    {
         if(my.plugins !== null)//no plugin set
        {
            if(my.plugins.hasOwnProperty('ERROR_HANDLER'))
            {
                my.plugins.ERROR_HANDLER.obj.on_error_change(element_error_array,elem);
            }
        }
    }();   
}

/**
 * Default error handler plugin that implements display_error() and on_error_change() and specific display_error_event
 * @inner ERROR_HANDLER
 
 */

my.plugins_default.ERROR_HANDLER = (function()
{
    var me = {};
    me.display_error_event = 'hover';
    me.display_error = function(element_error_array,elem)
    {
        //default way to display error
        
        if(element_error_array.hasOwnProperty(elem.attr('id')))
        {
            var elem_error_array = element_error_array[elem.attr('id')];
            var nextelem = elem.next();
            //errormessage = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage.trim():null;
            var labeldata = nextelem.prop('tagName');
            var errorhtml;
            if(elem_error_array.length > 1 )//implode with || only if atleast two errors are there
            errorhtml = SSKREPO_JSLIB.array.implode(" || ",elem_error_array);
            else if(elem_error_array.length == 1)
            errorhtml = elem_error_array['0'];
            else //no errors so set the box border also to black
            {
                errorhtml='';
                elem.attr('style','border:solid 1px black');
            }   
            if(nextelem.prop('tagName') == 'LABEL' && nextelem.attr('id') == 'formerror--label')
            {
                nextelem.children().first().html(errorhtml);
            }
            else
            {
                elem.attr('style','border:solid 1px red');             
                var errorlabel = "<label id='formerror--label' for='"+elem.attr('name')+"'><font color='red' size='2'>"+errorhtml+"</font></label>";
                elem.after(errorlabel);
            }
        }
    };
    me.on_error_change = function(element_error_array,elem)
    {
        //just run display_error every time add_error or remove_error occurs
        me.display_error(element_error_array,elem);
    }
    
    return me;
    
}());

/**
 * Validates elements using VALIDATOR plugin. If a external plugin is provided, uses it if given condition is supported by external plugin, if not used
 * default VALIDATOR plugin to validate the condition
 * @function validate_element
 * @memberof SSKREPO_FORM_JS
 * @param {array} element_cond Condition to be evaludated, array with keys 'op' and 'arg'.
 * @param {jQuery} element jQuery element object which need to be validated
 * @param {string} errormessage Errormessage to be added if validation fails.
 * @returns {array} Returns a result array which is actually returns by VALIDATOR plugin's validate_element function
 */
function validate_element(element_cond,element,errormessage)
{
    if(my.plugins.VALIDATOR.obj.is_condition_supported(element_cond['op']))
    {
        return my.plugins.VALIDATOR.obj.validate_element(element_cond,element,errormessage);
    }
    else
    {
       return my.plugins_default.VALIDATOR.validate_element(element_cond,element,errormessage); 
    }   
}

/**
 * 
 */

my.plugins_default.VALIDATOR = (function()
{
    var me = {};  
    
    me.validate_element = function(element_cond,element,errormessage)
    {
        //return 1;
        //if element_cond['op'] is compare_element, then we need to get the value of other element and the call jsvalidate.validate('compare',element_value,$secondelement_value);
        //var me.jsvalidator = new jsvalidator;
        var arg = SSKREPO_JSLIB.obj.isset(element_cond['arg'])?element_cond['arg']:null;
        errormessage = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:null;
        var element_value = element.val();
        
        var result = new Array();
        result.errormessages_to_add = new Array();
        result.errormessages_to_remove = new Array();
        

        /*removing below code from here and moving this inside "required" , because errormessage will be empty (as it is "required" errormessage" not each arg errormessage. each arg errormessage is extracted inside "required" condition in this function
        else if(my.required_element_array.hasOwnProperty(element.attr('id')) && element_cond['op'] == 'required')
        {
            //to prevent infinite loops for required condition between elements
            var result = new Array()
            //result['status'] = my.required_element_array[element.attr('id')];
            result['status'] = true;//always true to prevent infinite loops
            result['errormessage'] = errormessage;
            remove_error(element,errormessage);
            return result;        
        }*/

            if(SSKREPO_JSLIB.obj.isset(element_cond['op']) && element_cond['op'] == 'compare_element')
            {
                if(SSKREPO_JSLIB.obj.isset(arg['elementid']))
                {
                    var el = document.getElementById(arg['elementid']);
                    if(el !== null)
                    {
                        if(el.tagName != 'TEXTAREA' )
                        {
                            var operand2 = el.value;
                        }
                        else
                        {
                            var operand2 = el.innertext;
                        }
                    }
                }
                //elementname for comparision is not supported, using elementid only is supported for compare_element
                /*else if(SSKREPO_JSLIB.obj.isset(arg['elementname']))
                {
                    var el = document.getElementsByName(arg['elementname']);
                    var k;
                    if(el !== null)
                    {
                        for (k = 0; k < el.length ; k++)
                        {
                            if(el[k].tagName != 'TEXTAREA' )
                            {
                                var operand2 = el[k].value;
                            }
                            else
                            {
                                var operand2 = el[k].innertext;
                            }

                        }
                    }
                }*/
                var arg1 = Array();
                arg1['operator'] = arg['operator'];
                arg1['comparewith'] = operand2;
                if(errormessage === null)
                {
                    //default error message, of all other condition default messages are set in validator itslef, for this we need to set here
                    errormessage = "Should be "+arg['operator']+" elementid :"+arg['elementid'];
                }
                return me.jsvalidator.validate('compare',element_value,arg1,errormessage);
            }
            else if((SSKREPO_JSLIB.obj.isset(element_cond['op']) && element_cond['op'] == 'creditcard'))
            {
                var cardnumber = null;
                var arg1 = new Array();

                if(SSKREPO_JSLIB.obj.isset(arg))
                {

                    if(arg.hasOwnProperty("card_type_elementid"))
                    {
                        var card_type_elem = $('#'+arg["card_type_elementid"]);
                        if(SSKREPO_JSLIB.obj.isset(card_type_elem))                    
                        {
                            //card type list will be a 'select' element and we need to get the value of <option> element inside it which has selected property as 'selected'
                            arg1['cardtype_to_match'] = card_type_elem.val();
                        }
                    }
                    var cardnumber = element_value;
                }
                return me.jsvalidator.validate('creditcard',cardnumber,arg1,errormessage);
            }
            else if((SSKREPO_JSLIB.obj.isset(element_cond['op']) && element_cond['op'] == 'intl_phone'))//validate international phone numbers
            {
                var arg1 = new Array();
                if(SSKREPO_JSLIB.obj.isset(arg))
                {
                    if(arg.hasOwnProperty("ccelementid"))
                    {
                        var cc_elem = $('#'+arg["ccelementid"]);
                        if(SSKREPO_JSLIB.obj.isset(cc_elem))                    
                        {                                
                            arg1['input_cc'] = cc_elem.val();
                        }
                    }
                }
                var phonenumber = element_value;                   

                if(SSKREPO_JSLIB.obj.isset(arg['allowed_cc_list']))
                arg1['allowed_cc_list'] = arg['allowed_cc_list'];
                else
                arg1['allowed_cc_list'] = null;

                return me.jsvalidator.validate('intl_phone',phonenumber,arg1,errormessage);

            }
            else if((SSKREPO_JSLIB.obj.isset(element_cond['op']) && element_cond['op'] == 'required'))
            {
                //handled required condition, used for dependency between elements, example, validate Y if X is valid or Xis checked or X is selected etc
                //
                /*input :
                 *      isvalid
                 *checkbox:
                 *      checked
                 *select:
                 *      selected
                 *textarea:
                 *      isvalid
                 *      
                 * isvalid implementation
                 *      find all events that are set for the element
                 *      trigger the events manually
                 *      check if the element is valid
                 *      
                 */
                var req_error_stack = null;
                var req_element_id,elem_err_count=0,err_string;
                if(SSKREPO_JSLIB.obj.isset(arg))
                {
                    for(req_element_id in arg)
                    {   
                        var cur_elem_err = 0;
                        if(req_element_id != element.attr('id') )//self required events can cause self deadlock, dont process required for self
                        {
                            if(typeof arg[req_element_id] == 'string')//if not array
                            {
                                var req_element = $('#'+ req_element_id);

                                var req_arg = arg[req_element_id].split('|');//arg format "elementid":"valid|errormessage if not valid"
                                var req_arg_cond = req_arg[0];
                                var req_errormessage = SSKREPO_JSLIB.obj.isset(req_arg[1])?req_arg[1]:null;


                                if(SSKREPO_JSLIB.obj.isset(req_element)) //if element exists
                                {
                                    if(req_arg_cond == 'valid' && req_element.prop('tagName') == 'INPUT' )
                                    {                                       
                                        if(!SSKREPO_FORM_JS.is_element_valid(req_element))
                                        {
                                            cur_elem_err++;  
                                        }
                                    }
                                    else if(req_arg_cond == 'checked') 
                                    {
                                        //checkbox element are usually placed inside a fieldset and all checbox elements inside are given same name linke ch[] and same id ch[]
                                        //so id provided can be a fieldset id or directly a specific checkbox id
                                        var prop_type = req_element.attr('type');
                                        if(req_element.attr('type') == 'checkbox' && req_element.prop('tagName') == 'INPUT')//if id is checkbox id
                                        {
                                            if($('#'+req_element_id+':checked').length > 0 )
                                            {
                                            //return true;
                                            }   
                                            else
                                            {
                                                cur_elem_err++;
                                            }
                                        }
                                        else if(req_element.prop('tagName') == 'FIELDSET')
                                        {
                                            if($('#'+req_element_id+' :checkbox:checked').length <= 0)//not even one checkbox inside the fieldset is checked
                                            {
                                               cur_elem_err++;
                                            }                                                              
                                        }
                                    }
                                    else if(req_arg_cond == 'selected' && req_element.prop('tagName') == 'SELECT')
                                    {
                                        if(!req_element.val())
                                        {
                                            cur_elem_err++;
                                        }
                                    }
                                    //else
                                    //{
                                        //elem_err_count++;
                                        //cur_elem_err++;
                                    //}

                                }
                                if(cur_elem_err!=0)
                                {
                                   //add_error(element,req_errormessage);
                                   result['errormessages_to_add'].push(req_errormessage); 
                                }
                                else
                                {  //remove_error(element,req_errormessage);
                                   result['errormessages_to_remove'].push(req_errormessage);
                                }
                            }
                            else
                            {
                                //each required element condition is again specified with multiple validations.
                                //required:
                                /*{
                                 *      "elementid_1":
                                 *      {
                                 *          "isint":
                                 *          {
                                 *              "error":"elementid_1 is not valid integer"                        *              
                                 *          }
                                 *      }

                                }*/
                                var req_elem_cond;
                                var req_element = $('#'+ req_element_id)
                                for(req_elem_cond in arg[req_element_id])
                                {
                                   var req_array_each_error_count = 0;
                                   var condition = new Array();
                                   condition['op'] = req_elem_cond;
                                   if(arg[req_element_id][req_elem_cond].hasOwnProperty('arg'))
                                   condition['arg'] = arg[req_element_id][req_elem_cond]['arg'];
                                   else
                                   condition['arg'] = null;

                                   if(arg[req_element_id][req_elem_cond].hasOwnProperty('error'))
                                   req_errormessage = arg[req_element_id][req_elem_cond]['error'];
                                   else
                                   req_errormessage = null;

                                   if(condition['op'] != 'required')//nested required not supported, just ignore it
                                   {
                                        if(condition['op'] == 'valid' && req_element.prop('tagName') == 'INPUT' )
                                        {
                                            if(!SSKREPO_FORM_JS.is_element_valid(req_element))
                                            {
                                                req_array_each_error_count++; 
                                            }
                                        }
                                        else if(condition['op'] == 'checked') 
                                        {
                                            //checkbox element are usually placed inside a fieldset and all checbox elements inside are given same name linke ch[] and same id ch[]
                                            //so id provided can be a fieldset id or directly a specific checkbox id
                                            var prop_type = req_element.attr('type');
                                            if(req_element.attr('type') == 'checkbox' && req_element.prop('tagName') == 'INPUT')//if id is checkbox id
                                            {
                                                if($('#'+req_element_id+':checked').length > 0 )
                                                {
                                                //return true;
                                                }   
                                                else
                                                {
                                                    req_array_each_error_count++;
                                                }
                                            }
                                            else if(req_element.prop('tagName') == 'FIELDSET')
                                            {
                                                if($('#'+req_element_id+' :checkbox:checked').length <= 0)//not even one checkbox inside the fieldset is checked
                                                {
                                                   req_array_each_error_count++;
                                                }                                                              
                                            }

                                        }
                                        else if(condition['op'] == 'selected' && req_element.prop('tagName') == 'SELECT')
                                        {
                                            if(!req_element.is(':selected'))
                                            {
                                                req_array_each_error_count++;
                                            }

                                        }
                                        else if(condition['op'] == 'compare_element')
                                        {   
                                            if(req_element.attr('tagName') != 'TEXTAREA' )
                                            {
                                                var operand2 = req_element.val();
                                            }
                                            else
                                            {
                                                var operand2 = req_element.attr('innertext');
                                            }
                                            var arg1 = Array();
                                            arg1['operator'] = condition['arg']['operator'];
                                            arg1['comparewith'] = operand2;
                                            if(req_errormessage === null)
                                            {
                                                //default error message, of all other condition default messages are set in validator itslef, for this we need to set here
                                                req_errormessage = "Should be "+arg1['operator']+" elementid :"+req_element.attr('id');
                                            }
                                            var val_result = me.jsvalidator.validate('compare',element_value,arg1,req_errormessage);
                                            if(!val_result['status'])
                                            {
                                                req_array_each_error_count++;
                                            }
                                            req_errormessage = val_result['errormessage'];
                                        }
                                        else
                                        {
                                            var val_result = validate_element(condition,req_element,req_errormessage);
                                            if(!val_result['status'])
                                            {
                                                req_array_each_error_count++;
                                            }
                                            req_errormessage = val_result['errormessage'];
                                        }
                                        if(req_array_each_error_count != 0)
                                        {
                                           //add_error(element,req_errormessage);
                                           result['errormessages_to_add'].push(req_errormessage);
                                           cur_elem_err++;
                                        }
                                        else
                                        {
                                           //remove_error(element,req_errormessage);
                                           result['errormessages_to_remove'].push(req_errormessage);

                                        }
                                    }
                                }
                            }
                            if(cur_elem_err != 0)
                            {
                                elem_err_count++;
                            }
                        }
                    }
                    if(elem_err_count != 0)
                    {
                        result['status'] = false;
                        result['errormessage'] = '';//as errormessages are already added;
                    }
                    else
                    {
                        result['status'] = true;
                        result['errormessage'] = '';//as errormessages are already added;
                    }
                    return result;
                }
            }
            else
            {
                return me.jsvalidator.validate(element_cond['op'],element_value,arg,errormessage);
            }
    }
    var jsvalidator = function(){};
    
    jsvalidator.prototype.validate = function(cond,value,arg,errormessage,just_check_if_supported)
    {
            //var cond1 = cond.split("#");         //for date,zipcode etc we get extra string with # for example isdate#DDMMYYY#-
            if(typeof just_check_if_supported === 'undefined')
            {   //used by VALIDATOR.is_condition_supported to check if validator supported a specific condition
                just_check_if_supported = false;
            }
            var default_err_message = null,result = new Array();
            result['status'] = false;
            result['errormessage'] = '';
             switch (cond)
             {
                 case 'isint':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isint(value);
                     default_err_message = "Please enter valid integer";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'isfloat':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isfloat(value);
                     default_err_message = "Please enter valid Number";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'isemail':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isemail(value);
                     default_err_message = "Please enter valid EmailId";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'isip':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isip(value);
                     default_err_message = "Please enter valid IP Address";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'ismac':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.ismac(value);
                     default_err_message = "Please enter valid MAC Address";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'isurl':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isurl(value);
                     default_err_message = "Please enter valid URL";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 //if data then it comes in format of isdate#<Data format DDMMYYYY or DDMONYYYY>#<Separator - or / or .>
                 case 'isdate': //allowed formats #DDMMYYYY#- or #DDMMYYYY#/ or#DDMMYYYY#. or #DDMONYYYY#- or #DDMON-YYYY#/ or #DDMONYYYY#. where - , / , . are separators                     
                        if(!just_check_if_supported)
                        { 
                        if(SSKREPO_JSLIB.obj.isset(arg['format']) && SSKREPO_JSLIB.obj.isset(arg['separator']))
                        result['status'] = this.isdate(value,arg['format'],arg['separator']);
                        else
                        result['status'] = 0;    
                        default_err_message = "Please enter valid date in "+arg['format']+' format spearated by "'+arg['separator']+'"';
                        result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                        break;
                        }
                     else
                     return true;
                 case 'intl_phone': //provide cc and phone number, optionally allowed cc list, this will validate the number for the country code and return true or false
                     if(!just_check_if_supported)
                     {
                         if(SSKREPO_JSLIB.obj.isset(arg['input_cc']) && SSKREPO_JSLIB.obj.isset(value))
                         {   
                            allowed_cc_list = SSKREPO_JSLIB.obj.isset(arg['allowed_cc_list'])?arg['allowed_cc_list']:null;
                            if(SSKREPO_JSLIB.obj.isset(allowed_cc_list))
                            {
                                if(!SSKREPO_JSLIB.array.in_array(arg['input_cc'],allowed_cc_list))//if provided cc is not part of allowed cc list
                                result['status'] = 0;
                                else
                                result['status'] = this.intl_phone(value,arg['input_cc']);
                            }           
                            else
                            result['status'] = this.intl_phone(value,arg['input_cc']);

                         }
                         else
                         result['status'] = 0;  
                         default_err_message = 'Please enter valid phone number';
                         result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                         break;
                     }
                     else
                     return true;

                 case 'isalphanum':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isalphanum(value);
                     default_err_message = "Please enter valid Alphanumeric";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;

                 case 'iszipcode'://#CC where CC is two CHARACTER country code for which zip needs to be validated
                     if(!just_check_if_supported)
                     {
                     if(SSKREPO_JSLIB.obj.isset(arg['countrycode']))
                     result['status'] = this.iszipcode(value,arg['countrycode']);
                     else
                     result['status'] = 0;
                     default_err_message = "Please enter valid zipcode/postalcode for country "+arg['countrycode'];
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;

                 case 'isnotempty':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isnotempty(value);
                     default_err_message = "This is mandatory field";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;

                 case 'isempty':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.isempty(value);
                     default_err_message = "This must be empty";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'compare_element'://it is here just to return true if is_condition_supported is called with 'compare_element'
                 case 'compare':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.compare(value,arg['comparewith'],arg['operator'],arg['compareas']);
                     default_err_message = "This value must be "+arg['operator']+' '+arg['comparewith'];
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;

                 case 'regex':
                     if(!just_check_if_supported)
                     {
                     result['status'] = this.regexp(arg['expr'],value);
                     default_err_message = "";//for regex default error message doesnt make sense
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'abslength': //using just length creates problems as length is also used in javascript as standard object property and that is getting overwritten
                     if(!just_check_if_supported)
                     {
                     var minlength = SSKREPO_JSLIB.obj.isset(arg['length'])?arg['length']:null;
                     var maxlength = SSKREPO_JSLIB.obj.isset(arg['length'])?arg['length']:null;
                     result['status'] = this.length_validate(value,minlength,maxlength);
                     default_err_message = "Should be "+arg['length']+" characters";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'minlength':
                     if(!just_check_if_supported)
                     {
                     var minlength = SSKREPO_JSLIB.obj.isset(arg['length'])?arg['length']:null;
                     var maxlength = null;
                     result['status'] = this.length_validate(value,minlength,maxlength);
                     default_err_message = "Should be atleast "+arg['length']+" characters";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'maxlength':
                     if(!just_check_if_supported)
                     {
                     var maxlength = SSKREPO_JSLIB.obj.isset(arg['length'])?arg['length']:null;
                     var minlength = null;
                     result['status'] = this.length_validate(value,minlength,maxlength);
                     default_err_message = "Should be maximum of "+arg['length']+" characters";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'password':
                     if(!just_check_if_supported)
                     {
                     if(SSKREPO_JSLIB.obj.isset(arg))
                     {
                        var res = this.password_validate(value,arg);//result is a array of different keys(like uppercase_count etc) with their validation status
                     }
                     if(SSKREPO_JSLIB.obj.isset(res))
                     {
                         for(cond in res)
                         {
                             if(!res[cond])
                             {  
                                 var err = true;

                             }
                             //we show all the conditions that are mentioned in json for password
                             //not just the ones that are failed
                             //becuase user need to know what all condition will make a valid password all at once
                             //if we show only ones where validation failed, user will end up getting different errors multiple times
                             //which will be annoying
                             if(default_err_message === null)
                             default_err_message = "Password Must have :";

                             switch (cond)
                             {
                                 case 'uppercase_count':
                                     default_err_message = default_err_message+"\n\tAtleast "+arg[cond]+" UpperCase Letters";
                                     break;
                                 case 'lowercase_count':
                                     default_err_message = default_err_message+"\n\tAtleast "+arg[cond]+" LowerCase Letters";
                                     break;
                                 case 'digit_count':
                                     default_err_message = default_err_message+"\n\tAtleast "+arg[cond]+" Numbers";
                                     break;
                                 case 'spec_chars_count':
                                     default_err_message = default_err_message+"\n\tAtleast "+arg[cond]+" Special Characters from !#\$\&%&\'\"\(\)*+-.\/:;<=>?@\^_`{|}~";
                                     break;
                             }
                         }
                     }
                     if(!err)
                     result['status'] = true;
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'creditcard':
                     if(!just_check_if_supported)
                     {

                     var cardnumber = value;
                     var cardtype_to_match = SSKREPO_JSLIB.obj.isset(arg['cardtype_to_match'])?arg['cardtype_to_match']:null;
                     result = this.creditcard.validate_card(cardnumber,cardtype_to_match);
                      //creditcard.validate_card return a array with 'status','error','cardnumber','cardtype' keys
                     default_err_message = "Invalid credit card number or Not matching with Credit card type selected";
                     result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                     break;
                     }
                     else
                     return true;
                 case 'intl_phone':
                    
                 default:
                    if(!just_check_if_supported)
                    {
                        if(SSKREPO_JSLIB.obj.is_object_having_methods(this,cond))
                        {   //if user implmenented a validator condition on their own and not implemented in this default validator then call the same
                             result = this.object[cond](value,arg,errormessage);
                        }
                        else//if validator condition doesnt exists, then always return true
                        {
                            result = 'true';
                             //creditcard.validate_card return a array with 'status','error','cardnumber','cardtype' keys
                            default_err_message = "";
                            result['errormessage'] = SSKREPO_JSLIB.obj.isset(errormessage)?errormessage:default_err_message;
                            break;
                        }
                    }
                    else
                    return false;
             }
             return result;
        }

    jsvalidator.prototype.compare = function compare(operand1,operand2,operator,compareas)
     {
        if(compareas == 'numbers')
        {
            //operand1 = parseFloat(operand1.replace(",", ""));//remove commas like comma to separate thousand field etc
            //operand2 = parseFloat(operand2.replace(",", ""));
            operand1 = Number(operand1.replace(",", ""));
            operand2 = Number(operand2.replace(",", ""));
        }

        if(SSKREPO_JSLIB.obj.isset(operand1) && SSKREPO_JSLIB.obj.isset(operand2) && SSKREPO_JSLIB.obj.isset(operator))
        {
            switch (operator)
            {
                 case '==':
                     return (operand1 == operand2)?1:0;
                 case '!=':
                     return (operand1 != operand2)?1:0;
                 case '>':
                     return (operand1 > operand2)?1:0;
                 case '<':
                     return (operand1 < operand2)?1:0;
                 case '>=':
                     return (operand1 >= operand2)?1:0;
                 case '<=':
                     return (operand1 <= operand2)?1:0;
                 case '===':
                     return (operand1 === operand2)?1:0;
                 case '!==':
                     return (operand1 === operand2)?1:0;

            }
        }
        else
        return 0;
     }
    jsvalidator.prototype.regexp = function(regex,value)
    {
            //regex = "/"+regex+"/";
            var thisregex = new RegExp(regex);
        return thisregex.test(value);

    }

    jsvalidator.prototype.isdate = function(value,dateformat,separator)
    {       
            var regex;
            switch (dateformat)
            {
                case 'DDMMYYYY':
                    regex = "^(?:(?:31(\\"+separator+")(?:0?[13578]|1[02])))|(?:(?:29|30)(\\"+separator+")(?:0?[1,3-9]|1[0-2]))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\"+separator+")(?:0?2|(?:Feb))(\\"+separator+")(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\"+separator+")(?:(?:0?[1-9])|(?:1[0-2]))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})\$";
                    break;
                case 'DDMONYYYY':
                    regex = "^(?:(?:31(\\"+separator+")(?:Jan|Mar|May|Jul|Aug|Oct|Dec))|(?:(?:29|30)(\\"+separator+")(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\"+separator+")(?:Feb)(\\"+separator+")(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\"+separator+")((?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep)|(?:Oct|Nov|Dec))(\\"+separator+")(?:(?:1[6-9]|[2-9]\\d)?\\d{2})\$";
                    break;        
            }
            return this.regexp(regex,value);

    }

    jsvalidator.prototype.intl_phone = function(phonenumber,cc)
    {
        //Thanks to James Nonnemaker for publishing well prepared list of country code to length mapping on moo.net
        var intl_phone_criteria = 
                    {'93' : { 'min_length' : '9', 'max_length' : '9'}, //Afghanistan
                    '355' : {  'min_length' : '3', 'max_length' : '9'},//Albania
                    '213' : {  'min_length' : '8', 'max_length' : '9'},//Algeria
                    '376' : {  'min_length' : '6', 'max_length' : '9'},//Andorra
                    '244' : {  'min_length' : '9', 'max_length' : '9'},//Angola
                    '54' : {  'min_length' : '10', 'max_length' : '10'},//Argentina
                    '374' : {  'min_length' : '8', 'max_length' : '8'},//Armenia
                    '297' : {  'min_length' : '7', 'max_length' : '7'},//Aruba
                    '61' : {  'min_length' : '5', 'max_length' : '15'},//Australia
                    '672' : {  'min_length' : '6', 'max_length' : '6'},//Australian External Territories
                    '43' : {  'min_length' : '4', 'max_length' : '13'},//Austria
                    '994' : {  'min_length' : '8', 'max_length' : '9'},//Azerbaijan
                    '973' : {  'min_length' : '8', 'max_length' : '8'},//Bahrain
                    '880' : {  'min_length' : '6', 'max_length' : '10'},//Bangladesh
                    '375' : {  'min_length' : '9', 'max_length' : '10'},//Belarus
                    '32' : {  'min_length' : '8', 'max_length' : '9'},//Belgium
                    '501' : {  'min_length' : '7', 'max_length' : '7'},//Belize
                    '229' : {  'min_length' : '8', 'max_length' : '8'},//Benin
                    '975' : {  'min_length' : '7', 'max_length' : '8'},//Bhutan
                    '591' : {  'min_length' : '8', 'max_length' : '8'},//Bolivia (Plurinational State of},
                    '599' : {  'min_length' : '7', 'max_length' : '7'},//Bonaire Sint Eustatius and Saba
                    '387' : {  'min_length' : '8', 'max_length' : '8'},//Bosnia and Herzegovina
                    '267' : {  'min_length' : '7', 'max_length' : '8'},//Botswana
                    '55' : {  'min_length' : '10', 'max_length' : '10'},//Brazil
                    '673' : {  'min_length' : '7', 'max_length' : '7'},//Brunei Darussalam
                    '359' : {  'min_length' : '7', 'max_length' : '9'},//Bulgaria
                    '226' : {  'min_length' : '8', 'max_length' : '8'},//Burkina Faso
                    '257' : {  'min_length' : '8', 'max_length' : '8'},//Burundi
                    '855' : {  'min_length' : '8', 'max_length' : '8'},//Cambodia
                    '237' : {  'min_length' : '8', 'max_length' : '8'},//Cameroon
                    '238' : {  'min_length' : '7', 'max_length' : '7'},//Cape Verde
                    '236' : {  'min_length' : '8', 'max_length' : '8'},//Central African Rep.
                    '235' : {  'min_length' : '8', 'max_length' : '8'},//Chad
                    '56' : {  'min_length' : '8', 'max_length' : '9'},//Chile
                    '86' : {  'min_length' : '5', 'max_length' : '12'},//China
                    '57' : {  'min_length' : '8', 'max_length' : '10'},//Colombia
                    '269' : {  'min_length' : '7', 'max_length' : '7'},//Comoros
                    '242' : {  'min_length' : '9', 'max_length' : '9'},//Congo
                    '682' : {  'min_length' : '5', 'max_length' : '5'},//Cook Islands
                    '506' : {  'min_length' : '8', 'max_length' : '8'},//Costa Rica
                    '225' : {  'min_length' : '8', 'max_length' : '8'},//C?te d'Ivoire
                    '385' : {  'min_length' : '8', 'max_length' : '12'},//Croatia
                    '53' : {  'min_length' : '6', 'max_length' : '8'},//Cuba
                    '599' : {  'min_length' : '7', 'max_length' : '8'},//Cura?ao
                    '357' : {  'min_length' : '8', 'max_length' : '11'},//Cyprus
                    '420' : {  'min_length' : '4', 'max_length' : '12'},//Czech Rep.
                    '850' : {  'min_length' : '6', 'max_length' : '17'},//Dem. People's Rep. of Korea
                    '243' : {  'min_length' : '5', 'max_length' : '9'},//Dem. Rep. of the Congo
                    '45' : {  'min_length' : '8', 'max_length' : '8'},//Denmark
                    '246' : {  'min_length' : '7', 'max_length' : '7'},//Diego Garcia
                    '253' : {  'min_length' : '6', 'max_length' : '6'},//Djibouti
                    '593' : {  'min_length' : '8', 'max_length' : '8'},//Ecuador
                    '20' : {  'min_length' : '7', 'max_length' : '9'},//Egypt
                    '503' : {  'min_length' : '7', 'max_length' : '11'},//El Salvador
                    '240' : {  'min_length' : '9', 'max_length' : '9'},//Equatorial Guinea
                    '291' : {  'min_length' : '7', 'max_length' : '7'},//Eritrea
                    '372' : {  'min_length' : '7', 'max_length' : '10'},//Estonia
                    '251' : {  'min_length' : '9', 'max_length' : '9'},//Ethiopia
                    '500' : {  'min_length' : '5', 'max_length' : '5'},//Falkland Islands (Malvinas},
                    '298' : {  'min_length' : '6', 'max_length' : '6'},//Faroe Islands
                    '679' : {  'min_length' : '7', 'max_length' : '7'},//Fiji
                    '358' : {  'min_length' : '5', 'max_length' : '12'},//Finland
                    '33' : {  'min_length' : '9', 'max_length' : '9'},//France
                    '262' : {  'min_length' : '9', 'max_length' : '9'},//French Departments and Territories in the Indian Ocean
                    '594' : {  'min_length' : '9', 'max_length' : '9'},//French Guiana
                    '689' : {  'min_length' : '6', 'max_length' : '6'},//French Polynesia
                    '241' : {  'min_length' : '6', 'max_length' : '7'},//Gabon
                    '220' : {  'min_length' : '7', 'max_length' : '7'},//Gambia
                    '995' : {  'min_length' : '9', 'max_length' : '9'},//Georgia
                    '49' : {  'min_length' : '6', 'max_length' : '13'},//Germany
                    '233' : {  'min_length' : '5', 'max_length' : '9'},//Ghana
                    '350' : {  'min_length' : '8', 'max_length' : '8'},//Gibraltar
                    '30' : {  'min_length' : '10', 'max_length' : '10'},//Greece
                    '299' : {  'min_length' : '6', 'max_length' : '6'},//Greenland
                    '590' : {  'min_length' : '9', 'max_length' : '9'},//Guadeloupe
                    '502' : {  'min_length' : '8', 'max_length' : '8'},//Guatemala
                    '224' : {  'min_length' : '8', 'max_length' : '8'},//Guinea
                    '245' : {  'min_length' : '7', 'max_length' : '7'},//Guinea-Bissau
                    '592' : {  'min_length' : '7', 'max_length' : '7'},//Guyana
                    '509' : {  'min_length' : '8', 'max_length' : '8'},//Haiti
                    '504' : {  'min_length' : '8', 'max_length' : '8'},//Honduras
                    '852' : {  'min_length' : '4', 'max_length' : '9'},//Hong Kong China
                    '36' : {  'min_length' : '8', 'max_length' : '9'},//Hungary
                    '354' : {  'min_length' : '7', 'max_length' : '9'},//Iceland
                    '91' : {  'min_length' : '7', 'max_length' : '10'},//India
                    '62' : {  'min_length' : '5', 'max_length' : '10'},//Indonesia
                    '870' : {  'min_length' : '9', 'max_length' : '9'},//Inmarsat SNAC
                    '800' : {  'min_length' : '8', 'max_length' : '8'},//International Freephone Service
                    '882' : {  'min_length' : '0', 'max_length' : '0'},//International Networks shared code
                    '883' : {  'min_length' : '0', 'max_length' : '0'},//International Networks shared code
                    '979' : {  'min_length' : '9', 'max_length' : '9'},//International Premium Rate Service (IPRS},
                    '808' : {  'min_length' : '8', 'max_length' : '8'},//International Shared Cost Service (ISCS},
                    '98' : {  'min_length' : '6', 'max_length' : '10'},//Iran (Islamic Republic of},
                    '964' : {  'min_length' : '8', 'max_length' : '10'},//Iraq
                    '353' : {  'min_length' : '7', 'max_length' : '11'},//Ireland
                    '972' : {  'min_length' : '8', 'max_length' : '9'},//Israel
                    '39' : {  'min_length' : '1', 'max_length' : '11'},//Italy
                    '81' : {  'min_length' : '5', 'max_length' : '13'},//Japan
                    '962' : {  'min_length' : '5', 'max_length' : '9'},//Jordan
                    '7' : {  'min_length' : '10', 'max_length' : '10'},//Kazakhstan
                    '254' : {  'min_length' : '6', 'max_length' : '10'},//Kenya
                    '686' : {  'min_length' : '5', 'max_length' : '5'},//Kiribati
                    '82' : {  'min_length' : '8', 'max_length' : '11'},//Korea (Rep. of},
                    '965' : {  'min_length' : '7', 'max_length' : '8'},//Kuwait
                    '996' : {  'min_length' : '9', 'max_length' : '9'},//Kyrgyzstan
                    '856' : {  'min_length' : '8', 'max_length' : '10'},//Lao P.D.R.
                    '371' : {  'min_length' : '7', 'max_length' : '8'},//Latvia
                    '961' : {  'min_length' : '7', 'max_length' : '8'},//Lebanon
                    '266' : {  'min_length' : '8', 'max_length' : '8'},//Lesotho
                    '231' : {  'min_length' : '7', 'max_length' : '8'},//Liberia
                    '218' : {  'min_length' : '8', 'max_length' : '9'},//Libya
                    '423' : {  'min_length' : '7', 'max_length' : '9'},//Liechtenstein
                    '370' : {  'min_length' : '8', 'max_length' : '8'},//Lithuania
                    '352' : {  'min_length' : '4', 'max_length' : '11'},//Luxembourg
                    '853' : {  'min_length' : '7', 'max_length' : '8'},//Macao China
                    '261' : {  'min_length' : '9', 'max_length' : '10'},//Madagascar
                    '265' : {  'min_length' : '7', 'max_length' : '8'},//Malawi
                    '60' : {  'min_length' : '7', 'max_length' : '9'},//Malaysia
                    '960' : {  'min_length' : '7', 'max_length' : '7'},//Maldives
                    '223' : {  'min_length' : '8', 'max_length' : '8'},//Mali
                    '356' : {  'min_length' : '8', 'max_length' : '8'},//Malta
                    '692' : {  'min_length' : '7', 'max_length' : '7'},//Marshall Islands
                    '596' : {  'min_length' : '9', 'max_length' : '9'},//Martinique
                    '222' : {  'min_length' : '7', 'max_length' : '7'},//Mauritania
                    '230' : {  'min_length' : '7', 'max_length' : '7'},//Mauritius
                    '52' : {  'min_length' : '10', 'max_length' : '10'},//Mexico
                    '691' : {  'min_length' : '7', 'max_length' : '7'},//Micronesia
                    '373' : {  'min_length' : '8', 'max_length' : '8'},//Moldova (Republic of},
                    '377' : {  'min_length' : '5', 'max_length' : '9'},//Monaco
                    '976' : {  'min_length' : '7', 'max_length' : '8'},//Mongolia
                    '382' : {  'min_length' : '4', 'max_length' : '12'},//Montenegro
                    '212' : {  'min_length' : '9', 'max_length' : '9'},//Morocco
                    '258' : {  'min_length' : '8', 'max_length' : '9'},//Mozambique
                    '95' : {  'min_length' : '7', 'max_length' : '9'},//Myanmar
                    '264' : {  'min_length' : '6', 'max_length' : '10'},//Namibia
                    '674' : {  'min_length' : '4', 'max_length' : '7'},//Nauru
                    '977' : {  'min_length' : '8', 'max_length' : '9'},//Nepal
                    '31' : {  'min_length' : '9', 'max_length' : '9'},//Netherlands
                    '687' : {  'min_length' : '6', 'max_length' : '6'},//New Caledonia
                    '64' : {  'min_length' : '3', 'max_length' : '10'},//New Zealand
                    '505' : {  'min_length' : '8', 'max_length' : '8'},//Nicaragua
                    '227' : {  'min_length' : '8', 'max_length' : '8'},//Niger
                    '234' : {  'min_length' : '7', 'max_length' : '10'},//Nigeria
                    '683' : {  'min_length' : '4', 'max_length' : '4'},//Niue
                    '47' : {  'min_length' : '5', 'max_length' : '8'},//Norway
                    '968' : {  'min_length' : '7', 'max_length' : '8'},//Oman
                    '92' : {  'min_length' : '8', 'max_length' : '11'},//Pakistan
                    '680' : {  'min_length' : '7', 'max_length' : '7'},//Palau
                    '507' : {  'min_length' : '7', 'max_length' : '8'},//Panama
                    '675' : {  'min_length' : '4', 'max_length' : '11'},//Papua New Guinea
                    '595' : {  'min_length' : '5', 'max_length' : '9'},//Paraguay
                    '51' : {  'min_length' : '8', 'max_length' : '11'},//Peru
                    '63' : {  'min_length' : '8', 'max_length' : '10'},//Philippines
                    '48' : {  'min_length' : '6', 'max_length' : '9'},//Poland
                    '351' : {  'min_length' : '9', 'max_length' : '11'},//Portugal
                    '974' : {  'min_length' : '3', 'max_length' : '8'},//Qatar
                    '40' : {  'min_length' : '9', 'max_length' : '9'},//Romania
                    '7' : {  'min_length' : '10', 'max_length' : '10'},//Russian Federation
                    '250' : {  'min_length' : '9', 'max_length' : '9'},//Rwanda
                    '247' : {  'min_length' : '4', 'max_length' : '4'},//Saint Helena Ascension and Tristan da Cunha
                    '290' : {  'min_length' : '4', 'max_length' : '4'},//Saint Helena Ascension and Tristan da Cunha
                    '508' : {  'min_length' : '6', 'max_length' : '6'},//Saint Pierre and Miquelon
                    '685' : {  'min_length' : '3', 'max_length' : '7'},//Samoa
                    '378' : {  'min_length' : '6', 'max_length' : '10'},//San Marino
                    '239' : {  'min_length' : '7', 'max_length' : '7'},//Sao Tome and Principe
                    '966' : {  'min_length' : '8', 'max_length' : '9'},//Saudi Arabia
                    '221' : {  'min_length' : '9', 'max_length' : '9'},//Senegal
                    '381' : {  'min_length' : '4', 'max_length' : '12'},//Serbia
                    '248' : {  'min_length' : '7', 'max_length' : '7'},//Seychelles
                    '232' : {  'min_length' : '8', 'max_length' : '8'},//Sierra Leone
                    '65' : {  'min_length' : '8', 'max_length' : '12'},//Singapore
                    '421' : {  'min_length' : '4', 'max_length' : '9'},//Slovakia
                    '386' : {  'min_length' : '8', 'max_length' : '8'},//Slovenia
                    '677' : {  'min_length' : '5', 'max_length' : '5'},//Solomon Islands
                    '252' : {  'min_length' : '5', 'max_length' : '8'},//Somalia
                    '27' : {  'min_length' : '9', 'max_length' : '9'},//South Africa
                    '211' : {  'min_length' : '1', 'max_length' : '15'},//South Sudan
                    '34' : {  'min_length' : '9', 'max_length' : '9'},//Spain
                    '94' : {  'min_length' : '9', 'max_length' : '9'},//Sri Lanka
                    '249' : {  'min_length' : '9', 'max_length' : '9'},//Sudan
                    '597' : {  'min_length' : '6', 'max_length' : '7'},//Suriname
                    '268' : {  'min_length' : '7', 'max_length' : '8'},//Swaziland
                    '46' : {  'min_length' : '7', 'max_length' : '13'},//Sweden
                    '41' : {  'min_length' : '4', 'max_length' : '12'},//Switzerland
                    '963' : {  'min_length' : '8', 'max_length' : '10'},//Syrian Arab Republic
                    '886' : {  'min_length' : '8', 'max_length' : '9'},//Taiwan China
                    '992' : {  'min_length' : '9', 'max_length' : '9'},//Tajikistan
                    '255' : {  'min_length' : '9', 'max_length' : '9'},//Tanzania
                    '888' : {  'min_length' : '1', 'max_length' : '15'},//Telecommunications for Disaster Relief (TDR},
                    '66' : {  'min_length' : '8', 'max_length' : '9'},//Thailand
                    '389' : {  'min_length' : '8', 'max_length' : '9'},//The Former Yugoslav Republic of Macedonia
                    '670' : {  'min_length' : '7', 'max_length' : '7'},//Timor-Leste
                    '228' : {  'min_length' : '8', 'max_length' : '8'},//Togo
                    '690' : {  'min_length' : '4', 'max_length' : '4'},//Tokelau
                    '676' : {  'min_length' : '5', 'max_length' : '7'},//Tonga
                    '991' : {  'min_length' : '1', 'max_length' : '15'},//Trial of a proposed new international service shared code
                    '216' : {  'min_length' : '8', 'max_length' : '8'},//Tunisia
                    '90' : {  'min_length' : '10', 'max_length' : '10'},//Turkey
                    '993' : {  'min_length' : '8', 'max_length' : '8'},//Turkmenistan
                    '688' : {  'min_length' : '5', 'max_length' : '6'},//Tuvalu
                    '256' : {  'min_length' : '9', 'max_length' : '9'},//Uganda
                    '380' : {  'min_length' : '9', 'max_length' : '9'},//Ukraine
                    '971' : {  'min_length' : '8', 'max_length' : '9'},//United Arab Emirates
                    '44' : {  'min_length' : '7', 'max_length' : '10'},//United Kingdom
                    '1' : {  'min_length' : '10', 'max_length' : '10'},//United States / Canada / Many Island Nations
                    '878' : {  'min_length' : '1', 'max_length' : '15'},//Universal Personal Telecommunication (UPT},
                    '598' : {  'min_length' : '4', 'max_length' : '11'},//Uruguay
                    '998' : {  'min_length' : '9', 'max_length' : '9'},//Uzbekistan
                    '678' : {  'min_length' : '5', 'max_length' : '7'},//Vanuatu
                    '39' : {  'min_length' : '1', 'max_length' : '11'},//Vatican
                    '379' : {  'min_length' : '1', 'max_length' : '11'},//Vatican
                    '58' : {  'min_length' : '10', 'max_length' : '10'},//Venezuela (Bolivarian Republic of},
                    '84' : {  'min_length' : '7', 'max_length' : '10'},//Viet Nam
                    '681' : {  'min_length' : '6', 'max_length' : '6'},//Wallis and Futuna
                    '967' : {  'min_length' : '6', 'max_length' : '9'},//Yemen
                    '260' : {  'min_length' : '9', 'max_length' : '9'},//Zambia
                    '263' : {  'min_length' : '5', 'max_length' : '10'}//Zimbabwe
                    };
        if(SSKREPO_JSLIB.obj.isset(phonenumber) && SSKREPO_JSLIB.obj.isset(cc) && this.isint(cc) && this.isint(phonenumber))
        {
            if(intl_phone_criteria.hasOwnProperty(cc))
            {
                if(this.length_validate(phonenumber,intl_phone_criteria[cc]['min_length'],intl_phone_criteria[cc]['max_length']))
                return true;
            }            
        }
        return false;
    }

    jsvalidator.prototype.iszipcode = function(value,country)
    {
        switch(country)
        {
            case 'IN':
            var regex = "^[1-9]{1}[0-9]{5}";//6 digit numeric code not starting with 0
            return this.regexp(regex,value);
        }
    }

    jsvalidator.prototype.isalphanum = function(value)
    {
     var regex = "^[0-9a-zA-Z ,\\.\\-_\\\s\\?\\!]+$";
     return this.regexp(regex,value);
    }

    jsvalidator.prototype.isempty = function(value)
    {
     return SSKREPO_JSLIB.obj.empty(value);
    }

    jsvalidator.prototype.isnotempty = function(value)
    {
     return !SSKREPO_JSLIB.obj.empty(value);
    }

    jsvalidator.prototype.isemail = function(value) {
        var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return regex.test(value);
    }
    jsvalidator.prototype.isip = function(value)
    {
        var regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$|^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/i;
        return regex.test(value);
    }
    jsvalidator.prototype.isint = function(value) 
    {
      //return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
      var checkifint =  Number.isInteger || function(value) {
                                    return value != null && //not null 
                                    isFinite(value) &&  //not infinity
                                    Math.floor(value) === value; //not float
                                    
                                                            }
      var intvalue = parseInt(value);
      
      if(value == intvalue)
      {
       		return checkifint(intvalue);	
      }
      else
      return false;
    }
    jsvalidator.prototype.isfloat = function(value)
    {
        var val = parseFloat(value);
        if(isNaN(val))
        return 0;
        else
        return 1;

    }
    jsvalidator.prototype.isurl = function(value)
    {
       return 1; 
    }
    jsvalidator.prototype.length_validate = function(value,minlength,maxlength)
    {

            var err = 0;
            if(SSKREPO_JSLIB.obj.isset(value))
            var var_len = value.length;

            if(SSKREPO_JSLIB.obj.isset(var_len))
            {
                if(SSKREPO_JSLIB.obj.isset(minlength))
                {
                    minlength = Number(minlength);
                    if(var_len < minlength)
                    {
                        err = 1;
                    }
                }
                if(SSKREPO_JSLIB.obj.isset(maxlength))
                {
                   maxlength = Number(maxlength);
                   if(var_len > maxlength)
                    {
                        err = 1;
                    }  
                }
                if(!err)
                return true;
                else
                return false;
            }
    }
    jsvalidator.prototype.password_validate = function(value,validate_cond)
    {
        var reg = new Array(),cond;
        var result = new Array();
        var validation_status = new Array();
        reg['uppercase_count'] = /[A-Z]/g;
        reg['lowercase_count'] = /[a-z]/g;
        reg['digit_count'] = /[0-9]/g;
        //https://kb.wisc.edu/page.php?id=4073 allowed special characters for passwords
        reg['spec_chars_count'] = /[!#\$\&%\'\"\(\)*+-.\/:;<=>?@\^_`{|}~]/g;

        for(cond in reg)
        {
            if(validate_cond.hasOwnProperty(cond))
            {
                var res = value.match(reg[cond]);
                if(typeof res !== undefined && res !== null)
                {
                    if(res.length < validate_cond[cond])
                    {
                        validation_status[cond] = false;
                    }
                    else
                    {
                        validation_status[cond] = true;
                    }

                }
                else
                validation_status[cond] = false;
            }

        }
        return validation_status;


    }
    jsvalidator.prototype.creditcard = (function()
    {
        var my = {};    
        my.$result = new Array();
        my.$cardsarray = new Array();

        my.loadcard_list = function()
        {
           my.$cardsarray['American Express'] = {  
                  'length' : '15', 
                  'prefixes' : '34,37',
                  'checkdigit' : true
                };
           my.$cardsarray['Diners Club Carte Blanche'] ={
                  'length' : '14', 
                  'prefixes' : '300,301,302,303,304,305',
                  'checkdigit' : true
                };
           my.$cardsarray['Diners Club'] ={
                  'length' : '14,16',
                  'prefixes' : '36,38,54,55',
                  'checkdigit' : true
                };
           my.$cardsarray['Discover'] ={ 
                  'length' : '16', 
                  'prefixes' : '6011,622,64,65',
                  'checkdigit' : true
                };
           my.$cardsarray['Diners Club Enroute'] ={ 
                  'length' : '15', 
                  'prefixes' : '2014,2149',
                  'checkdigit' : true
                };
           my.$cardsarray['JCB'] = { 
                  'length' : '16', 
                  'prefixes' : '35',
                  'checkdigit' : true
                };
           my.$cardsarray['Maestro'] = {
                  'length' : '12,13,14,15,16,18,19', 
                  'prefixes' : '5018,5020,5038,6304,6759,6761,6762,6763',
                  'checkdigit' : true
                };
           my.$cardsarray['MasterCard'] = {
                  'length' : '16', 
                  'prefixes' : '51,52,53,54,55',
                  'checkdigit' : true
                };
           my.$cardsarray['Solo'] = { 
                  'length' : '16,18,19', 
                  'prefixes' : '6334,6767',
                  'checkdigit' : true
                };
           my.$cardsarray['Switch'] = {
                  'length' : '16,18,19', 
                  'prefixes' : '4903,4905,4911,4936,564182,633110,6333,6759',
                  'checkdigit' : true
                };
           my.$cardsarray['VISA'] = {
                  'length' : '16', 
                  'prefixes' : '4',
                  'checkdigit' : true
                };
           my.$cardsarray['VISA Electron'] = {
                  'length' : '16', 
                  'prefixes' : '417500,4917,4913,4508,4844',
                  'checkdigit' : true
                };
           my.$cardsarray['LaserCard'] = {
                  'length' : '16,17,18,19', 
                  'prefixes' : '6304,6706,6771,6709',
                  'checkdigit' : true
              };
        };

        my.is_supported_cardtype = function($cardtype)
        {
            if(SSKREPO_JSLIB.obj.isset($cardtype))
            {
              var $type = -1,$ctype;
              my.loadcard_list();
              for($ctype in my.$cardsarray ) 
              {
                  var $cprops = my.$cardsarray[$ctype];
                // See if it is this card (ignoring the case of the string)
                if ($cardtype.toLowerCase() == $ctype.toLowerCase())
                {
                  $type = $ctype;
                  break;
                }
              }
              // If card type not found, report an error
              if ($type == -1) 
              return false;
              else
              return true;
            }
            else
            return false;
        };

        my.check_modulus10 = function($cardnumber)
        {

            if(typeof $cardnumber !== 'undefined' && SSKREPO_JSLIB.obj.isset($cardnumber))
            {
                var $checksum = 0;                                  // running checksum total
                var $mychar = "";                                   // next char to process
                var $j = 1,$i;                                         // takes value of 1 or 2

                // Process each digit one by one starting at the right
                for ($i = ($cardnumber.length) - 1; $i >= 0; $i--) 
                {

                  // Extract the next digit and multiply by 1 or 2 on alternative digits.      
                  var $calc = Number($cardnumber.charAt($i)) * $j;

                  // If the result is in two digits add 1 to the checksum total
                  if ($calc > 9) {
                    $checksum = $checksum + 1;
                    $calc = $calc - 10;
                  }

                  // Add the units element to the checksum total
                  $checksum = $checksum + $calc;

                  // Switch the value of j
                  if ($j ==1) {$j = 2;} else {$j = 1;};
                } 

                // All done - if checksum is divisible by 10, it is a valid modulus 10.
                // If not, report an error.
                if ($checksum % 10 != 0) {
                 return false;
                }
                else
                return true;
            }
            else
            return false;

        };
        my.get_cardtype = function()
        {
            //Card specific Checks
            if(my.hasOwnProperty('$cardnumber'))
            {
                if(!my.hasOwnProperty('$cardsarray') || !SSKREPO_JSLIB.obj.numberOfKeysinObject(my.$cardsarray))
                {
                    my.loadcard_list();
                }
                var $cardtype;
                for($cardtype in my.$cardsarray )
                {
                       // Load an array with the valid prefixes for this card
                       var $cardproperties = my.$cardsarray[$cardtype];
                      var $prefix = $cardproperties['prefixes'].split(',');

                      // Now see if any of them match what we have in the card number  
                      var $PrefixValid = false;
                      var $i;
                      for ($i=0; $i< $prefix.length ; $i++) 
                      {
                           var exp = new RegExp ("^" + $prefix[$i]);
                            if (exp.test (my.$cardnumber))
                            {
                                $PrefixValid = true;
                                break;
                            }
                      }
                      // If it isn't a valid prefix there's no point at looking at the length
                      if ($PrefixValid) 
                      {
                          // See if the length is valid for this card
                          var $LengthValid = false,$j;
                          var $lengths = $cardproperties['length'].split(',');
                          for ($j=0; $j < $lengths.length ; $j++) {
                            if (my.$cardnumber.length == $lengths[$j]) {
                              $LengthValid = true;
                              break;
                            }
                          }

                          // See if all is OK by seeing if the length was valid. 
                          if ($LengthValid) {
                             return $cardtype;
                          }; 
                      }    
                }
                return null;//Tried matching against all supported cardtypes but failed to match, so return null;
            }
            return null;
        };
        my.validate_card = function($cardnumber,$cardtype_to_match)
        {
            $cardtype_to_match = (typeof $cardtype_to_match !== 'undefined')?$cardtype_to_match:null;
            var $result = new Array();
            my.loadcard_list();

            if(SSKREPO_JSLIB.obj.numberOfKeysinObject(my.$cardsarray))//is array
            {
                if(typeof $cardtype_to_match !== 'undefined')//if card type to match is not provided, we match it with all possible cardtypes
                {
                    if(!my.is_supported_cardtype($cardtype_to_match))//if cardtype_to_match is provided then it must be one of the supported ones
                    {
                        $result['status'] = false;
                        $result['error'] = 'Card type give to match is not one of the Supported Card types';
                        $result['card_number'] = null;
                        $result['card_type'] = 'Invalid';
                        return $result;
                    }
                    my.$cardtype_to_match = $cardtype_to_match;
                }


                //Check for Numerics and min and max length of any card possible(13-19)
                if(typeof $cardnumber !== 'undefined')
                {

                    // Remove any spaces or hyphens from the credit card number
                    $cardnumber = $cardnumber.replace (/-/g, "");
                    $cardnumber = $cardnumber.replace (/\s/g, "");

                    var cardexp = /^[0-9]{13,19}$/;
                    if (!cardexp.exec($cardnumber))
                    {
                        $result['status'] = false;
                        $result['error'] = 'Card number provided is not having right length or is having non-numeric characters';
                        $result['card_number'] = my.$cardnumber;
                        $result['card_type'] = 'Invalid';
                        return $result;
                    }
                    my.$cardnumber = $cardnumber;
                }
                else
                {
                    $result['status'] = false;
                    $result['error'] = 'Card Number Cannot be empty';
                    $result['card_number'] = null;
                    $result['card_type'] = 'Invalid';
                    return $result;
                }

                //Check for modulus10 of card is valid
                if(!my.check_modulus10(my.$cardnumber))
                {
                    $result['status'] = false;
                    $result['error'] = 'Cardnumber failed Modulus10 verfication';
                    $result['card_number'] = my.$cardnumber;
                    $result['card_type'] = 'Invalid';
                    return $result;
                }
                else//passed modulus10 verification
                {
                    var $cardtyp = my.get_cardtype();
                    if($cardtyp == null)
                    {
                        $result['status'] = false;
                        $result['error'] = 'Cardnumber doesnt match any supported cardtype';
                        $result['card_number'] = my.$cardnumber;
                        $result['card_type'] = 'Invalid';
                        return $result;
                    }
                    else
                    {    
                        if(!my.hasOwnProperty('$cardtype_to_match')) 
                        {
                            $result['status'] = true;
                            $result['error'] = null;                       
                        }
                        else if(my.$cardtype_to_match.toLowerCase() == $cardtyp.toLowerCase())
                        {
                            $result['status'] = true;
                            $result['error'] = null;
                        }
                        else
                        {
                            $result['status'] = false;
                            $result['error'] = 'Cardnumber is a valid creditcard number, but not the same type as '+my.$cardtype_to_match;                 
                        }
                        $result['card_number'] = my.$cardnumber;
                        $result['card_type'] = $cardtyp;
                        return $result;

                    }

                }

            }
        };

        return my;
    }());
    
    me.jsvalidator = new jsvalidator;
    
    me.is_condition_supported = function(cond_op)
    {
        return me.jsvalidator.validate(cond_op,null,null,null,true);
    }
    return me;
}());

/**
 * Checks if element is having any errors in element_error_array , if yes returns false, else returns true
 * @function is_element_valid
 * @memberof SSKREPO_FORM_JS
 * @param {jQuery} jquery_element
 * @returns {Boolean} Returns true if no errors for element, else false
 */
my.is_element_valid = function(jquery_element)
{
    //this wont fire any events on a element, this will just return current status of element based if any errors are there for this element in element_error_array
    
    var elementid = jquery_element.attr('id');
    if(my.element_error_array.hasOwnProperty(jquery_element.attr('id')))
    {
        if(my.element_error_array[jquery_element.attr('id')].length > 0)
        return false;
        else
        return true;
    }
    else
    return true;
}

/**
 * Get a list of elements that are dependent on element_id
 * inform those dependents, that I have a change, recheck your dependency on me
 * those elements can check, if they are effected compared to their current state(means they are going to have a change in their state)
 * they get their list of dependents and inform them to recheck their dependency on them
 * this goes on as a chain till we end up with a leafs of the tree, which are not having any dependents on them
 * On problem here is is cyclic dependencies, Say B depends on A and C Depends on B, and A depends on C.
 * Now when A changes, A refresh B , if B is effected by change in A , then B refresh C, if C is effected then C refresh A
 * and this cycle goes on infinitely. So We track recursion depth,means we track all elements that already the source for refresh in the chain
 * and we dont issue a refresh on them again down the line in the chain to prevent infinite loops 
 * @function refresh_dependent_elements
 * @memberof SSKREPO_FORM_JS
 * @param {string} elementid ID of element for which dependents need to be refreshed
 * @param {array} refresh_source_array Used to track source in the refresh chain so that we dont end up refreshing the same element due to cyclic dependencies
 * @returns {Function} Returns a Callback function that can be used  in event function like $.on() , Used by events_validator()
 * @see events_validator
 */

function refresh_dependent_elements(elementid,refresh_source_array)
{
    
    var dependents = my.elements_that_depend_onme.hasOwnProperty(elementid)?my.elements_that_depend_onme.hasOwnProperty(elementid):null;
    var dependent,dep_cond,error_before_refresh,error_after_refresh,rec_count=0;
    
    return function(trigevent)
    {
        if(rec_count == 0)
        {
            elementid = $(this).attr('id');
            refresh_source_array = new Array();
        }
        else if(rec_count > 0)
        elementid = dependent;
        
        if(typeof refresh_source_array === 'undefined' || refresh_source_array === null )
        {
            refresh_source_array = new Array();
        }

        if(!refresh_source_array.hasOwnProperty(elementid))//prevent infinite loop
        {
            refresh_source_array[elementid] = 1;//to track source in the refresh chain so that we dont end up refreshing the same element due to cyclic dependencies

            if(dependents !== null)
            {
                for(dependent in my.elements_that_depend_onme[elementid])
                {
                    if(dependent )
                    dep_cond = my.elements_that_depend_onme[elementid][dependent];
                    error_before_refresh = get_error_text_of_element(dependent);
                    var element = SSKREPO_JSLIB.obj.isset($('#'+dependent))?$('#'+dependent):null;
                    var element_cond = new Array(),dep_type;
                    for(dep_type in dep_cond)
                    {
                        if(dep_type == 'required')
                        {
                            element_cond['op'] = 'required';
                            element_cond['arg'] = dep_cond[dep_type]['arg'];
                            var errormessage = null;//for required elements required itslef will not have any error, becuase errormessage is in specific condition['arg'];
                            //validate_element(element_cond,element,null,trigevent);
                            var res = validate_and_adderror(element_cond,null,element);
                            res();
                        }
                        else if(dep_type == 'revalidate_self')
                        {
                            var op;
                        
                            for(op in dep_cond[dep_type])
                            {
                                element_cond['op'] = op;
                                element_cond['arg'] = dep_cond[dep_type][op].hasOwnProperty('arg')?dep_cond[dep_type][op]['arg']:null;
                                if(dep_cond[dep_type][op].hasOwnProperty('error'))
                                var errormessage = dep_cond[dep_type][op]['error'];//for required elements required itslef will not have any error, becuase errormessage is in specific condition['arg'];
                                else
                                var errormessage = null;
                            
                                var el = $('#'+dependent);
                                if(SSKREPO_JSLIB.obj.isset(el))
                                {
                                    var res = validate_and_adderror(element_cond,errormessage,el);
                                    res();
                                }
                            }
                            
                        }
                    }

                    error_after_refresh = get_error_text_of_element(dependent);           

                    if(error_before_refresh !== error_after_refresh)//dependent is effected by change on source element
                    {
                         rec_count++;//to track recursion depth;
                         arguments.callee(trigevent);//recursion - this function recursively
                         rec_count--;
                    }
                }
            }
            delete(refresh_source_array[elementid]);
            
        }
        if(rec_count == 0)
        {
            refresh_cyclic_dependencies(elementid);
        }
    }
}

/**
 * Implode all errors for a specific element in element_error_array using '||' and return the same. 
 * @function get_error_text_of_element
 * @memberof SSKREPO_FORM_JS
 * @param {string} elementid ID of the element for which to check for errors
 * @returns {String} Returns implode of all errors for a element using '||'
 */
function get_error_text_of_element(elementid)
{
    if(SSKREPO_JSLIB.obj.isset(elementid))
    {
        if(my.element_error_array.hasOwnProperty(elementid))
        {
            if(my.element_error_array[elementid].length > 0)
            {
                var errorhtml;
                if(my.element_error_array[elementid].length > 1 )//implode with || only if atleast two errors are there
                errorhtml = SSKREPO_JSLIB.array.implode("||",my.element_error_array[elementid]);
                else if(my.element_error_array[elementid].length == 1)
                errorhtml = my.element_error_array[elementid][0];

                return errorhtml;
            }
            else
            return null;
        }
        else
        return null;
             
    }
    else
    return null;
    
}

/**
 * 
 * Find all cyclic dependencies in the validation array derived from Validation JSON and stores the same in cyclic_dependencies_array
 * Check refresh_cyclic_dependencies() for more explanation on why we need to take care of cyclic depenendencies
 * @function find_cyclic_dependencies
 * @memberof SSKREPO_FORM_JS
 * @param {array} element_dep_array Same as SSKREPO_FORM_JS.required_valid_element_array
 * @param {type} source_array Used to manage recursion of this function, Array of elements that are currently higher in the stack in recursion.
 * @param {type} processed_array Used to manage recursion of this function, Array of elements that are already processed before and for which cyclic depenedecy chains are found, so that we dont have to process the chain again.
 * @returns none
 */
function find_cyclic_dependencies(element_dep_array,source_array,processed_array)
{
    if(!SSKREPO_JSLIB.obj.isset(source_array))
    var source_array = new Array();

    if(!SSKREPO_JSLIB.obj.isset(processed_array))
    var processed_array = new Array();//to check if that element is already processed, if yes there is no need to process it again.

    var dep,elementid;
    if(SSKREPO_JSLIB.obj.isset(my.required_valid_element_array) && SSKREPO_JSLIB.obj.isset(element_dep_array))
    {
        for(elementid in element_dep_array)
        {
            if($.inArray(elementid,processed_array) === -1)
            {
                var source_length = source_array.length;
                var index_of_dep = $.inArray(elementid,source_array);
                if(index_of_dep == -1)//dep not in source_array
                {
                    source_array.push(elementid);
                    
                    if(my.required_valid_element_array.hasOwnProperty(elementid))
                    arguments.callee(my.required_valid_element_array[elementid],source_array,processed_array);
                    source_array.pop();
                }
                else//index_of_dep will have index number where dep is in source_array,its cyclic array, capture it
                {
                    var i = index_of_dep,cyc_array = new Array(),j=0;
                    for(i = index_of_dep ; i < source_array.length ; i++)
                    {
                        cyc_array[source_array[i]] = j;
                        j++;
                    }
                    var curr_cyclic_array_length = my.cyclic_dependencies_array.length;
                    my.cyclic_dependencies_array[curr_cyclic_array_length] = cyc_array;
                }
            }
        }
        processed_array.push(elementid);//to check if that element is already processed, if yes there is no need to process it again.
    }
}

/**
 * 
 * Use cyclic_dependencies_array, and find all cyclic chains that this element is part of, and then extract all elements which are part of those cyclic chains
 * @function get_unique_elements_inmy_cyclic_chain
 * @memberof SSKREPO_FORM_JS
 * @param {string} elementid
 * @returns {Array} Array of element ids , which are part of cyclic chains, in which "elementid" is a member
 */
function get_unique_elements_inmy_cyclic_chain(elementid)
{
    var i,cyc_elem;
    var my_cyc_array = get_my_cyclicchains(elementid);
    var my_unique_cyclic_elems = new Array();
    
    if(SSKREPO_JSLIB.obj.isset(my_cyc_array))
    {
        for(i in my_cyc_array)
        {
            for(cyc_elem in my_cyc_array[i])
            {
                my_unique_cyclic_elems[cyc_elem] = 1;
            }
        }
    }
    return my_unique_cyclic_elems;  
}

/**
 * 
 * Get list of elements that are part of cyclic chains in which Given element is part of using get_unique_elements_inmy_cyclic_chain() and then
 * get all the error messages from those elements "required"->"valid" conditions. Used by refresh_cyclic_dependencies().
 * @function get_my_cyclicchain_errors
 * @memberof SSKREPO_FORM_JS
 * @param {type} elementid
 * @returns {Array} Array of error messages indexed by elementid
 * @see refresh_cyclic_dependencies
 */
function get_my_cyclicchain_errors(elementid)
{
    var my_cyc_array = null,i,my_position, I_depend_on = new Array(),dep_elem,cyc_dep_errors;
    
    my_cyc_array = get_my_cyclicchains(elementid);
    if(my_cyc_array !== null)
    {
       for(i in my_cyc_array)
       {
           my_position = my_cyc_array[i][elementid];
           if( (my_position+1) == SSKREPO_JSLIB.obj.numberOfKeysinObject(my_cyc_array[i]))//element is last in chain so it depends on element at 0 position
           {
               //get element id at position 0;
               dep_elem = SSKREPO_JSLIB.obj.findPropertyWithValue(0,my_cyc_array[i]);
               I_depend_on[dep_elem] = 1;
           }
           else
           {
               dep_elem = SSKREPO_JSLIB.obj.findPropertyWithValue(++my_position,my_cyc_array[i]);
               I_depend_on[dep_elem] = 1;
           }
       }
       
       //build errors that elementid has in its required condition for elements in I_depend_on
       var k = 0,cyc_dep_errors= new Array();
       for(dep_elem in I_depend_on)
       {
           if(my.required_valid_element_array[elementid].hasOwnProperty(dep_elem))
           {
               cyc_dep_errors[k] = my.required_valid_element_array[elementid][dep_elem].trim();
               k++;
           }
       }
       
       return cyc_dep_errors;        
    }

}

/**
 * Get all cyclic chains from cyclic_dependencies_array in which given element is part of
 * @function get_my_cyclicchains
 * @memberof SSKREPO_FORM_JS
 * @param {string} elementid
 * @returns {Array} Array of cyclic chains in which given element is part of
 */
function get_my_cyclicchains(elementid)
{
    var i,cyc_array = new Array();
    if(my.cyclic_dependencies_array.length > 0)
    {
        for(i in my.cyclic_dependencies_array)
        {
            if(my.cyclic_dependencies_array[i].hasOwnProperty(elementid))
            {
                cyc_array[cyc_array.length] = my.cyclic_dependencies_array[i];
            }
        }
        return cyc_array;
    }
}

/**
 * Take all cyclic chains that elemenid is involved
 * find unique element list from all the chains
 * for each uniqiue element, get list of errors from its cyclic conditions
 * get a list of errors for each element that are currently in element_error_array
 * check if all current errors are only cyclic dep errors on all unique elements
 * if yes , remove all of them
 * if not just do nothing
 * 
 * @function refresh_cyclic_dependencies
 * @memberof SSKREPO_FORM_JS
 * @param {type} elementid
 * @returns {undefined}
 */

function refresh_cyclic_dependencies(elementid)
{
    var eachelem,all_cyclic_errors = new Array(),errortext,error_array,error,youcantremove_errors = null;
    var my_unique_cyclic_elements = get_unique_elements_inmy_cyclic_chain(elementid);
    if(SSKREPO_JSLIB.obj.isset(my_unique_cyclic_elements))
    {
        youcantremove_errors = 'no';
        for(eachelem in my_unique_cyclic_elements)
        {
            all_cyclic_errors[eachelem] = get_my_cyclicchain_errors(eachelem);
        }
        for(eachelem in all_cyclic_errors)
        {
           errortext = get_error_text_of_element(eachelem);
           if(SSKREPO_JSLIB.obj.isset(errortext))
           {
               errortext = errortext.trim();
               errortext = SSKREPO_JSLIB.string.trimChar(errortext,'|');//remove || if in start or at end
               error_array = errortext.split('||');
               if(errortext !== "")//If no error we are fine no need to split with || and check for subset
               {
                   for(error in error_array)
                   {
                       error_array[error] = error_array[error].trim();

                   }
                   if(!SSKREPO_JSLIB.array.isAsubsetofB(error_array,all_cyclic_errors[eachelem]))
                   {
                       youcantremove_errors = 'yes';
                       break;//break for loop as even if one element is having more errors than its cyclic dependency errors, we cant remove them, so no point looping through all other elements
                   }
               }
           }
        }  
        if(youcantremove_errors !== 'yes' && youcantremove_errors !== null)
        {
            //you can remove errors from elementid and then refresh elementid oncemore
            for(elementid in all_cyclic_errors)
            {
                var errors_to_remove = all_cyclic_errors[elementid];
                var elem = $('#'+elementid);
                if(SSKREPO_JSLIB.obj.isset(elem))
                {
                    for(error in errors_to_remove)
                    {
                        remove_error(elem,errors_to_remove[error]);
                    }
                }
            }            
        }
    }    
}
/**
 * Loads all plugins to my.plugins, if external plugin(in plugins_array) is valid, load it, else load default plugins
 * @function plugin_loader
 * @memberof SSKREPO_FORM_JS
 * @returns none
 * @see load_plugin
 */
my.plugin_loader = function()
{
    var plugin_object,plugin_type;
    if(my.params.hasOwnProperty('plugins_array'))
    {
        my.plugins_array = my.params.plugins_array; //get global variable with name as value of plugin_name
    }
    if(typeof my.plugins_array !== 'undefined')
    {
        for(plugin_type in my.plugins_supported)
        {
            if(my.plugins_array.hasOwnProperty(plugin_type))
            {
                plugin_object = window[my.plugins_array[plugin_type]];
                if(my.plugin_validate(plugin_type,plugin_object))
                {
                    my.load_plugin(plugin_type,plugin_object);
                }
                else
                {
                    //plugin_object = my.default_plugin_object[plugin_type];
                    my.load_plugin(plugin_type,'default');
                }
            }
            else
            my.load_plugin(plugin_type,'default');
        }
    }
    else//load all default plugins, if plugins_array is not defined by user.
    {
        for(plugin_type in my.plugins_supported)
        {
           my.load_plugin(plugin_type,'default'); 
        }
    }
}
/**
 * Load a plugin_object of type plugin_type into my.plugins
 * @function load_plugin
 * @memberof SSKREPO_FORM_JS
 * @param {string} plugin_type Type of Plugin to load. For all types supported, Please see my.plugins_supported
 * @param {Object} plugin_object Plugin Object of type plugin_type
 * @returns None
 */
my.load_plugin = function(plugin_type,plugin_object)//loads a single plugin
{
    if(typeof plugin_type !== 'undefined' && typeof plugin_object !== 'undefined')
    {
        if(!my.plugins.hasOwnProperty(plugin_type))
        {
          my.plugins[plugin_type] = new Array();
        }
        if(plugin_object === 'default')
        {   
            //load default plugin
            my.plugins[plugin_type]['obj'] =  my.plugins_default[plugin_type];
            my.plugins[plugin_type]['loaded'] = 'default';
        }
        else
        {
            //load custom external plugin
            my.plugins[plugin_type]['obj'] = plugin_object;
            my.plugins[plugin_type]['loaded'] = 'external';
        }
    }
}

/**
 * Validate a Plugin object provided in plugins_array for its type(plugin_type). Check if all required methods and properties are implemented in plugin_object for a specific plugin_type
 * @function plugin_validate
 * @memberof SSKREPO_FORM_JS
 * @param {string} plugin_type Type of Plugin to load. For all types supported, Please see my.plugins_supported
 * @param {Object} plugin_object Plugin Object of type plugin_type
 * @returns {Boolean} True if Plugin is valid,else false
 */
my.plugin_validate = function(plugin_type,plugin_object)
{
    //checks if a plugin is having all required properties and methods
    if(typeof plugin_type !== 'undefined' && typeof plugin_object !== 'undefined')
    {
        var ext_plugin_struct = SSKREPO_JSLIB.obj.get_Methods_and_Props(plugin_object);
        var default_plugin_struct = SSKREPO_JSLIB.obj.get_Methods_and_Props(my.plugins_default[plugin_type]);
        if(SSKREPO_JSLIB.array.isAsubsetofB(default_plugin_struct['methods'],ext_plugin_struct['methods']))//if all methods for plugin_type are defined in plugin_object
        {
            if(SSKREPO_JSLIB.array.isAsubsetofB(default_plugin_struct['props'],ext_plugin_struct['props']))//if all non-method properties for plugin_type are defined in plugin_object
            {
                return true;
            }
            return false;
        }
        else
        return false;
    } 
}
/**
 * Returns structure of a Plugin_object for a specific plugin_type
 * @function plugin_structure
 * @memberof SSKREPO_FORM_JS
 * @param {type} plugin_type
 * @returns {Array|sskrepo_jslib_L80.my.get_Methods_and_Props.methods_props}
 */
my.plugin_structure = function(plugin_type)
{
    //var plugin_struct = new Array();
    //returns plugin methods and properties to be present for the plugin to be valid
    if(my.plugins_default.hasOwnProperty(plugin_type))
    {
        return SSKREPO_JSLIB.obj.get_Methods_and_Props(my.plugins_default[plugin_type]);
        
    }
    else
    return null;
}

return my;
}());

//$.getScript("https://newlogin.letterbox.in/tests/opentip-jquery-excanvas.js");
//$.extend(SSKREPO_FORM_JS,new_plugin);
//$(document).ready(SSKREPO_FORM_JS.events_validator());
