<?php

/**
 * form.php - Main form class
 * @author Sravan Kumar S
 * @see \\sskrepo\\sklib\\form\\form
 */
 

namespace sskrepo\sklib\form;
/**
 * Below constants are for simple_html_dom to work, as defined in 
 * Website: https://sourceforge.net/projects/simplehtmldom/
 * Acknowledge: Jose Solorzano (https://sourceforge.net/projects/php-html/)
 */
define('HDOM_TYPE_ELEMENT', 1);
define('HDOM_TYPE_COMMENT', 2);
define('HDOM_TYPE_TEXT',	3);
define('HDOM_TYPE_ENDTAG',  4);
define('HDOM_TYPE_ROOT',	5);
define('HDOM_TYPE_UNKNOWN', 6);
define('HDOM_QUOTE_DOUBLE', 0);
define('HDOM_QUOTE_SINGLE', 1);
define('HDOM_QUOTE_NO',	 3);
define('HDOM_INFO_BEGIN',   0);
define('HDOM_INFO_END',	 1);
define('HDOM_INFO_QUOTE',   2);
define('HDOM_INFO_SPACE',   3);
define('HDOM_INFO_TEXT',	4);
define('HDOM_INFO_INNER',   5);
define('HDOM_INFO_OUTER',   6);
define('HDOM_INFO_ENDSPACE',7);
define('DEFAULT_TARGET_CHARSET', 'UTF-8');
define('DEFAULT_BR_TEXT', "\r\n");
define('DEFAULT_SPAN_TEXT', " ");
define('MAX_FILE_SIZE', 600000);

/**
 * @class form : Load a HTML,
 *              Load validation criteria,
 *              Check which form submitted,
 *              Load submitted data to form elements, 
 *              Validate submitted form,
 *              Add/remove JS validation
 *              Supports Plugins
 *                  Validate Plugins
 *                  Load valid plugins
 *  
 *              
 *              
 */
class form
{
    
    private $data;
    private $uniq_input_elements = array();
    private $request;
    private $fulldata = array();
    private $inputelementlist = array("input","textarea","select");
    /**
     * safeinput type
     * @var boolean $safeinput
     */
    public $safeinput;
    /**
     * validation_json will have validation json from <validatecrit> comment section,to be used when adding javascript validation to form.
     * @var json $validation_json
     */
    public $validation_json; 
    /**
     * $this->validation_json data parsed to array
     * @var array $validation_json_array
     */
    public $validation_json_array; 
    /**
     * Set to true by add_js_validation(),when js validation is enabled, else false. This is used by display()
     * @var boolean $js_validation
     */
    public $js_validation = false;
    /**
     * Used by add_error_to_element, to manually add a errormessage to element.
     * @var integer $manual_error_count
     * @see add_error_to_element()
     */
    public $manual_error_count = 0;
    /**
     * Gets incremented if there is error for an element, during validtion. Used by add_error/remove_error
     * loadForm()->sets it to -1,so if -1, then form is never validated
     * validate()->sets it to 0, if no errors it remains at 0, if not it will be equal to number of errors
     * @var integer $validate_error_count
     * @see loadForm()
     * @see validate()
     */
    public $validate_error_count = 0;//Used by validate()/validate_element();
    /**
     * Sum of $manual_error_count and $validate_error_count;
     * @var integer $error_count
     * 
     */
    public $error_count = 0;
    /**
     * Current Submitted form element
     * @var simple_html_dom_node $currentform
     * 
     */
    public $currentform;
    /**
     * Validation criteria applicable only for onsubmit from $validation_json
     * @var array $formevents_validation_array
     * 
     */
    public $formevents_validation_array;
    /**
     * 0 if json is invalid or no json at all, 1 if valid json
     * @var boolean $is_json_valid
     * 
     */
    public $is_json_valid;
    /**
     * contains element specific validation array
     * @var array $element_event_validation_array
     * 
     */
    public $element_event_validation_array;
    
    //public $required_element_array = array();set when required condition is being validated for a element, to prevent infinite loops between elements required conditions
    /**
     * all javascript includes added using $this->add_js_script();
     * @var array $js_include
     * @see add_js_script()
     * 
     */
    public $js_include;
    /**
     * contains php plugin object which should have plugin methods and/or properties implemented
     * @var array $php_plugins_array
     * 
     */
    
    public $php_plugins_array;
    /**
     * contains all error messages for each element, indexed by elementid
     * @var array $errormessage_stack
     * 
     */
    public $errormessage_stack;
    /**
     * All elements that a specific element depends on for valid condition in required condition, indexed by elementid
     * @var array $elements_that_I_depend_for_valid_cond
     * 
     */
    public $elements_that_I_depend_for_valid_cond = array();
    /**
     * All elements that depend on a specific element for valid condition, indexed by elementid
     * @var array $elements_that_depend_onme_for_valid_cond
     * 
     */
    public $elements_that_depend_onme_for_valid_cond = array();
    /**
     * All elements that a specific element depends on, indexed by elementid
     * @var array $elements_that_I_depend_on
     * 
     */
    public $elements_that_I_depend_on = array();
    /**
     * if user wants to add their preferred version of jQuery
     * @var string $js_jsquery_script
     * 
     */
    public $js_jsquery_script;
    /**
     * Used by prepare_js_validation_scripts(), to load the final js include string that need to be appended to HTML for display
     * @var string $js_final_include_string
     * @see prepare_js_validation_scripts()
     */
    public $js_final_include_string;
    /**
     * used by add_js_plugin
     * @var string $js_plugin_scripts
     * @see add_js_plugin()
     */
    public $js_plugin_scripts;
    /**
     * Path of MAIN js validation script
     * @var string $latest_validation_js
     * 
     */
    public $latest_validation_js;
    /**
     * used by add_js_plugin, array of plugin_types => plugin_object names
     * @var string $js_plugins_array
     * @see add_js_plugin()
     */
    public $js_plugins_array;
    /**
     * used by add_js_validation, if parameter cookie_use is true, then this is set to true, else false
     * If cookie can be used to store validation criteria and sending validation errors, default is true;
     * @var boolean $js_use_cookie
     * @see add_js_validation
     */
    public $js_use_cookie;
    /**
     * entire parameter set to be passed to JS,either through setting param cookie or passed as json to JS, depending on $this->js_use_cookie is true or false
     * @var string $js_parameter_array;
     * @see add_js_parameter()
     * @see js_prepare_parameters()
     */
    public $js_parameter_array;
    /**
     * contains currently loaded plugins(either external or default depending on if external plugin is available and valid)
     * @var array $php_plugins
     * @see php_plugin_loader()
     * 
     */
    public $php_plugins;
    /**
     * default plugin objects array for all plugins
     * @var $php_plugins_default
     * 
     * 
     */
    public $php_plugins_default;
    /**
     * array all supported php plugin types, loaded by php_plugin_loader
     * @var $php_plugins_supported
     * @see php_plugin_loader()
     * 
     */
    public $php_plugins_supported;
    
    /**
     * creates instance of safeinput to $this->safeinput
     * @see \sskrepo\sklib\safeinput
     */
    public $yui_profiler = false;
    
    public function __construct()
    {
        $this->safeinput = new \sskrepo\sklib\safeinput;
    }
    public function __get($varName)
    {
      if (array_key_exists($varName,$this->data))
      {
          return $this->data[$varName];
      }     
    }
    public function __set($varName,$value)
    {
      $this->data[$varName] = $value;
    }
    
    /**
     * Loads HTML file or HTML String provided
     * Automatically checks for existance of validationcrit element for validation criteria, If yes will load it
     * Also if withdata, then checks $_REQUEST, automatically findsout which form is submitted in the HTML, loads all elements inside form as per $_REQUEST Data
     * Supports arrays as element names as well
     * 
     * @param string $load This can take two values ,withdata or withoutdata, if withoutdata Loadform() will just load the form without any data from $_REQUEST. This is is useful when
     * form html file may contain php code for action etc, so we need to execute form content so that php code gets executed first
     * and then load the form data, becuase then it will be only html and text
     * 
     * @param string $formfilepath Path of the HTML File that needs to be loaded
     * @param array $view Variables inside HTML that may be dynamic and need to replaced with values from $view array before loading data from $_REQUEST to HTML, NULL when no such dynamic data in HTML
     * @see $this->getscriptoutput()
     * @return void
     * @code
     * Example:
     * $html = new \sskrepo\sklib\form\form;
       $html->loadForm('/var/www/newsso/public/home/tests/new_form_js_8.html');
     * @endcode
     */   
    public function loadForm($formfilepath,$view = NULL,$load='withdata') //$view will take a view object supposed to have all values referenced in html form
    {
        
        $form_after_php = $this->getscriptoutput($formfilepath,$view);
        $this->validate_error_count = '-1'; //to differentiate if we validated a form or not, if validate was called then this will be set to 0 and remains that way if validation pass,  and if validate has errors this will be 1 or more
        /*Load HTML*/
        $this->domobj = $this->str_get_html($form_after_php);
        
        $this->add_from_form_id();
       
        if($load == 'withdata')//this is default, user has to explicitly set load to withoutdata for not loading data as below
        {
            $request = $_REQUEST;

            /******************************************************/
            //Make a list of Unique input element names in HTML Forms, so that we can use that list to fill the form with user submitted data
            $this->uniq_input_elements = array();
            if(isset($_REQUEST['from_form-id-1824']))
            {
                $formid = explode("form-id-1824",$_REQUEST['from_form-id-1824']);
                $formid = $formid[0];
                $forms = $this->domobj->find('form');
                foreach($forms as $form)
                {
                   if($form->id == $formid)
                   {
                       $this->currentform = $form;
                
                        foreach ($this->inputelementlist as $ielement)
                        {
                            $inputelements = $this->currentform->find($ielement);
                            //we store element names as $this->uniq_input_elements['elementname'] = 0 or 1, 0 if not array 1 if array
                            foreach ($inputelements as $t)
                            {
                                if(isset($t->name))
                                {
                                    $el = explode('[',$t->name);
                                    if(count($el) == 1) //not array and there is no [ in element name
                                    {
                                        $this->uniq_input_elements[$el[0]]['isarray'] = 0;
                                        $this->uniq_input_elements[$el[0]][$t->name] = 1;
                                        // when request is not set(form loading first time), and if input element is having value attriute set, then we should not override it, if $val is NULL because REQUEST is not set yet.
                                        $val = isset($_REQUEST[$t->name])?$_REQUEST[$t->name]:NULL;

                                        if($ielement == 'input')
                                        {   

                                            if($t->type == 'checkbox' or $t->type == 'radio')  //if it is checkbox
                                            {
                                                //for checkbox value is always hardcoded in HTML without any submit. So we should not overwrite it, and also for checkbox if submitted value == existing value then we should set "checked" attribute to "checked"
                                                if($t->value == $val)
                                                $t->checked = "checked";

                                            }
                                            else 
                                            $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);

                                        }
                                        if($ielement == 'textarea')//if it is textarea, textarea element is not having value attribute so have to add it to innertext
                                        {                       
                                            $t->innertext .= isset($val)?$val:NULL;//no htmlspecchars needed as even if you have <html> inside textarea tag browser show <html> only it wont consider it as <html>
                                        }
                                        if($ielement == 'select')
                                        {   //for select elements $_REQUEST['selectelementname'] is coming from <option> elements inside it.
                                            //So if we want to preserve which option user selected from drop down, then we need to set "selected" attribute of that option element to "selected"(.i.e. <option selected="selected>)
                                            //if <select> element name is dept and we got $_REQUEST['dept'] as 2, then we need to find <option value=2> and make it <option value=2 selected="selected">
                                            //$t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                            //$selectvalue = $t->value; 
                                            //If the we have "$_REQUEST['selectelement_name'] is set, then we know atleast one option for this select list is selected and submitted.
                                            //So we need remove "selected" from default option which is selected
                                            //Then for all values in array $_REQUEST['selectelement_name'] find the corresponding option and updated its "selected" attribute to "selected" This is for select elements which can be used for "multiple" options to be selected.
                                            //since select element is only one element, we will not get a chance to get to this element again in the for loop, so for all options selected for this select element, update all of them at once

                                            //$t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                            //$selectvalue = $t->value; 

                                            //check if $_REQUEST['selectelement_name'] is set
                                            if(isset($_REQUEST[$t->name]))
                                            {
                                                foreach($t->find('option') as $t1)
                                                {
                                                    //$inner = $t1->innertext;
                                                    $optionvalue = $t1->value;
                                                    $optionset = false;
                                                    $selectedvalue = $_REQUEST[$t->name];

                                                    if($optionvalue == $selectedvalue)
                                                    {
                                                       $optionset = true;
                                                    }

                                                    if($optionset)
                                                    {
                                                        $t1->selected = "selected";
                                                    }
                                                    elseif(isset($t1->attr['selected']))//nullify "selected" attribute for all elements which are not set, effectively removing default options for select list                                                   
                                                    {
                                                        unset($t1->attr['selected']);
                                                    }
                                                }
                                            }                                           
                                        }
                                    }
                                    
                                    if(count($el) > 1) // there is [ in element name
                                    {
                                        $this->uniq_input_elements[$el[0]]['isarray'] = 1;
                                        $this->uniq_input_elements[$el[0]][$t->name] = 1;

                                        $elementname = $el[0];
                                        $arraypart = preg_match("/(\[.*)$/",$t->name,$match);
                                        $arraypart = $match[0];
                                        $elarr = explode('[]',$arraypart);

                                        if(count($elarr) > 1) //atleast one blank braces are there in element name
                                        {   
                                            if(!isset($this->uniq_input_elements[$el[0]]['count']))
                                            $this->uniq_input_elements[$el[0]]['count'] = 0;
                                            $i = $this->uniq_input_elements[$el[0]]['count'];
                                            $j = count($elarr)-1;
                                            $count = 0;
                                            $postnamearray = NULL;
                                            while($count <= $j)
                                            {   
                                                if($count == 0)
                                                $index = $i;
                                                else
                                                $index = 0;

                                                if($count != $j)
                                                $postnamearray .= $elarr[$count].'['.$index.']';
                                                else
                                                $postnamearray .= $elarr[$count];
                                                $count++;
                                            }

                                            $reqstring = '['.$el[0].']'.$postnamearray;
                                            preg_match_all("/\[(.*?)\]/",$reqstring,$matchs);
                                            $val = $this->get_array_keys_values($_REQUEST,$matchs[1]);
                                            //$val = $val[$matchs[1]];
                                            if($ielement == 'input')
                                            {   
                                                if($t->type == 'checkbox' or $t->type == 'radio') //if it is checkbox
                                                {
                                                    //for checkbox value is always hardcoded in HTML without any submit. So we should not overwrite it, and also for checkbox if submitted value == existing value then we should set "checked" attribute to "checked"
                                                    if($t->value == $val)
                                                    {
                                                    $t->checked = "checked";
                                                    $this->uniq_input_elements[$el[0]]['count']++;//increment count only if checkbox is actually ticked. Only ticked checkboxes are submitted unlike other input types
                                                    //for example if you have following checkboxes in html file
                                                    /*
                                                     * <li>JavaScript: <input type="checkbox" name="checkin[]" id="javascript" value="javascript" /></li>
                                                        <li>CSS: <input type="checkbox" name="checkin[]" id="css" value="css"/></li>
                                                        <li>PHP: <input type="checkbox" name="checkin[]" id="php" value="php"/></li>
                                                        <li>Ruby: <input type="checkbox" name="checkin[]" id="ruby" value="ruby"/></li>
                                                        <li>Python: <input type="checkbox" name="checkin[]" id="python" value="python"/></li>

                                                     *  in this case when user checks say javascript and php only and submits
                                                     * then submit string will be checkin[]=javascript&checkin[]=php
                                                     * so $_REQUEST will be
                                                     * $_REQUEST[0] = 'javascript' and $_REQUEST[1]='php';
                                                     * For other input types like text all of them gets submitted with empty values
                                                     * if it is a text type then submit string will be
                                                     * $_REQUEST[0]='javascript',$_REQUEST[1]='', $_REQUEST[2]='php',$_REQUEST[3]=''
                                                     * 
                                                     * So in checkbox case count should be incremented only when checkbox is submitted.
                                                     * otherwise we will endup checking wrong boxes in output after loading and displaying                                */
                                                    }

                                                }
                                                else
                                                {
                                                    $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                    $this->uniq_input_elements[$el[0]]['count']++;                               
                                                }

                                            }   
                                            if($ielement == 'textarea')//if it is textarea, textarea element is not having value attribute so have to add it to innertext
                                            {                       
                                                $t->innertext .= isset($val)?$val:NULL;//no htmlspecchars needed as even if you have <html> inside textarea tag browser show <html> only it wont consider it as <html>
                                                $this->uniq_input_elements[$el[0]]['count']++;

                                            }
                                            if($ielement == 'select')
                                            {   //for select elements $_REQUEST['selectelementname'] is coming from <option> elements inside it.
                                                //So if we want to preserve which option user selected from drop down, then we need to set "selected" attribute of that option element to "selected"(.i.e. <option selected="selected>)
                                                //if <select> element name is dept and we got $_REQUEST['dept'] as 2, then we need to find <option value=2> and make it <option value=2 selected="selected">
                                                
                                                //If the we have "$_REQUEST['selectelement_name'] is set, then we know atleast one option for this select list is selected and submitted.
                                                //So we need remove "selected" from default option which is selected
                                                //Then for all values in array $_REQUEST['selectelement_name'] find the corresponding option and updated its "selected" attribute to "selected" This is for select elements which can be used for "multiple" options to be selected.
                                                //since select element is only one element, we will not get a chance to get to this element again in the for loop, so for all options selected for this select element, update all of them at once
                                                
                                                //$t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                //$selectvalue = $t->value; 
                                                
                                                //check if $_REQUEST['selectelement_name'] is set
                                                if(isset($_REQUEST[$elementname]))
                                                {
                                                        foreach($t->find('option') as $t1)
                                                        {
                                                            //$inner = $t1->innertext;
                                                            $optionvalue = $t1->value;
                                                            $optionset = false;
                                                            foreach($_REQUEST[$elementname] as $selectedvalue)
                                                            {
                                                                if($optionvalue == $selectedvalue)
                                                                {
                                                                   $optionset = true;
                                                                }
                                                            }
                                                            if($optionset)
                                                            {
                                                                $t1->selected = "selected";
                                                            }
                                                            elseif(isset($t1->attr['selected']))//nullify "selected" attribute for all elements which are not set, effectively removing default options for select list                                                   
                                                            {
                                                                unset($t1->attr['selected']);
                                                            }
                                                        }
                                                }
                                                $this->uniq_input_elements[$el[0]]['count']++;//this doesnt matter actually for select
                                            }
                                        }
                                        else  //element name is having [ but no [] means we can get POST/GET value exactly using its name
                                        {
                                            $requestname = '['.$elementname.']'.trim($arraypart);

                                            preg_match_all("/\[(.*?)\]/",$requestname,$matchs);
                                            $val = $this->get_array_keys_values($_REQUEST,$matchs[1]);
                                            if($val != NULL)
                                            $val = $val[$matchs[1]];

                                            if($ielement == 'input')
                                            {   

                                                if($t->type == 'checkbox' or $t->type == 'radio') //if it is checkbox
                                                {
                                                    //for checkbox value is always hardcoded in HTML without any submit. So we should not overwrite it, and also for checkbox if submitted value == existing value then we should set "checked" attribute to "checked"
                                                    if($t->value == $val)
                                                    {
                                                    $t->checked = "checked";
                                                    preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                    $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                    $this->uniq_input_elements[$el[0]]['count']++;;//increment count only if checkbox is actually ticked. Only ticked checkboxes are submitted unlike other input types
                                                    //for example if you have following checkboxes in html file
                                                    /*
                                                     * <li>JavaScript: <input type="checkbox" name="checkin[]" id="javascript" value="javascript" /></li>
                                                        <li>CSS: <input type="checkbox" name="checkin[]" id="css" value="css"/></li>
                                                        <li>PHP: <input type="checkbox" name="checkin[]" id="php" value="php"/></li>
                                                        <li>Ruby: <input type="checkbox" name="checkin[]" id="ruby" value="ruby"/></li>
                                                        <li>Python: <input type="checkbox" name="checkin[]" id="python" value="python"/></li>

                                                     *  in this case when user checks say javascript and php only and submits
                                                     * then submit string will be checkin[]=javascript&checkin[]=php
                                                     * so $_REQUEST will be
                                                     * $_REQUEST[0] = 'javascript' and $_REQUEST[1]='php';
                                                     * For other input types like text all of them gets submitted with empty values
                                                     * if it is a text type then submit string will be
                                                     * $_REQUEST[0]='javascript',$_REQUEST[1]='', $_REQUEST[2]='php',$_REQUEST[3]=''
                                                     * 
                                                     * So in checkbox case count should be incremented only when checkbox is submitted.
                                                     * otherwise we will endup checking wrong boxes in output after loading and displaying                                */
                                                    }
                                                }
                                                else
                                                {
                                                    $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                    preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                    $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                    $this->uniq_input_elements[$el[0]]['count']++;                             
                                                }

                                            }  
                                            if($ielement == 'textarea')//if it is textarea, textarea element is not having value attribute so have to add it to innertext
                                            {                       
                                                $t->innertext .= isset($val)?$val:NULL;//no htmlspecchars needed as even if you have <html> inside textarea tag browser show <html> only it wont consider it as <html>
                                                preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                $this->uniq_input_elements[$el[0]]['count']++;  

                                            }
                                            if($ielement == 'select')
                                            {   //for select elements $_REQUEST['selectelementname'] is coming from <option> elements inside it.
                                                //So if we want to preserve which option user selected from drop down, then we need to set "selected" attribute of that option element to "selected"(.i.e. <option selected="selected>)
                                                //if <select> element name is dept and we got $_REQUEST['dept'] as 2, then we need to find <option value=2> and make it <option value=2 selected="selected">
                                                $t->value = isset($val)?$this->safeinput->filter('htmlspecchars','VAR',$val):$this->safeinput->filter('htmlspecchars','VAR',$t->value);
                                                $selectedvalue = $t->value; 
                                                if(isset($_REQUEST[$t->name]))
                                                {
                                                        foreach($t->find('option') as $t1)
                                                        {
                                                            //$inner = $t1->innertext;
                                                            $optionvalue = $t1->value;
                                                            $optionset = false;
                                                            
                                                            
                                                            if($optionvalue == $selectedvalue)
                                                            {
                                                               $optionset = true;
                                                            }
                                                            
                                                            if($optionset)
                                                            {
                                                                $t1->selected = "selected";
                                                            }
                                                            elseif(isset($t1->attr['selected']))//nullify "selected" attribute for all elements which are not set, effectively removing default options for select list                                                   
                                                            {
                                                                unset($t1->attr['selected']);
                                                            }
                                                        }
                                                }
                                                preg_match("/\[([0-9])\]/",$arraypart,$mat); //get first numeric braces and extract numeric value and assign it to mat[0]
                                                $this->uniq_input_elements[$el[0]]['count'] = $mat[1];
                                                $this->uniq_input_elements[$el[0]]['count']++;  
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }   
            }
            
            /*********************Load Validation Criteria**********************************/
            $this->php_plugin_loader();//Load all plugins as per currently provided external plugins/if not, then default plugins
            $this->load_validation_json();               
        }
    }
    /**
     * Loads validation criteria dependingon one of the parameters passed
     * @param json|array $validation_array_or_jsonstring Either a json string or validation array(json already converted as array)
     * Depending on array or json string or if nothing is passed, by default checks if #validatecrit div element is present in HTML, and it has a comment, it will load the contents of it, if it is as valid json. If not Throws excpetion.
     * @return boolean Returns true if valid json or valid validation array is found, false if no json at all, Exception if json is set, but invalid json
     * @throws {if json is set but if it is invalid json format}
     */
    public function load_validation_json($validation_array_or_jsonstring = null)
    {
        if(isset($validation_array_or_jsonstring))
        {
            if(is_array($validation_array_or_jsonstring))
            {
                //if supplied validation criteria is array,then convert it into JSON and check if no errors.then convert it back into array again
                $this->validation_json = json_encode($validation_array_or_jsonstring);     
            }
            else
            {
                $this->validation_json = $validation_array_or_jsonstring;
            }
        }
        else
        {   //check if element with id #validatecrit is presnet in HTML , if yes then load the same; else do nothing
            $this->validation_json = $this->get_validation_criteria('validatecrit'); 
        }
        
        if(isset($this->validation_json))
        {
            $this->validation_json_array = json_decode($this->validation_json, true);     
            //only if valid json
            if(json_last_error() == JSON_ERROR_NONE and isset($this->validation_json_array) and is_array($this->validation_json_array))
            {
                //rewrite any rewritable conditions before using them
                $this->rewrite_conditions();
                $this->is_json_valid = 1;//valid json
                $this->formevents_validation_array = array();
                
                //load only onsubmit selector/condition criteria
                foreach($this->validation_json_array as $formid => $val )
                {
                    $selector_array = $this->validation_json_array[$formid];
                    foreach($selector_array as $selector => $condition_array)
                    {
                       foreach($condition_array as $condition => $cond_array)
                       {
                            if(isset($cond_array['events']))
                            {
                                $selector_events = explode(',',trim($cond_array['events']));
                                if(isset($selector_events))
                                {
                                    foreach($selector_events as $sevent => $vale)
                                    {   //remove any uneccessary spaces
                                        $selector_events[$sevent] = trim($selector_events[$sevent]);
                                    }
                                }
                            }
                            else
                            {
                                //No events specifies for a condition, so Use default events as onchange and onsubmit
                                $selector_events = array('onchange','onsubmit');
                            }
                            foreach($selector_events as $event)
                            {
                                if($event == 'onsubmit')
                                {
                                    $this->formevents_validation_array[$formid][$selector][$condition] = $cond_array;
                                }
                            }
                       }
                    }                                                   
                }
                if(isset($this->currentform))//only if a form is submitted, then load element specific events for elements in that form/used in validate_element for "required" condition
                $this->prepare_element_event_validation_array();//generate event specific validation array
                
                return true;
            }
            else//validation_json is set but invalid
            {
                $this->is_json_valid = 0;
                throw new \Exception('Invalid JSON specified for validation criteria - JSON validation failed with :'.$this->json_last_error_msg().' when loading HTML:'.$formfilepath);
            }
        }
        else//no json at all
        $this->is_json_valid = NULL;
        return false;           
    }
    
    /**
     * Rewrite conditions based on rewrite plugin if provided ,else using standard rewrite plugin($this->php_plugins_default['REWRITE_ENGINE'])
     * Modifies $this->validation_json_array accordingly
     * @return void
     */
   
    public function rewrite_conditions()
    {
        foreach($this->validation_json_array as $formid => $selector_array )
        {
            foreach($selector_array as $selector => $condition_array)
            {
               foreach($condition_array as $condition => $cond_array)
               {
                   $rewritten_cond = null;
                   if($condition !== 'required')
                   {
                       $cond_to_pass = array();
                       $cond_to_pass[$condition] = $cond_array;
                       
                       if($this->php_plugins['REWRITE_ENGINE']['obj']->is_condition_supported($condition))
                       {
                            $rewritten_cond = $this->php_plugins['REWRITE_ENGINE']['obj']->rewrite_cond($cond_to_pass);
                       }
                       elseif($this->php_plugins_default['REWRITE_ENGINE']->is_condition_supported($condition))
                       {
                            $rewritten_cond = $this->php_plugins_default['REWRITE_ENGINE']->rewrite_cond($cond_to_pass);
                       }
                       
                       if($rewritten_cond != null)
                       {
                           
                           $diff_array = strcmp(json_encode($rewritten_cond),json_encode($cond_to_pass));
                           if(count($diff_array) != 0)
                           {
                               //then condition is rewritten so remove existing condition and add the new one to the array
                               unset($this->validation_json_array[$formid][$selector][$condition]);
                               foreach($rewritten_cond as $rew_cond => $rew_cond_array)
                               {
                                   $this->validation_json_array[$formid][$selector][$rew_cond] = $rew_cond_array;
                               }

                           }
                       }
                   }
                   else
                   {
                       //if condition is required then we need to go through each elementid in required and foreach condition , rewrite it
                       if(isset($cond_array['arg']))
                       {
                            foreach($cond_array['arg'] as $dep_elem => $dep_elem_array)
                            {
                                //valid,checked,selected are not supported to be rewritten due many other dep issues and cyclic dependencies it can break
                                if(is_array($dep_elem_array))//valid,checked,selected are the only ones with string format, so not processing them
                                {
                                    foreach($dep_elem_array as $dep_cond => $dep_cond_array)
                                    {
                                        if($dep_cond !== 'valid' and $dep_cond !== 'checked' and $dep_cond !== 'selected')
                                       {
                                            $cond_to_pass = array();
                                            $cond_to_pass[$dep_cond] = $dep_cond_array;
                                            if($this->php_plugins['REWRITE_ENGINE']['obj']->is_condition_supported($condition))
                                            {
                                                $rewritten_cond = $this->php_plugins['REWRITE_ENGINE']['obj']->rewrite_cond($cond_to_pass);
                                            }
                                            elseif($this->php_plugins_default['REWRITE_ENGINE']->is_condition_supported($condition))
                                            {
                                                $rewritten_cond = $this->php_plugins_default['REWRITE_ENGINE']->rewrite_cond($cond_to_pass);
                                            }
                                           if($rewritten_cond != null)
                                           {
                                               //$diff_array = array_diff_key($rewritten_cond,$cond_to_pass);
                                               $diff_array = strcmp(json_encode($rewritten_cond),json_encode($cond_to_pass));
                                               if(count($diff_array) != 0)
                                               {
                                                   //then condition is rewritten so remove existing condition and add the new one to the array
                                                   unset($this->validation_json_array[$formid][$selector][$condition]['arg'][$dep_elem][$dep_cond]);
                                                   foreach($rewritten_cond as $rew_cond => $rew_cond_array)
                                                   {
                                                       $this->validation_json_array[$formid][$selector][$condition]['arg'][$dep_elem][$rew_cond] = $rew_cond_array;
                                                   }
                                               }
                                           }
                                       } 
                                    } 
                                }
                            }
                        }
                    }
                }
            }
        }       
    }
    
    /**
     * Takes a array and uses json_encode and returns a json
     * @param array $array
     * @return json
     * 
     */
    public function array_to_json($array)
    {
        if(is_array($array))
        {
            return json_encode($array);
        }
    }
    /**
     * Takes json and covnerts it into array, if invalid jsonm then throws exception
     * @param json $json
     * @return array
     * @throws Exception if provided $json is not a valid json
     */
    public function json_to_array($json)
    {
        if(isset($json))
        {
            $array = json_decode($json, true);
            if(json_last_error() == JSON_ERROR_NONE)
            {
                return $array;               
            }
            else
            {
                $this->is_json_valid = 0;
                throw new \Exception('Invalid JSON specified for validation criteria - JSON validation failed with :'.$this->json_last_error_msg().' when loading HTML:'.$formfilepath);
                die(); 
            }           
        }       
    }
    /**
     * Finds if each of $keys is existing on $array keys, if yes stores its value in $array1, if not assign NULL for that key and returns $array1
     * @param array $array
     * @param array $keys
     * @return array|NULL returns array of keys and its values if found in $array, if not NULL for those keys values, Returns NULL only if $keys is not passed or NULL
     */
    public function get_array_keys_values($array,$keys)
    {
        $key = array_shift($keys);
        while(isset($key))
        {
            $array = isset($array[$key])?$array[$key]:NULL;
            $key = array_shift($keys);
        }
        return isset($array)?$array:NULL;
    }
    
    /**
     * Main Validation function.
     * Validates submitted form elements, using validation_json_array conditions and populates $this->errormessage_stack with elementid as index and that elements error messages
     * @see $errormessage_stack
     * @return int Zero if validation failed, 1 if validation is successful.
     */
    public function validate()
    {
        $errormessage_stack = array();
        $this->validate_error_count=0;
        if(isset($this->formevents_validation_array) and isset($this->currentform) and $this->is_json_valid)
        {
            foreach($this->formevents_validation_array as $formid => $form_validation_array)
            {
                if($formid == $this->currentform->id)
                {
                    foreach($form_validation_array as $selector => $cond_array)
                    {
                        $elements = $this->currentform->find($selector);
                        
                        foreach($elements as $domelement)
                        {
                            foreach($cond_array as $op => $cond)
                            {
                                $eachcond['op'] = $op;
                                $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                                $err = isset($cond['error'])?$cond['error']:NULL;           
                                
                                $result = $this->validate_element($domelement,$eachcond,$err);
                                
                                $result_status = $result['status'];
                                if(isset($result['errormessage']))
                                $errormessage = $result['errormessage'];
                                else
                                $errormessage = null;
                                
                                //if($op != 'required')//for required add and remove error are manged inside validate_element itself
                                //{
                                if(!$result_status)
                                {   $this->validate_error_count++;
                                    if(isset($errormessage))
                                    {
                                        if($errormessage != '')
                                        $this->add_error($domelement->id,$errormessage);
                                    }
                                }
                                else
                                {
                                    if(isset($errormessage))
                                    {
                                        if($errormessage != '')
                                        $this->remove_error($domelement->id,$errormessage);
                                    }
                                }
                                if(array_key_exists('errormessages_to_add',$result))
                                {
                                    if(is_array($result['errormessages_to_add']))
                                    {
                                        foreach($result['errormessages_to_add'] as $errmessage)
                                        {
                                            $this->add_error($domelement->id,$errmessage);
                                        }
                                    }
                                }
                                if(array_key_exists('errormessages_to_remove',$result))
                                {
                                    if(is_array($result['errormessages_to_remove']))
                                    {
                                        foreach($result['errormessages_to_remove'] as $errmessage)
                                        {
                                            $this->remove_error($domelement->id,$errmessage);
                                        }
                                    }
                                }                              
                            }                          
                        }
                    }
                    //$this->display_errors();removing it here and added this in $this->display();                   
                }
            }
            foreach($this->elements_that_depend_onme_for_valid_cond as $elementid => $deparray)
            {
                $this->refresh_required_valid_dependencies($elementid,null);
            }
            $this->error_count = $this->manual_error_count + $this->validate_error_count;
            if($this->validate_error_count)
            return 0;//validation failed
            else
            return 1;
        }
        else
        {
            if(is_null($this->is_json_valid))//there is no validatecrit at all
            return 1;//nothing to validate so returns 1;
            else
            return 0;//means $this->is_json_valid is 0 and it is due to invalid json
        }
    }
    /**
     * For each element, there may be elements which are dependent through required->valid condition
     * For exmaple element B may be having its validation criteria as "required":{"elementA":"valid|B is valid only if A is valid"}
     * In this case if status of A changes(when error is added or removed for A), need to refresh B's valid condition on A, so that B can refresh its status too"
     * @param string $elementid elementid of A
     * @param array $refresh_source_array Array of element ids which are dependent on A for "required"->"valid" condition(like B in above example)
     * @param int $rec_count to track recursive depth at any point during refresh, to prevent infinite loops
     * @return void
     */
    public function refresh_required_valid_dependencies($elementid,$refresh_source_array,$rec_count = 0)
    {
        if(!isset($refresh_source_array[$elementid]))//prevent infinite loop
        {
            $refresh_source_array[$elementid] = 1;//to track source in the refresh chain so that we dont end up refreshing the same element due to cyclic dependencies

            if(isset($this->elements_that_depend_onme_for_valid_cond[$elementid]))
            {
                foreach($this->elements_that_depend_onme_for_valid_cond[$elementid] as $dependentid => $errormessage )
                {
                   $dependent = $this->currentform->find('#'.$dependentid);
                   foreach($dependent as $e)
                   {
                       $dependent = $e;
                   }
                    $element_cond = array();
                    $element_cond['op'] = 'required';
                    $element_cond['arg'][$elementid] = 'valid|'.$errormessage;
                    $error_before_refresh =  $this->get_error_text_of_element($dependentid);
                    
                    $this->validate_element($dependent,$element_cond,null);

                    $error_after_refresh = $this->get_error_text_of_element($dependentid);           

                    if($error_before_refresh !== $error_after_refresh)//dependent is effected by change on source element
                    {
                         $rec_count++;//to track recursion depth;
                         $this->refresh_required_valid_dependencies($dependentid,$refresh_source_array,$rec_count);//recursion - this function recursively
                         $rec_count--;
                    }
                }
            }
            unset($refresh_source_array[$elementid]);
        }
    }
    /**
     * 
     * @param string $elementid element id
     * @return string Array of errors for element with id as $elementid, imploded using "||"
     */
    public function get_error_text_of_element($elementid)
    {
        if(isset($this->errormessage_stack[$elementid]))
        {
            if(count($this->errormessage_stack[$elementid]) > 0)
            {
                
                if(count($this->errormessage_stack[$elementid]) > 1 )//implode with || only if atleast two errors are there
                $errorhtml = implode("||",$this->errormessage_stack[$elementid]);
                else if(count($this->errormessage_stack[$elementid]) == 1)
                $errorhtml = $this->errormessage_stack[$elementid][0];

                return $errorhtml;
            }
            else
            return null;
        }
        else
        return null;            
    }
    /**
     * Add errormessage to $elementid .i.e add errormessage to $this->errormessage_stack[$elementid]
     * @param string $elementid
     * @param string $errormessage
     * @return void
     */
    
    public function add_error($elementid,$errormessage)
    {
        if(isset($this->errormessage_stack[$elementid]))
        {
            if(!in_array(trim($errormessage),$this->errormessage_stack[$elementid]))
            $this->errormessage_stack[$elementid][sizeof($this->errormessage_stack[$elementid])] = $errormessage;
        }
        else
        {
        $this->errormessage_stack[$elementid][0] = $errormessage;
        
        }
    }
    /**
     * Remove errormessage for $elementid .i.e Remove errormessage from $this->errormessage_stack[$elementid]
     * @param string $elementid
     * @param string $errormessage
     * @return void
     */
    
    public function remove_error($elementid,$errormessage)
    {
        if(isset($this->errormessage_stack[$elementid]))
        {
            if(false !== $key = array_search(trim($errormessage), $this->errormessage_stack[$elementid]))
            unset($this->errormessage_stack[$elementid][$key]);
        }
        
    }
    /*public function display_errors()
    {
        if(!$this->js_validation)///IF JS validation is not there, then add errors the normal way,but if js_validation is true and if use cookie is not true for js then we send errors to js as a parameter, so we dont use below method
        {
            if(is_array($this->errormessage_stack))
            {
                foreach($this->errormessage_stack as $elementid => $errormessage_array)
                {
                    $ele = $this->domobj->find('#'.$elementid);
                    foreach($ele as $e)//find always returns a array even if find with id;
                    foreach($errormessage_array as $i => $errormessage)
                    {
                        $this->add_error_message($e,$errormessage);
                    }
                }
            }
        }
        /*else//we just set the error messages as JSON in cookie and let Javascript display them, based on whatever display plugin being used
        {
           if(is_array($this->errormessage_stack))
           $this->set_json_cookie('element_error_array_json',$this->errormessage_stack);
           else
           {
               //unset if a cookie is existing, otherwise JS will endup using old errormessages
               if (isset($_COOKIE['element_error_array_json'])) 
               {
                unset($_COOKIE['element_error_array_json']);
                setcookie('element_error_array_json','', time() - 3600);
               }
           }
        }
    }*/
    /**
     * Main validator for a specific element, Uses Plugin architecture
     * If user provided any "VALIDATOR" plugin, then uses it to validate a specific condition for a element, if condition is supported by that VALIDATOR
     * If not uses default "VALIDATION" plugin to validate the condition
     * @param simple_html_dom_node $ielement element object to be validated
     * @param string $cond Condition to be validated for $ielement
     * @param string $error errormessage to be appended to $ielement, if validation fails, or to be removed from $ielement if validation succeeds
     * @return array $result Return a $result array with below keys
     * $result['status'] , Either true or false, depending on validation status of the $cond for $ielement
     * $result['errormessage'] Returns errormessage to be appended or removed from $ielement, depending on $result['status']
     * errormessage will be same as $error, if $error is not NULL, if NULL, then default errormessage may be returned by "VALIDATOR"
     */
    public function validate_element($ielement,$cond,$error = null)
    {
       if($this->php_plugins['VALIDATOR']['obj']->is_condition_supported($cond['op']))
       {
            return $this->php_plugins['VALIDATOR']['obj']->validate_element($ielement,$cond,$error,$this);
       }
       else
       {//if default validator doesnt support a condition, it just returns true for validation
           return $this->php_plugins_default['VALIDATOR']->validate_element($ielement,$cond,$error,$this);
       }
    }
    
    /**
     * Doesnt validate the element, just checks if element is having any errors in $this->errormessage_stack, if yes, returns false, if no errors returns true
     * @param simple_html_dom_node $element
     * @return boolean Just checks if element is having any errors in $this->errormessage_stack, if yes, returns false, if no errors returns true
     */
    
    public function is_element_valid($element)
    {
        
        if(isset($this->errormessage_stack[$element->id]))
        {
            if(count($this->errormessage_stack[$element->id]) > 0)
            {
                return false;
            }
            else
            return true;
        }
        else
        return true;
    }
    
    /**
     * Prepares following arrays
     * $this->element_event_validation_array From Selector validation array, finds all elements from the selector and prepares element specific validation array
     * $this->elements_that_I_depend_on
     * $this->elements_that_I_depend_for_valid_cond
     * $this->elements_that_depend_onme_for_valid_cond
     * 
     * @see $element_event_validation_array
     * @see $elements_that_I_depend_on
     * @see $elements_that_I_depend_for_valid_cond
     * @see $elements_that_depend_onme_for_valid_cond
     */
    public function prepare_element_event_validation_array()
    {
        $errormessage_stack = array();
        $this->validate_error_count=0;
        $this->element_event_validation_array = array();
        $this->elements_that_I_depend_for_valid_cond = array();
        $this->elements_that_depend_onme_for_valid_cond = array();
        if(isset($this->formevents_validation_array) and $this->is_json_valid)
        {
            foreach($this->formevents_validation_array as $formid => $form_validation_array)
            {
                foreach($form_validation_array as $selector => $cond_array)
                {
                    $elements = $this->currentform->find($selector);

                    foreach($elements as $domelement)
                    {
                        foreach($cond_array as $op => $cond)
                        {
                            $eachcond['op'] = $op;
                            $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                            $err = isset($cond['error'])?$cond['error']:'error';           

                            $this->element_event_validation_array[$formid][$domelement->id][$op] = $cond;
                            if($op == 'required')
                            {
                                
                                $this->elements_that_I_depend_on[$domelement->id] = $eachcond['arg'];
                                foreach($eachcond['arg'] as $req_elemid => $req_cond)
                                {
                                    if(!is_array($req_cond))
                                    {
                                        $req_arg =  explode('|',$req_cond);//arg format "elementid": "valid|errormessage if not valid"
                                        $req_arg_cond = trim($req_arg[0]);
                                        $req_errormessage = isset($req_arg[1])?$req_arg[1]:null;
                                        if($req_arg_cond == 'valid')
                                        {
                                           $this->elements_that_I_depend_for_valid_cond[$domelement->id][$req_elemid] = $req_errormessage;
                                           $this->elements_that_depend_onme_for_valid_cond[$req_elemid][$domelement->id] = $req_errormessage;
                                        }
                                    }
                                    else
                                    {
                                        $req_arg = $req_cond;
                                        foreach($req_arg as $cond => $cond_arg)
                                        {
                                            if($cond == 'valid')
                                            {
                                                $req_errormessage = isset($cond_arg['error'])?$cond_arg['error']:null;
                                                $this->elements_that_I_depend_for_valid_cond[$domelement->id][$req_elemid] = $req_errormessage;
                                                $this->elements_that_depend_onme_for_valid_cond[$req_elemid][$domelement->id] = $req_errormessage;
                                        
                                            }
                                        }
                                    }
                                }
                            }  
                        }
                    }
                }   
            }
        }
    }
    
    /**
     * Main Display function , Prepares all required stuff for Javascript validation if enabled
     * Also removes #validatecrit div element from HTML to be displayed
     * echo entire HTML
     * @see prepare_js_validation_scripts()
     */
    
    public function display()
    {
        if(!$this->js_validation)///IF JS validation is not there, then add errors to elements using ERROR_HANDLER php plugin provided by user, or using default ERROR_HANDLER php plugin
        {
            $this->php_plugins['ERROR_HANDLER']['obj']->display_errors($this->errormessage_stack,$this->domobj);
        }
        if($this->js_validation)//prepare js validation scripts and add all required scripts, plugins, validation criteria cookie etc
        {
            if($this->yui_profiler)
            $this->prepare_js_validation_scripts_with_yui_profiler ();
            else
            $this->prepare_js_validation_scripts();
            
        }
        foreach($this->domobj->find('#validatecrit') as $t)
        {
            //with or without javascript, we are not going to send validatecrit comment to client
            //if using javascript, we set cookie with validation json data, so that javascript can use it
            //if($this->js_validation != 'TRUE')
            //{
            $t->outertext = '';
        }      
        echo $this->domobj;
    }
    
    /**
     * If user wants to add his own version of jquery script, use this function
     * @param string  $jquery_path Path of jquery script that user wants to add
     */
    public function add_jquery_script($jquery_path)
    {
        
        if(isset($jquery_path))
        $this->js_jquery_script = '<script type="text/javascript" src="'.$jquery_path.'"></script>';       
    }
    
    /**
     * Main function to prepare all scripts/paths includes for Javascript validation
     * Order of scripts added are as follows:
     * 1.jQuery and Cookie plugin for jquery
     * 2.All scripts in $this->js_include
     * 3.All Plugin scripts in $this->js_plugin_script
     * 4.Actual latest_validation_js.js script
     * 5.prepare all js parameters and then if js_use_cookie is true, then create cookie from paraemters json array
     * else add a <script> with SSKREPO_FORM_JS.params = parameters_json;          
     * 
     * If 
     * It is proper HTML, having <head> element, then add it to all of them to <head>
     * else
     * Add it next to some random form(Bad HTML , cant help, have to add somewhere)
     * 
     * @return boolean true if successfully prepared all js script, else false
     */
    
    public function prepare_js_validation_scripts()
    {
        if($this->is_json_valid and $this->js_validation)      //only if validation json is valid and js validation is set to true
        {
           
            $headelement = $this->domobj->find('head');
            
            //Add jQuery script
            if(isset($this->js_jsquery_script))
            {
                $this->js_final_include_string = $this->js_jsquery_script;
            }
            else
            {
               //add our own jquery script
               $this->js_final_include_string = '<script src="https://code.jquery.com/jquery-1.11.3.js"></script>';
            }
            //Add cookie plugin for jQuery
            $this->js_final_include_string .= '<script src="https://newlogin.letterbox.in/tests/js.cookie.js"></script>';
            
            //Add All scripts in $this->js_include added using $this->add_js_script();
            
            $this->js_final_include_string .= $this->js_include;
            
            //Add All Plugin scripts in $this->js_plugin_script
            if(isset($this->js_plugin_scripts))
            $this->js_final_include_string .= $this->js_plugin_scripts;
            
            //Add our latest_validation_js script
            
            $this->js_final_include_string .= $this->latest_validation_js;
           
            
            $this->js_prepare_parameters();
            if($this->js_use_cookie)
            {  //set validation criteria cookie
               $this->setcookie('sskrepo_js_param',$this->array_to_json($this->js_parameter_array)); 
            }
            else
            {
               //unset parameter cookie;
               if(isset($_COOKIE['sskrepo_js_param'])) 
               {
                unset($_COOKIE['sskrepo_js_param']);
                setcookie('sskrepo_js_param','', time() - 3600);
               }
               //pass parameter string to SSKREPO_FORM_JS
               //SSKREPO_FORM_JS.params_json = $this->array_to_json($this->js_parameter_array);
               $this->js_final_include_string .= '<script>SSKREPO_FORM_JS.params_json = '.$this->array_to_json($this->js_parameter_array).';</script>'; 
            }
            //Add onready event to execute MAIN events_validator
            $this->js_final_include_string .= "<script>$(document).ready(SSKREPO_FORM_JS.events_validator);</script>";
      
            if(!empty($headelement))
            {
                foreach($headelement as $head)
                {
                     $head->innertext = $head->innertext.$this->js_final_include_string;
                }
            }
            else //Not proper HTML, as it is without <head> tag So we just need to add js include somewhere
            {
                $forms = $this->domobj->find('form');
                foreach($forms as $someform)
                {}//just to get some random form in the HTML , so that we can append <script> code next to it only if <head> is not available
                $someform->outertext = $someform->outertext.$this->js_final_include_string;//just adding js include next to last accessed Form element
            }
            return true;
        }
        else
        return false;       
    }
    /**
     * 
     * This is from standard simple_html_dom package
     * Website: https://sourceforge.net/projects/simplehtmldom/
     * Acknowledge: Jose Solorzano (https://sourceforge.net/projects/php-html/)
     */
    public function file_get_html($url, $use_include_path = false, $context=null, $offset = -1, $maxLen=-1, $lowercase = true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT, $defaultSpanText=DEFAULT_SPAN_TEXT)
    {
        
	// We DO force the tags to be terminated.
	$dom = new \sskrepo\sklib\form\simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $stripRN, $defaultBRText, $defaultSpanText);
	// For sourceforge users: uncomment the next line and comment the retreive_url_contents line 2 lines down if it is not already done.
	$contents = file_get_contents($url, $use_include_path, $context, $offset);
	// Paperg - use our own mechanism for getting the contents as we want to control the timeout.
	//$contents = retrieve_url_contents($url);
	if (empty($contents) || strlen($contents) > MAX_FILE_SIZE)
	{
		return false;
	}
	// The second parameter can force the selectors to all be lowercase.
	$dom->load($contents, $lowercase, $stripRN);
	return $dom;
    }

    /**
     * Get html dom from string
     * This is from standard simple_html_dom package
     * Website: https://sourceforge.net/projects/simplehtmldom/
     * Acknowledge: Jose Solorzano (https://sourceforge.net/projects/php-html/)
     */
    
    public function str_get_html($str, $lowercase=true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT, $defaultSpanText=DEFAULT_SPAN_TEXT)
    {
            $dom = new \sskrepo\sklib\form\simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $stripRN, $defaultBRText, $defaultSpanText);
            if (empty($str) || strlen($str) > MAX_FILE_SIZE)
            {
                    $dom->clear();
                    return false;
            }
            $dom->load($str, $lowercase, $stripRN);
            return $dom;
    }
    
    /**
     * dump html dom tree
     * This is from standard simple_html_dom package
     * Website: https://sourceforge.net/projects/simplehtmldom/
     * Acknowledge: Jose Solorzano (https://sourceforge.net/projects/php-html/)
     */

    
    public function dump_html_tree($node, $show_attr=true, $deep=0)
    {
            $node->dump($node);
    }
    
    /**
     * Executes PHP Sript at $path , replaced any dynamic parameters with $view and return PHP script output
     * Used by LoadForm to preprocess the HTML form for any replacable PHP variable before loading it with $_REQUEST data
     * @param string $path Path of PHP/HTML script 
     * @param array $view $view array to replace any dynamic variables in HTML/PHP script
     * @return string|HTML PHP script output
     */
    public function getscriptoutput($path, $view)
    { 
        //phpscriptoutput_to_variable
        ob_start();
        if( is_readable($path) && $path )
        {
            include $path;
        }
        else
        {
            return FALSE;
        }
        return ob_get_clean();
    }
    
    
    public function add_element($parentelement,$elementcode)
    {
        //add $elementcode to $parentelement
    }
    
    /**
     * Extracts Validation JSON from HTML div element #validatecrit if present, else returns NULL
     * @param string $validate_div_element_id iv id of validatecrit div element, it can be #validatecrit by default, or anything else passed by user
     * @return Validation JSON string from HTML div element #validatecrit if present, else returns NULL
     */
    public function get_validation_criteria($validate_div_element_id)
    {
        //takes validate_div_element and extracts validation json from comment section and returns the same
        if(isset($validate_div_element_id))
        {
            $validate_div_element = $this->domobj->find('#'.$validate_div_element_id);
            foreach($validate_div_element as $vc)
            {
                $comm = $this->str_get_html($vc);
                $comm1 = $comm->find('comment');
            }
            if(isset($comm1))
            {
                //comment tags need to be stripped out and also they may have white space around so trim it
                foreach ($comm1 as $comm2)
                {
                    $comm2 = str_replace('<!--','',$comm2->innertext);
                    $comm2 = str_replace('-->','',$comm2);
                    $comm2 = trim($comm2);
                    //comment is in the form of <form element name>:<validation condition>:<error message incase validation fails>;
                    //each line will have validation condition for each element. same element may have multiple validations too in multiple lines.

                }
                return $comm2;
            }
            else
            return null;
        }
        else
        return null;
    }
    
    /**
     * Sets below things
     * $this->js_use_cookie , sets it to true for $cookie_use is true, false if $cookie_use is false
     * $this->js_validation to true
     * $this->latest_validation_js sets this to complete <script> syntax using $form_js_path
     * @param string $form_js_path Path for main Javascript validation script latest_validation_js
     * @param boolean $cookie_use true if cookie use is fine, else false to prohibit cookie use
     * @return boolean return true always
     */
    public function add_js_validation($form_js_path,$cookie_use=true)//by default use cookies, only if user pass $cookie_use as false, then disable cookies
    {
        if(!$cookie_use)
        $this->js_use_cookie = false;
        else
        $this->js_use_cookie = true;

        $this->js_validation = true; //USed by display(), so that it wont remove validatecrit from HTML
        $this->latest_validation_js = '<script type="text/javascript" src="'.$form_js_path.'"></script>';
        return true;
    }
    /**
     * Add a js script tag with a specific JS script provided using $script
     * @param string $script script path for JS script
     */
    
    public function add_js_script($script)
    {
        if(isset($script))
        $this->js_include .= '<script type="text/javascript" src="'.$script.'"></script>';       
    }
    /**
     * Adds Javascript Plugin array to $this->js_plugins_array, and <script> code for all scripts provided in $script_array in the same order as mentioned in that array
     * @param type $plugins_array Array of PLUGIN types that need to be added as JS plugins
     * @param type $script_array Array of Script that supports the JS plugin types being added, in the same order they need to be added as <script>s to HTML <head> based on dependencies on each other
     */
    public function add_js_plugin($plugins_array,$script_array)
    {
        if(isset($plugins_array) and isset($script_array))
        {
            $this->js_plugins_array = $plugins_array;
            if(!is_array($script_array))
            {
                $this->js_plugin_scripts .= '<script type="text/javascript" src="'.$script.'"></script>';
                //$this->js_plugin_script .= '<script>'.'$.extend(SSKREPO_FORM_JS,'.$pluginname.');</script>';
            }
            else
            {
                foreach($script_array as $i => $script)
                {
                   $this->js_plugin_scripts .= '<script type="text/javascript" src="'.$script.'"></script>'; 
                }
            }           
        }        
    }
    
    /**
     * Add PHP plugin to $this->php_plugins_array, PHP plugin type as key and $php_plugin_object as value and repload plugins so that new plugins are loaded as well
     * @param type $php_plugin_type PHP plugin TYPE to be added
     * @param type $php_plugin_object PHP plugin Object for plugin type being added
     */
    public function add_php_plugin($php_plugin_type,$php_plugin_object)
    {
        if(isset($php_plugin_object) and $php_plugin_type)
        {
            $this->php_plugins_array[$php_plugin_type] = $php_plugin_object;
            $this->php_plugin_loader();//reload plugins with newly added plugins as well;
        }
    }
    
    /**
     * Find all forms in $this->domobj as mentioned in $form_name_array
     * @param ALL|string|array $form_name_array $form_name_array can be 'all' or a array of form names or a single form name
     * @return array Returns array of form elements of type simple_html_dom_node
     */
    public function find_forms($form_name_array)
    {
        
        if(is_array($form_name_array))
        {
            $i = 0;
            $forms = $this->domobj->find('form');
            foreach($forms as $form)
            {
                if(in_array($form->name,$form_name_array))
                {
                    //add event to form;

                    $form_elements[$i] = $form;
                    $i++;
                }
            }
        }
        elseif($form_name_array == 'all')
        {
            $form_elements = $this->domobj->find('form');

        }
        elseif(!empty($form_name_array))
        {
            $form_elements = $this->domobj->find('form');
            foreach($forms as $form)
            {
                if($form->name == $form_name_array)
                {
                    $form_elements[$i] = $form;
                }
            }
        }
        else
        {
            return 0;//no forms were provided to set the event
        }
        return $form_elements;
    }
    
    /**
     * adds error label to elements with given name
     * increments $this->manual_error_count 
     * @param string $element name or id of a element, depends on element_type
     * @param string $errormessage errormessage to add to $element
     * @param string $element_type $element_type can be 'name' or 'id', referes to if $element value to be considered as element name or element id
     * @return int Returns $this->manual_error_count
     */
    
    public function add_error_to_element($element,$errormessage,$element_type='name')//$type defaults to name , it can "id" as well
    {
        //adds error label to elements with given name
        //increments $this->manual_error_count 
        //$element_type can be 'name' or 'id', referes to if $element value to be considered as element name or element id
        if($element_type == 'name' or $element_type == 'id')
        {
            if($element_type == 'name')
            $el_search_string = '[name='.$element.']';
            elseif($element_type == 'id')
            $el_search_string = '#'.$element;

            $domelement = $this->domobj->find($el_search_string);
            foreach($domelement as $el)
            {
                $this->add_error($el->id,$errormessage);
                $this->manual_error_count++;
            }
        }
        elseif($element_type = 'element')
        {
            $this->add_error($element->id,$errormessage);
            $this->manual_error_count++;
        }        
        return $this->manual_error_count;        
    }
    
    /**
     * Add errormessage to as a label to $domelement
     * @param simple_html_dom_node $domelement Dom element
     * @param array $errormessage_stack error message array , indexed with elementids
     */
    public function add_error_message($domelement,$errormessage_stack)
    {
        $domelement->style = "border:solid 1px red";
        $domelement->outertext .= "<label id='formerror--label' for='".$domelement->name."'><font color='red' size=2> || ".$errormessage_stack."</font></label>";        
    }
    
    /**
     * When a form is submitted, in a multiple form HTMLs we need to be able to uniquely identify which form is submitted
     * So we add a input element hidden for each form, with name="from_form-id-1824" and value as $form->id.'form-id-1824'
     * Based on this we find which form is submitted and do our data loading and validation accordingly for that form
     * 
     */
    
    public function add_from_form_id()
    {
        $forms = $this->domobj->find('form');
        foreach($forms as $form)
        {
            $inputel = '<input name="from_form-id-1824" id="from_form-id-1824" type="hidden" value="'.$form->id.'form-id-1824"/>';
            //Using $form->innertext will cause problems. when displaying form if innertext is modified once, it will take only inntertext 
            //for that element, for exmaple if we do $form->innertext = $form->innertext.$inputel, it will set INNER text modified flag in
            //form node, and when displaying form , if there is innertext set, it will not look for individual child node outertext.
            //so if we have modified any input element to have error label , that will not be considered as innertext of parent form.
            //so never use innertext if you plan to modify its child nodes.
            //$form->innertext = $form->innertext.$inputel;
            
            $i=0;
            foreach($form->nodes as $e) //find any one element inside form and add out input element next to it
            {
                if($i < 1)
                {
                    $e->outertext = $e->outertext.$inputel;
                    $i++;
                }
                else
                break;
            }            
        }
    }
    
    /**
     * Set a cookie with name $cookie_name and content as JSON string (json_encode of $json_array)
     * @param type $cookie_name Cookie name
     * @param type $json_array json_array to be converted to json_string using json_encode and set as content of Cookie
     */
    
    public function set_json_cookie($cookie_name,$json_array)
    {
        //cookie to set validatecrit json so that we dont have to send validatecrit comment to client, javascript can use the cookie to get validatecrit
        $json_string = json_encode($json_array);
        setcookie($cookie_name,$json_string);
        //echo $json_string;       
    }
    
    /**
     * Checks if $this->currentform is set, so that we know a form is submitted.
     * @return int|string form id if currentform is set,else 0 
     */
    public function is_form_submitted()
    {
        if(isset($this->currentform))
        {
            return $this->currentform->id;
        }
        else
        return 0;
    }
    /**
     * json_last_error_msg available only from PHP 5.5. or later so this will help irrespective of PHP version
     * 
     * @return string Error type of last json_decode/encode is having any errors, else null
     */
    public function json_last_error_msg() 
    {//
        static $errors = array(
            JSON_ERROR_NONE             => null,
            JSON_ERROR_DEPTH            => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH   => 'Underflow or the modes mismatch',
            JSON_ERROR_CTRL_CHAR        => 'Unexpected control character found',
            JSON_ERROR_SYNTAX           => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8             => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );
        $error = json_last_error();
        return array_key_exists($error, $errors) ? $errors[$error] : "Unknown error ({$error})";
    }
    
    /**
     * Just add JS parameter to $this->js_parameter_array
     * @param string $key
     * @param string $value
     */
    
    public function add_js_parameter($key,$value)
    {
        if(isset($key) and isset($value))
        {
            $this->js_parameter_array[$key] = $value;
        }
    }
    
    /**
     * Add JS parameters to be sent to Client for JS validation
     */
    public function js_prepare_parameters()
    {
        if(is_array($this->errormessage_stack))
        $this->add_js_parameter('element_error_array_json',$this->errormessage_stack);
        
        //set validation criteria
        $this->add_js_parameter('validate_crit_json3',$this->validation_json_array);
        
        if(isset($this->js_plugins_array))
        $this->add_js_parameter('plugins_array',$this->js_plugins_array);     
    }
    /**
     * Check if a specific object is implementing a interface or not
     * @param type $object
     * @param type $interface
     * @return boolean true if it implements, false if not
     */

    public function is_object_implementing_interface($object,$interface)
    {
        if(isset($object) and isset($interface) and is_object($object))
        {
            if($object instanceof $interface)
            return true;
            else
            return false;
        }        
    }
    
    /**
     * Check if $object is having properties mentioned in $properties_array
     * @param type $object
     * @param type $properties_array
     * @return boolean true if all properties are implemented in $object , else false
     */
    public function is_object_having_properties($object,$properties_array)
    {
        if(is_object($object) and is_array($properties_array))
        {
            $property_exists = true;
            foreach($properties_array as $property)
            {
                if(!property_exists($object,$property))
                $property_exists = false;
            }           
        }
        else
        $property_exists = false;
        
        return $property_exists;
    }
    /**
     * Loads all plugins, if external plugin is valid, load it, else load default plugins
     */
    public function php_plugin_loader()//
    {
        $this->php_plugins_supported = array('REWRITE_ENGINE' => 1,'ERROR_HANDLER' => 1,'VALIDATOR' => 1);
        $this->php_plugins_load_defaults();
        
        if(is_array($this->php_plugins_array))
        {
            foreach($this->php_plugins_supported as $plugin_type => $value)
            {
                if(array_key_exists($plugin_type,$this->php_plugins_array))
                {
                    $plugin_object = $$this->php_plugins_array[$plugin_type];
                    if($this->php_plugin_validate($plugin_type,$plugin_object))
                    {
                        $this->load_php_plugin($plugin_type,$plugin_object);
                    }
                    else
                    {
                        //plugin_object = my.default_plugin_object[plugin_type];
                        $this->load_php_plugin($plugin_type,'default');
                    }
                }
                else
                $this->load_php_plugin($plugin_type,'default');
            }
        }
        else//load all default plugins, if plugins_array is not defined by user.
        {
            foreach($this->php_plugins_supported as $plugin_type => $value)
            {
               $this->load_php_plugin($plugin_type,'default');
            }
        }        
    }
    /**
     * Load all default plugins with default plugin objects using default plugin classes
     */
    public function php_plugins_load_defaults()
    {
        foreach($this->php_plugins_supported as $plugin_type => $value)
        {
            $default_class_name = '\\'.__NAMESPACE__.'\\plugins\\defaults\\'.$plugin_type;
            $this->php_plugins_default[$plugin_type] = new $default_class_name;
        }
    }
    
    /**
     * loads a single plugin,load default plugin if $plugin_object is default, else load external plugin
     * @param string $plugin_type
     * @param plugin_object $plugin_object
     */
    public function load_php_plugin($plugin_type,$plugin_object)
    {
        if(isset($plugin_type) and isset($plugin_object))
        {
            if($plugin_object === 'default')
            {   
                //load default plugin
                $this->php_plugins[$plugin_type]['obj'] =  $this->php_plugins_default[$plugin_type];
                $this->php_plugins[$plugin_type]['loaded'] = 'default';
            }
            else
            {
                //load custom external plugin
                $this->php_plugins[$plugin_type]['obj'] = $plugin_object;
                $this->php_plugins[$plugin_type]['loaded'] = 'external';
            }
        }
    }
    /**
     * checks if a plugin is having all required properties and methods
     * @param type $plugin_type
     * @param type $plugin_object
     * @return boolean true if plugin is valid , else false
     */
    public function php_plugin_validate($plugin_type,$plugin_object)
    {
        
        if(isset($plugin_type) and is_object($plugin_object))
        {
            $methods_valid = $this->is_object_implementing_interface($plugin_object,$plugin_type);
            
            $default_plugin_properties = get_class_vars('\\'.__NAMESPACE__.'\\plugins\\abstract\\'.$plugin_type.'_ABSTRACT');
            $external_plugin_properties = get_object_vars($plugin_object);
            $diff = array_diff_key($default_plugin_properties,$external_plugin_properties);
            
            $properties_valid = (count($diff) > 0)?false:true;
            
            if($methods_valid and $properties_valid)
            return true;
            else
            return false;
        }
        else
        return false;
    }
    public function prepare_js_validation_scripts_with_yui_profiler()
    {
        if($this->is_json_valid and $this->js_validation)      //only if validation json is valid and js validation is set to true
        {
           
            $headelement = $this->domobj->find('head');
            
            //Add jQuery script
            if(isset($this->js_jsquery_script))
            {
                $this->js_final_include_string = $this->js_jsquery_script;
            }
            else
            {
               //add our own jquery script
               $this->js_final_include_string = '<script src="https://code.jquery.com/jquery-1.11.3.js"></script>';
            }
            //Add cookie plugin for jQuery
            $this->js_final_include_string .= '<script src="https://newlogin.letterbox.in/tests/js.cookie.js"></script>';
            
            
            
            
            
            //Add All scripts in $this->js_include added using $this->add_js_script();
            
        
            
            $this->js_final_include_string .= $this->js_include;
            
            //Add All Plugin scripts in $this->js_plugin_script
            if(isset($this->js_plugin_scripts))
            $this->js_final_include_string .= $this->js_plugin_scripts;
            
            //Add our latest_validation_js script
            
            $this->js_final_include_string .= $this->latest_validation_js;
           
            
            $this->js_prepare_parameters();
            if($this->js_use_cookie)
            {  //set validation criteria cookie
               $this->setcookie('sskrepo_js_param',$this->array_to_json($this->js_parameter_array)); 
            }
            else
            {
               //unset parameter cookie;
               if(isset($_COOKIE['sskrepo_js_param'])) 
               {
                unset($_COOKIE['sskrepo_js_param']);
                setcookie('sskrepo_js_param','', time() - 3600);
               }
               //pass parameter string to SSKREPO_FORM_JS
               //SSKREPO_FORM_JS.params_json = $this->array_to_json($this->js_parameter_array);
               $this->js_final_include_string .= '<script>SSKREPO_FORM_JS.params_json = '.$this->array_to_json($this->js_parameter_array).';</script>'; 
            }
            //Add onready event to execute MAIN events_validator
            //$this->js_final_include_string .= "<script>$(document).ready(SSKREPO_FORM_JS.events_validator);</script>";
            //Add YUI profiler scripts and related CSS
            
            $this->js_final_include_string .= '<script type="text/javascript" src="https://yui-s.yahooapis.com/2.5.1/build/yuiloader/yuiloader-beta-min.js"></script>';
            $this->js_final_include_string .= '<script type="text/javascript" src="https://yui-s.yahooapis.com/2.5.1/build/profiler/profiler-beta-min.js"></script>';
            //$this->js_final_include_string .= '<script type="text/javascript" src="https://developer.yahoo.com/yui/examples/profilerviewer/assets/dpSyntaxHighlighter.js"></script>';
            //$this->js_final_include_string .= '<link rel="stylesheet" type="text/css" href="https://developer.yahoo.com/yui/examples/profilerviewer/assets/dpSyntaxHighlighter.css">';
            $this->js_final_include_string .= '<script src="https://newlogin.letterbox.in/tests/yui_profiler.js"></script>';;
            
            if(!empty($headelement))
            {
                foreach($headelement as $head)
                {
                     $head->innertext = $head->innertext.$this->js_final_include_string;
                }
            }
            else //Not proper HTML, as it is without <head> tag So we just need to add js include somewhere
            {
                $forms = $this->domobj->find('form');
                foreach($forms as $someform)
                {}//just to get some random form in the HTML , so that we can append <script> code next to it only if <head> is not available
                $someform->outertext = $someform->outertext.$this->js_final_include_string;//just adding js include next to last accessed Form element
            }
            return true;
        }
        else
        return false;       
    }
    public function add_js_validation_with_yui_profiler($form_js_path,$cookie_use=true)
    {
        if(!$cookie_use)
        $this->js_use_cookie = false;
        else
        $this->js_use_cookie = true;

        $this->js_validation = true; //USed by display(), so that it wont remove validatecrit from HTML
        $this->latest_validation_js = '<script type="text/javascript" src="'.$form_js_path.'"></script>';
        $this->yui_profiler = true;
        return true;
        
    }
}
?>