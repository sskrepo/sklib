<?php
namespace sskrepo\sklib\form;
/**
 * Used by sskrepo\\sklib\\form\\form as view object for loading values to be used in html form where we can use $view->value in html form
 */
class view
{
    
   private $data;

   public function __get($varName){

      if (!array_key_exists($varName,$this->data)){
          //this attribute is not defined!
          return NULL;
      }
      else return $this->data[$varName];

   }
   public function __set($varName,$value){
      $this->data[$varName] = $value;
   }

}

