<?php
namespace sskrepo\sklib\form;
/**
 * Creditcard validation class
 * Supports a list of credit cards
 * @see loadcard_list() for supported list of cards
 * @code
 * Example Usage:
 * $creditcard = new \sskrepo\sklib\form\creditcard;
 * $cardnumber = $var;
 * $cardtype_to_match = isset($arg['cardtype_to_match'])?$arg['cardtype_to_match']:null;
 * $this->result = $creditcard->validate_card($cardnumber,$cardtype_to_match);
 * @endcode
 * $creditcard->validate_card returns a array with 'status','error','cardnumber','cardtype' keys               
 * 
 */
class creditcard
{
    /**
     *Credit card number to be validated
     * @var int $cradnumber 
     */
    public $cardnumber;
    /**
     *If $cradnumber need to be matched against a specific credit card type, then this need to be set to that card type
     * @var string  $cardtype_to_match
     * @see loadcard_list() for supported card types
     */
    public $cardtype_to_match;
    /**
     *$cardsarray is used to store a list of all supported card types and their related data
     * @var array $cardsarray 
     * loadcard_list() loads this parameter with supported cards array
     * @see loadcard_list() for supported card types
     */
    public $cardsarray;
    /**
     *Result of validate_card() to validate a given cardnumber/cardtype
     * An array of 'status','error','cardnumber','cardtype' as keys
     * @var array $result
     * @see validate_card()
     */
    public $result = array();
    
    /**
     * Loads All supported cards list to $cardsarray
     * @see $cardsarray
     */
    public function loadcard_list()
    {
        $this->cardsarray = array (  
                   'American Express' => array (  
                          'length' => '15', 
                          'prefixes' => '34,37',
                          'checkdigit' => true
                         ),
                   'Diners Club Carte Blanche' => array (
                          'length' => '14', 
                          'prefixes' => '300,301,302,303,304,305',
                          'checkdigit' => true
                         ),
                   'Diners Club' => array (
                          'length' => '14,16',
                          'prefixes' => '36,38,54,55',
                          'checkdigit' => true
                         ),
                   'Discover' => array ( 
                          'length' => '16', 
                          'prefixes' => '6011,622,64,65',
                          'checkdigit' => true
                         ),
                   'Diners Club Enroute' => array ( 
                          'length' => '15', 
                          'prefixes' => '2014,2149',
                          'checkdigit' => true
                         ),
                   'JCB' => array ( 
                          'length' => '16', 
                          'prefixes' => '35',
                          'checkdigit' => true
                         ),
                   'Maestro' => array (
                          'length' => '12,13,14,15,16,18,19', 
                          'prefixes' => '5018,5020,5038,6304,6759,6761,6762,6763',
                          'checkdigit' => true
                         ),
                   'MasterCard' => array (
                          'length' => '16', 
                          'prefixes' => '51,52,53,54,55',
                          'checkdigit' => true
                         ),
                   'Solo' => array ( 
                          'length' => '16,18,19', 
                          'prefixes' => '6334,6767',
                          'checkdigit' => true
                         ),
                   'Switch' => array (
                          'length' => '16,18,19', 
                          'prefixes' => '4903,4905,4911,4936,564182,633110,6333,6759',
                          'checkdigit' => true
                         ),
                   'VISA' => array (
                          'length' => '16', 
                          'prefixes' => '4',
                          'checkdigit' => true
                         ),
                   'VISA Electron' => array (
                          'length' => '16', 
                          'prefixes' => '417500,4917,4913,4508,4844',
                          'checkdigit' => true
                         ),
                   'LaserCard' => array (
                          'length' => '16,17,18,19', 
                          'prefixes' => '6304,6706,6771,6709',
                          'checkdigit' => true
                         )
                );
        
    }
    /**
     * Checks if $cardtype is a supported card type
     * @param string $cardtype
     * @return boolean true if $cardtype is supported, else false
     * @see loadcard_list() for supported card types
     */
    public function is_supported_cardtype($cardtype)
    {
          $type = -1;
          $this->loadcard_list();
          foreach($this->cardsarray as $ctype => $cprops) 
          {
            // See if it is this card (ignoring the case of the string)
            if (strtolower($cardtype) == strtolower($ctype)) {
              $type = $ctype;
              break;
            }
          }
          // If card type not found, report an error
          if ($type == -1) 
          return false;
          else
          return true;
    }
    
    /**
     * Modulus10 is a validation method to validate credit card.
     * This method validated $cardnumber for modulus10 algorithm 
     * @see link https://en.wikipedia.org/wiki/Luhn_algorithm for more details about Modulus 10
     * @param int $cardnumber
     * @return boolean True if cardnumber has valid modulus10, else false
     */
    public function check_modulus10($cardnumber)
    {
        if(isset($cardnumber))
        {
        
            $checksum = 0;                                  // running checksum total
            $mychar = "";                                   // next char to process
            $j = 1;                                         // takes value of 1 or 2

            // Process each digit one by one starting at the right
            for ($i = strlen($cardnumber) - 1; $i >= 0; $i--) {

              // Extract the next digit and multiply by 1 or 2 on alternative digits.      
              $calc = $cardnumber{$i} * $j;

              // If the result is in two digits add 1 to the checksum total
              if ($calc > 9) {
                $checksum = $checksum + 1;
                $calc = $calc - 10;
              }

              // Add the units element to the checksum total
              $checksum = $checksum + $calc;

              // Switch the value of j
              if ($j ==1) {$j = 2;} else {$j = 1;};
            } 

            // All done - if checksum is divisible by 10, it is a valid modulus 10.
            // If not, report an error.
            if ($checksum % 10 != 0) {
             return false;
            }
            else
            return true;
        }
        else
        return false;

    }
    /**
     * Checks if $this->cardnumber is already set, if yes then returns cardtype if valid card number, else null
     * @return string Returns cardtype if valid card ,else null
     */
    public function get_cardtype()
    {
        //Card specific Checks
        if(isset($this->cardnumber))
        {
            if(!is_array($this->cardsarray))
            {
                $this->loadcard_list();
            }
            foreach($this->cardsarray as $cardtype => $cardproperties)
            {
                   // Load an array with the valid prefixes for this card
                  
                  $prefix = explode(',',$cardproperties['prefixes']);

                  // Now see if any of them match what we have in the card number  
                  $PrefixValid = false; 
                  for ($i=0; $i<sizeof($prefix); $i++) {
                    $exp = '/^' . $prefix[$i] . '/';
                    if (preg_match($exp,$this->cardnumber)) {
                      $PrefixValid = true;
                      break;
                    }
                  }

                  // If it isn't a valid prefix there's no point at looking at the length
                  if ($PrefixValid) 
                  {
                      // See if the length is valid for this card
                      $LengthValid = false;
                      $lengths = explode(',',$cardproperties['length']);
                      for ($j=0; $j<sizeof($lengths); $j++) {
                        if (strlen($this->cardnumber) == $lengths[$j]) {
                          $LengthValid = true;
                          break;
                        }
                      }

                      // See if all is OK by seeing if the length was valid. 
                      if ($LengthValid) {
                         return $cardtype;
                      }; 
                  }    
            }
            return null;//Tried matching against all supported cardtypes but failed to match, so return null;
        }
        return null;
    }
    /**
     * Main card validation function
     * Validates if $cardnumber is a valid supported credit card, 
     * if $cardtype_to_match is provided, then matches card number for that card type,
     * else validates against all supported cardtypes
     * 
     * @param int $cardnumber
     * @param string $cardtype_to_match
     * @return array Returns $result array
     * An array of 'status','error','cardnumber','cardtype' as keys
     * @see $result
     */
    public function validate_card($cardnumber,$cardtype_to_match = null)
    {
        
        $this->loadcard_list();
        if(is_array($this->cardsarray))
        {
            if(isset($cardtype_to_match))//if card type to match is not provided, we match it with all possible cardtypes
            {
                if(!$this->is_supported_cardtype($cardtype_to_match))//if cardtype_to_match is provided then it must be one of the supported ones
                {
                    $this->result['status'] = false;
                    $this->result['error'] = 'Card type give to match is not one of the Supported Card types';
                    $this->result['card_number'] = null;
                    $this->result['card_type'] = 'Invalid';
                    return $this->result;
                }
                $this->cardtype_to_match = $cardtype_to_match;
            }
            
            
            //Check for Numerics and min and max length of any card possible(13-19)
            if(isset($cardnumber))
            {
                
                // Remove any spaces from the credit card number
                $cardnumber = str_replace ('-', '', $cardnumber);
                $cardnumber = str_replace (' ', '', $cardnumber);
                
                
                if (!preg_match("/^[0-9]{13,19}$/",$cardnumber))
                {
                    $this->result['status'] = false;
                    $this->result['error'] = 'Card number provided is not having right length or is having non-numeric characters';
                    $this->result['card_number'] = $this->cardnumber;
                    $this->result['card_type'] = 'Invalid';
                    return $this->result;
                }
                $this->cardnumber = $cardnumber;
            }
            else
            {
                $this->result['status'] = false;
                $this->result['error'] = 'Card Number Cannot be empty';
                $this->result['card_number'] = null;
                $this->result['card_type'] = 'Invalid';
                return $this->result;
            }
            
            //Check for modulus10 of card is valid
            if(!$this->check_modulus10($this->cardnumber))
            {
                $this->result['status'] = false;
                $this->result['error'] = 'Cardnumber failed Modulus10 verfication';
                $this->result['card_number'] = $this->cardnumber;
                $this->result['card_type'] = 'Invalid';
                return $this->result;
            }
            else//passed modulus10 verification
            {
                $cardtyp = $this->get_cardtype();
                if($cardtyp == null)
                {
                    $this->result['status'] = false;
                    $this->result['error'] = 'Cardnumber doesnt match any supported cardtype';
                    $this->result['card_number'] = $this->cardnumber;
                    $this->result['card_type'] = 'Invalid';
                    return $this->result;
                }
                else
                {    
                    if(!isset($this->cardtype_to_match)) 
                    {
                        $this->result['status'] = true;
                        $this->result['error'] = null;                       
                    }
                    elseif(strtolower($this->cardtype_to_match) == strtolower($cardtyp))
                    {
                        $this->result['status'] = true;
                        $this->result['error'] = null;
                    }
                    else
                    {
                        $this->result['status'] = false;
                        $this->result['error'] = 'Cardnumber is a valid creditcard number, but not the same type as '.$this->cardtype_to_match;                 
                    }
                    $this->result['card_number'] = $this->cardnumber;
                    $this->result['card_type'] = $cardtyp;
                    return $this->result;
                    
                }
                
            }
            
        }
    }
    
}

/*
echo'    <form name="seccodeform"  id="seccodeform" method="post">
    <p>                                                 
    <label for="seccode">Please Enter Security Code(Valid for 30 minutes only)</label>
    <input type="text" name="seccode" id="seccodeid"/>
    <p>
    <input type="submit" value="Next" />
    </p>
    </form>';

$ccard = new creditcard;
//$cardnumber = 'xxxxxxxxxxxx';
$cardnumber = $_REQUEST['seccode'];
$cardtype = null;
$this->result = $ccard->validate_card($cardnumber,$cardtype);

foreach($this->result as $key => $value)
{
    echo $key.'  :  '.$value;
}*/
?>