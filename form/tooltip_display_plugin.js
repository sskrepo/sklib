var tooltip_display_plugin = (function(){


var my = {};
my.opentip_element_array = new Array();
my.plugin_display_error_event = 'select';
function implode(glue, pieces) {
  //  discuss at: http://phpjs.org/functions/implode/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Waldo Malqui Silva
  // improved by: Itsacon (http://www.itsacon.net/)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
  //   returns 1: 'Kevin van Zonneveld'
  //   example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
  //   returns 2: 'Kevin van Zonneveld'

  var i = '',
    retVal = '',
    tGlue = '';
  if (arguments.length === 1) {
    pieces = glue;
    glue = '';
  }
  if (typeof pieces === 'object') {
    if (Object.prototype.toString.call(pieces) === '[object Array]') {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}

my.plugin_display_error = function(element_error_array,elem)
{
    if(element_error_array.hasOwnProperty(elem.attr('id')))
    {
        var elem_error_array = element_error_array[elem.attr('id')];
        var nextelem = elem.next();
        //errormessage = isset(errormessage)?errormessage.trim():null;
        var labeldata = nextelem.prop('tagName');
        var errorhtml;
        if(elem_error_array.length > 1 )//implode with || only if atleast two errors are there
        {
            var error;
            //errorhtml = implode(" || ",elem_error_array);
            for(error in elem_error_array)
            {
                if(typeof errorhtml == 'undefined')
                {
                    errorhtml = '=>'+elem_error_array[error];
                }
                else
                {
                    errorhtml = errorhtml+'<br> =>'+elem_error_array[error];
                }
                
            }
        }
        else if(elem_error_array.length == 1)
        errorhtml = '=>'+elem_error_array['0'];
        else //no errors so set the box border also to black
        {
            errorhtml='';
            
        }
        
        if(!my.opentip_element_array.hasOwnProperty(elem.attr('id')))
        {    
            var myOpentip = new Opentip(elem,{style: 'alert'});
            my.opentip_element_array[elem.attr('id')] = myOpentip;
        }
        
        my.opentip_element_array[elem.attr('id')].setContent(errorhtml);
        //myOpentip.prepareToHide();
        if(errorhtml === '')
        {
            my.opentip_element_array[elem.attr('id')].deactivate();
           //elem.attr('style','border:solid 1px black'); 
           elem.css({
               'border-color' : 'black',
               'background-color' : 'rgb(220, 251, 164)'
             });
        }
        else
        {
            //elem.attr('style','border:solid 1px red'); 
            my.opentip_element_array[elem.attr('id')].activate();
        }
    }	
}
my.plugin_on_error_change = function(element_error_array,elem)
{
    // border-color: rgb(180, 207, 94);
    //background-color: rgb(255, 183, 183);
    
     //elem.attr('style','border:solid 1px black');
     //elem.attr('border-color','rgb(180, 207, 94)');
     //elem.attr('bgcolor','rgb(255, 183, 183)');
     elem.css({
       'border-color' : 'red',
       'background-color' : 'rgb(255, 183, 183)'
     });
    my.plugin_display_error(element_error_array,elem);
}
return my;
}());
