/**
 * Namespace SSKREPO_JSLIB , Useful JS functions library
 * @namespace SSKREPO_JSLIB
 * @author Sravan Kumar S
 */
var SSKREPO_JSLIB = (function(){
    
    var my = {};

    my.string = (function()
        {
            var my = {};
            
            /**
             * Trim a specific character if it is at beginning or end of a string
             * @function trimChar
             * @memberof SSKREPO_JSLIB
             * @param {string} string
             * @param {string} charToRemove
             * @returns {string} Returns processed string
             * @link http://stackoverflow.com/questions/26156292/jquery-trim-specific-character-from-string
             */
            my.trimChar = function(string, charToRemove) 
            {
                while(string.charAt(0)==charToRemove) {
                    string = string.substring(1);
                }

                while(string.charAt(string.length-1)==charToRemove) {
                    string = string.substring(0,string.length-1);
                }

                return string;
            }
            /**
             * Read a URI encoded JSON from cookie as array
             * @function read_json_cookie
             * @memberof SSKREPO_JSLIB
             * @param string name Cookie Name
             * @returns {Array|Object} Returns Array decoded rom JSON, if invalid json, returns null
             */
            my.read_json_cookie = function(name)
            {
             var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
             var result_decoded = decodeURIComponent(result[1].replace(/\+/g, ' '));//browser replaces spaces with + while creating ccokies, so need to replace + with space and also browser does URI encode, so we need to URIdecode
             var result_json;
             if(isValidJson(result_decoded))
             {
                var json_array = JSON.parse(result_decoded);
                return json_array;
             }
             else
             return null;
            }

            /**
             * Delete a cookie
             * @function delete_cookie
             * @memberof SSKREPO_JSLIB
             * @param {string} name Cookie Name
             * 
             */
            my.delete_cookie = function(name)
            {
              document.cookie = [name, '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/; domain=.', window.location.host.toString()].join('');
            }

            /**
             * Validates if str is a valid JSON or not
             * @function isValidJson
             * @memberof SSKREPO_JSLIB
             * @param {string} str
             * @returns {Boolean} True if str is valid JSON,else false
             */

            my.isValidJson = function(str)
            {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            }
        
            return my;
        }());
    my.obj = (function()
        {
            var my = {};
            my.empty = function(str)
            {
                return (!str || 0 === str.length);
            }
            /**
             * If a variable is set or not
             * @function isset
             * @memberof SSKREPO_JSLIB
             * @returns {Boolean} True if set, else false
             * @link http://phpjs.org/functions/isset/
             * @author Kevin van Zonneveld (http://kevin.vanzonneveld.net)
             */
            my.isset = function() 
            {
              var a = arguments,
                l = a.length,
                i = 0,
                undef;

              if (l === 0) {
                throw new Error('Empty isset');
              }

              while (i !== l) {
                if (a[i] === undef || a[i] === null) {
                  return false;
                }
                i++;
              }
              return true;
            }
            /**
             * Equivalent to array.length for associative arrays(arrays with strings as keys instead of numbers)
             * @function numberOfKeysinObject
             * @memberof SSKREPO_JSLIB
             * @param {Object} obj
             * @returns {Number} Number of Properties in a Object
             * @link http://stackoverflow.com/questions/126100/how-to-efficiently-count-the-number-of-keys-properties-of-an-object-in-javascrip
             */
            my.numberOfKeysinObject = function(obj)
            {
                if (!Object.keys) 
                {
                    Object.keys = function (obj) {
                        var keys = [],
                            k;
                        for (k in obj) {
                            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                                keys.push(k);
                            }
                        }
                        return keys;
                    };
                }
                return Object.keys(obj).length;
            }
            /**
             * Find if an associative array 'obj' is having any keys with vaule 'val' in it , if yes return such first occurrence
             * in Javascript arrays only are designed to have numeric keys, if you want strings as keys then you are just adding a property to the array object
             * Javascript doesnt treat them as Array keys, so you cant use Jquery $.inArray to find key for a specific value in a associative array(nothing but set of properties of a object)
             * So use below function for finding a key of a specific value in Associative arrays   
             * @function findPropertyWithValue
             * @memberof SSKREPO_JSLIB
             * @param {string} val
             * @param {Object} obj
             * @returns {String|Null} Retruns First occurrence of Key that matches its value to 'val', null if None found
             */
            my.findPropertyWithValue = function(val,obj) 
            {

                for (var i in obj) {
                    if (obj.hasOwnProperty(i) && obj[i] === val) {
                        return i;
                    }
                }
                return null;
            }

            /**
             * Validates if Object 'obj' is having all the methods mentioned in methods_array
             * @function is_object_having_methods
             * @memberof SSKREPO_JSLIB
             * @param {Object} obj
             * @param {array} methods_array Array of methods names to be checked in Obj
             * @returns {Boolean} True of all methods are existing in Obj, else false
             */

            my.is_object_having_methods = function(obj,methods_array)
            {
                var methods_exists,method;
                if(typeof obj !== 'undefined' && typeof methods_array !== 'undefined')
                {
                    if($.isArray(methods_array))
                    {
                        methods_exists = true,method;
                        for(method in methods_array)
                        {
                            if(obj.hasOwnProperty(method))
                            {
                                if(typeof obj[method] !== 'function')
                                methods_exists = false;
                            }
                            else
                            methods_exists = false;
                        }
                    }
                    else
                    {   methods_exists = true;
                        if(obj.hasOwnProperty(methods_array))
                        {
                            if(typeof obj[methods_array] !== 'function')
                            methods_exists = false;

                        }
                        else
                        methods_exists = false;
                    }

                }
                else
                methods_exists = false;

                return methods_exists;
            }

            /**
             * Validates if Object 'obj' is having all the properties mentioned in properties_array
             * @function is_object_having_properties
             * @memberof SSKREPO_JSLIB
             * @param {Object} obj
             * @param {array} properties_array Array of Properties names to be checked in Obj
             * @returns {Boolean} True of all Properties are existing in Obj, else false
             */
            my.is_object_having_properties = function(obj,properties_array)
            {
                var property_exists,property;
                if(typeof obj !== 'undefined' && typeof properties_array !== 'undefined')
                {
                    if($.isArray(properties_array))
                    {
                        property_exists = true;
                        for(property in properties_array)
                        {
                            if(!obj.hasOwnProperty(property))
                            property_exists = false;
                        }

                    }
                    else
                    {
                        if(!obj.hasOwnProperty(property))
                        property_exists = false;
                        else
                        property_exists = true;

                    }
                }
                else
                property_exists = false;

                return property_exists;
            }

            /**
             * Get list of all methods and properties of a Object
             * @function get_Methods_and_Props
             * @memberof SSKREPO_JSLIB
             * @param {Object} obj
             * @returns {Array} Array of methods and Properties, 'methods','props' as keys
             */
            my.get_Methods_and_Props = function(obj)
            {
                var methods = [],nonmethods = [],methods_props = [];
                for(var m in obj) {
                    if(typeof obj[m] == "function") {
                        methods.push(m)
                    }
                    else
                    {
                        nonmethods.push(m);
                    }

                }
                methods_props['methods'] = methods;
                methods_props['props'] = nonmethods;
                return methods_props;
            }

            return my;
            
        }());
    my.array = (function()
        {   var my = {};
            /**
             * @link http://phpjs.org/functions/in_array/
             * @author  Kevin van Zonneveld (http://kevin.vanzonneveld.net)
             */
            my.in_array = function(needle, haystack, argStrict) 
            {
              var key = '',
                strict = !! argStrict;
              if (strict) {
                for (key in haystack) {
                  if (haystack[key] === needle) {
                    return true;
                  }
                }
              } else {
                for (key in haystack) {
                  if (haystack[key] == needle) {
                    return true;
                  }
                }
              }

              return false;
            }
            /**
             * Same as PHP implode
             * @function implode
             * @memberof SSKREPO_JSLIB
             * @returns string imploded string of glue and pieces
             * @link http://phpjs.org/functions/implode/
             * @author Kevin van Zonneveld (http://kevin.vanzonneveld.net)
             */

            my.implode = function(glue, pieces)
            {
              var i = '',
                retVal = '',
                tGlue = '';
              if (arguments.length === 1) {
                pieces = glue;
                glue = '';
              }
              if (typeof pieces === 'object') {
                if (Object.prototype.toString.call(pieces) === '[object Array]') {
                  return pieces.join(glue);
                }
                for (i in pieces) {
                  retVal += tGlue + pieces[i];
                  tGlue = glue;
                }
                return retVal;
              }
              return pieces;
            }
           /**
            * Accepts two arrays and determines if A is subset of B
            * @function isAsubsetofB
            * @memberof SSKREPO_JSLIB
            * @param {Array} A
            * @param {Array} B
            * @returns {Boolean} True if A is subset of B, else false
            */
            my.isAsubsetofB = function(A,B)
            {
               var i,C = new Array();
               for(i in B)
               {
                   var indexinA = $.inArray(B[i],A);
                   if(indexinA !== -1)//if B[i] found in A
                   {
                       delete(A[indexinA]);
                   }
               }
               //load A into C and check if C length is > 0
               for(i in A)
               {
                   C[i] = A[i];
               }
               if(C.length > 0)
               {
                   return false;
               }
               else
               return true;
            }
            return my;

        }());
   
    return my;
}());
