<?php

namespace sskrepo\sklib\form\plugins\abstracts;
/**
 * Abstract class to be implemented by plugins of type 'REWRITE_ENGINE' to be used with \\sskrepo\\sklib\\form\\form
 */
abstract class REWRITE_ENGINE_ABSTRACT
{
    /**
     * Abstract function , to check if $cond_op is supported, by implemting plugin, if yes return true, else return false
     * @param string $cond_op is just the cond['op'], opreation name for the condition return true(or)false based on a specific condition is supported by this validator
     * @return boolean True if $cond_op is supported by plugin implementing this Abstract, else false
     */
    abstract function is_condition_supported($cond_op);
    /**
     * Abstract functoin, to be implemented by plugin of type 'REWRITE_ENGINE'.
     * @param array $condition 
     * $condition is a array in below format
     * $condition[$op] = $arg
     * $op is Condition Operation, like isinit, creditcard etc
     * $arg is an array
     * $arg['arg'] - Argument values for $op, for example , if $op is 'creditcard', $arg['arg'] can be $cardnumber
     * @return array $rewritten_cond
     * $rewritten_cond should be in single array or multiple arrasy each of same format as $condition, with different $ops and $args in them
     * 
     */
    abstract function rewrite_cond($condition);
}

?>