<?php

namespace sskrepo\sklib\form\plugins\abstracts;
/**
 * Abstract class to be implemented by plugins of type 'ERROR_HANDLER' to be used with \\sskrepo\\sklib\\form\\form
 */
abstract class ERROR_HANDLER_ABSTRACT
{
    
    /**
     * @param array $errormessage_stack An array in format $elementid => $elements_error_array
     * $elements_error_array is in format $i => $errormessage
     * @param simple_html_dom $domobj is a simple_html_dom object where you can traverse entire dom tree of HTML loaded
     * @return void
     */
    abstract function display_errors($errormessage_stack,$domobj);
}

?>