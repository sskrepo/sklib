<?php

namespace sskrepo\sklib\form\plugins\abstracts;
/**
 * Abstract class to be implemented by plugins of type 'VALIDATOR' to be used with \\sskrepo\\sklib\\form\\form
 */
abstract class VALIDATOR_ABSTRACT
{
    /**
     * Abstract function , to check if $validation_cond is supported, by implemting plugin, if yes return true, else return false
     * @param string $validation_cond is just the cond['op'], opreation name for the condition return true(or)false based on a specific condition is supported by this validator
     * @return boolean True if $validation_cond is supported by plugin implementing this Abstract, else false
     */
    abstract function is_condition_supported($validation_cond);
    
    /**
     * Abstract function to be implemented in plugin type 'VALIDATOR' to validate a specific html element agains $cond, and return $error or default $errormessage accordingly
     * @param simple_html_dom_node $element is dom element which needs to be validated
     * @param string $cond is the condition to be validated on $element
     * $cond is a array in below format
     * $cond['op'] = 'creditcard' //operation
     * $cond['arg'] argument array for the operation for example array('credit_card_type_elementid')
     * @param string $error is errormessage if element fails validation
     * @param simple_html_dom $domobj is a simple_html_dom object where you can traverse entire dom tree of HTML loaded, can be used if you are writing validtor which can have dependency on other elements(like creditcard), so that you can access all elements in the DOM tree using $domobj->find()
     * @return array $result
     * for each confition it should return result array with below structure
     * $result['status'] boolean, true if valid , false otherwise
     * $result['errormessage'] string error message to display if validation fails, usually you can just return $error passed to it,this is used in case $error is not set by user, in that case your validator should return a default error message, so that errormessage should not be blank
     */
    
    abstract function validate_element($element,$cond,$error,$domobj);
    
   
}

?>