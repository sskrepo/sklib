<?php

namespace sskrepo\sklib\form\plugins\defaults;
/**
 * Default plugin of type 'VALIDATOR' used in \\sskrepo\\sklib\form\\form
 */
class VALIDATOR extends \sskrepo\sklib\form\plugins\abstracts\VALIDATOR_ABSTRACT
{
    /**
     *
     * @var \\sskrepo\\sklib\\safeinput $safeinput_validator
     * @see \\sskrepo\\sklib\\safeinput
     */
    public $safeinput_validator;
    /**
     * Abstract function , to check if $validation_cond is supported, by implemting plugin, if yes return true, else return false
     * @param string $validation_cond is just the cond['op'], opreation name for the condition return true(or)false based on a specific condition is supported by this validator
     * @return boolean True if $validation_cond is supported by plugin implementing this Abstract, else false
     */
    public function is_condition_supported($cond_op)
    {
         $this->safeinput_validator  = new \sskrepo\sklib\safeinput;
         return $this->safeinput_validator->validate($cond_op,null,null,null,true);//last argument is true, so that it just checks if operation is supported by validator, if yes, it returns true , if not false
    }
    
    /**
     * Validate a specific html element agains $cond, and return $error or default $errormessage accordingly
     * @param simple_html_dom_node $element is dom element which needs to be validated
     * @param string $cond is the condition to be validated on $element
     * $cond is a array in below format
     * $cond['op'] = 'creditcard' //operation
     * $cond['arg'] argument array for the operation for example array('credit_card_type_elementid')
     * @param string $error is errormessage if element fails validation
     * @param simple_html_dom $domobj is a simple_html_dom object where you can traverse entire dom tree of HTML loaded, can be used if you are writing validtor which can have dependency on other elements(like creditcard), so that you can access all elements in the DOM tree using $domobj->find()
     * @return array $result
     * for each confition it should return result array with below structure
     * $result['status'] boolean, true if valid , false otherwise
     * $result['errormessage'] string error message to display if validation fails, usually you can just return $error passed to it,this is used in case $error is not set by user, in that case your validator should return a default error message, so that errormessage should not be blank
     */
    public function validate_element($ielement,$cond,$error = null,$formobj)
    {
        $validation_result = array();
        $validation_result['errormessages_to_add'] = array();
        $validation_result['errormessages_to_remove'] = array();
        
        if($ielement->tag != 'textarea')
        $val = $ielement->value;
        else
        $val = $ielement->innertext;
        
        $arg = isset($cond['arg'])?$cond['arg']:NULL;
        
        /*if(isset($formobj->required_element_array[$ielement->id]) and $cond['op'] == 'required')
        {
            return array("status" => $formobj->required_element_array[$ielement->id],"errormessage" => $error);//to prevent infinite loops for required condition between elements
            //return array("status" => true,"errormessage" => $error);
        }*/
        //if(isset($ielement) and isset($cond) and isset($formobj->domobj))
        //{
            if($cond['op'] == 'compare_element')//to compare two elements
            {
                if(isset($arg['elementid']) and $arg['operator'])
                {
                    $el_search_string = '#'.$arg['elementid'];
                    $elem = $formobj->currentform->find($el_search_string);
                    foreach($elem as $e)
                    {
                        if($e->tag != 'textarea')
                        $operand2 = $e->value;
                        else
                        $operand2 = $e->innertext;
                    }
                }
                //elementname for comparision is not supported, using elementid only is supported for compare_element
                /*elseif(isset($arg['elementname']) and $arg['operator'])
                {
                    $el_search_string = '[name='.$arg['elementname'].']';
                    $elem = $formobj->currentform->find($el_search_string);
                    foreach($elem as $e)
                    {
                        if($e->tag != 'textarea')
                        $operand2 = $e->value;
                        else
                        $operand2 = $e->innertext;
                    }

                }*/
                $arg1['comparewith'] = $operand2;
                $arg1['operator'] = $arg['operator'];
                if($error == null)
                {
                    //default error message, of all other condition default messages are set in validator itslef, for this we need to set here
                    $error = "Should be ".$arg1['operator']." elementid :".$arg['elementid'];
                }
                $validation_result = $this->safeinput_validator->validate('compare',$val,$arg1,$error);
            }
            elseif($cond['op'] == 'creditcard')
            {
                $cardnumber = null;
                $arg1 = array();

                if(isset($cond['arg']))
                {
                    $arg = $cond['arg'];
                    if(isset($arg["card_type_elementid"]))
                    {
                        $card_type_elem = $formobj->currentform->find('#'.$arg["card_type_elementid"]);
                        foreach($card_type_elem as $ce)
                        $card_type_elem = $ce;

                        if($card_type_elem->tag == 'select')//card type list will be a 'select' element and we need to get the value of <option> element inside it which has selected property as 'selected'
                        {
                            foreach($card_type_elem->find('option') as $t1)
                            {
                                if($t1->selected == 'selected')
                                {
                                    $optionvalue = $t1->value;
                                    break;
                                }

                            }
                            $arg1['cardtype_to_match'] = $optionvalue;
                        }
                    }

                    $cardnumber = $val;

                }
                return $this->safeinput_validator->validate('creditcard',$cardnumber,$arg1,$error);

            }
            elseif($cond['op'] == 'intl_phone')//validate international phone numbers
            {
                if(isset($arg['ccelementid']))
                {
                    $el_search_string = '#'.$arg['ccelementid'];
                    $elem = $formobj->currentform->find($el_search_string);
                    foreach($elem as $e)
                    {}
                    
                    if($e->tag == 'select')//if country code is a "Select" element in html then we need to find which country code user selected
                    {
                        foreach($e->find('option') as $t1)
                        {
                            if($t1->selected == 'selected')
                            {
                                $optionvalue = $t1->value;
                                break;
                            }

                        }
                        $operand2 = $optionvalue;
                    }
                    else
                    {                       
                        if($e->tag != 'textarea')
                        $operand2 = $e->value;
                        else
                        $operand2 = $e->innertext;                       
                    }
                    
                    $arg1['input_cc'] = $operand2;
                }               
                
                if(isset($arg['allowed_cc_list']))
                $arg1['allowed_cc_list'] = $arg['allowed_cc_list'];
                else
                $arg1['allowed_cc_list'] = NULL;
                
                $validation_result = $this->safeinput_validator->validate('intl_phone',$val,$arg1,$error);
            }
            elseif($cond['op'] == 'required')
            {
                //handled required condition, used for dependency between elements, example, validate Y if X is valid or Xis checked or X is selected etc
                //
                /*input :
                 *      valid
                 *checkbox:
                 *      checked
                 *select:
                 *      selected
                 *textarea:
                 *      valid
                 *      
                 * valid implementation
                 *     validate the element without adding any errors and check if validation_result is true or false
                 *      
                 */
                
                if(isset($arg))
                {
                    $req_elements_err = 0;
                    $req_errorstack = null;
                    foreach($arg as $req_element_id => $req_element_condition)
                    {

                        $curr_element_err = 0;
                        if(isset($req_element_id))
                        {
                                //condition can be valid, checked,selected

                                $req_element = $formobj->currentform->find('#'.$req_element_id);
                                foreach($req_element as $e)
                                $req_element = $e;
                                if(!is_array($req_element_condition))//if not array 
                                {
                                        $req_arg =  explode('|',$req_element_condition);//arg format "elementid":"valid|errormessage if not valid"
                                        $req_arg_cond = trim($req_arg[0]);
                                        $req_errormessage = isset($req_arg[1])?$req_arg[1]:null;

                                        if(isset($req_element))//if element exists
                                        {
                                            if($req_arg_cond == 'valid' and $req_element->tag == 'input' )
                                            {
                                                //$formobj->required_element_array[$ielement->id] = $formobj->is_element_valid($ielement);//Used to prevent infinite loops for required condition between elements
                                                if(!$formobj->is_element_valid($req_element))
                                                {
                                                    $curr_element_err++;
                                                }
                                                /*foreach($formobj->element_event_validation_array[$formobj->currentform->id][$req_element_id] as $op => $cond)
                                                {
                                                    $eachcond['op'] = $op;
                                                    $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                                                    $err = isset($cond['error'])?$cond['error']:null;           

                                                    $req_result = $formobj->validate_element($req_element,$eachcond,$err);
                                                    if(!$req_result['status'])
                                                    {
                                                        $curr_element_err++;
                                                        break;//break at first validation failure for a element, no need to check further as anyway it is invalid

                                                    }

                                                }
                                                unset($formobj->required_element_array[$ielement->id]);*/
                                            }
                                            elseif($req_arg_cond == 'checked') 
                                            {
                                                $req_element = $formobj->currentform->find('#'.$req_element_id);
                                                if(isset($req_element[0]))//above find return array[0] set to null if no element found
                                                {   
                                                    foreach($req_element as $e)
                                                    $req_element = $e;
                                                    if($req_element->tag == 'input' and $req_element->type == 'checkbox')//if element itself is a checkbox
                                                    {
                                                        if($req_element->checked != 'checked')
                                                        $curr_element_err++;//checkbox is not checked
                                                    }                                           
                                                    elseif($req_element->tag == 'fieldset')
                                                    {   //if element is a fieldset which has a set of checkboxes inside it, atleast one checkbox should have been checked for this condition to be valid
                                                        $checkbox_array = $req_element->find('input[type=checkbox]');
                                                        $atleast_one_checkbox_checked = NULL;
                                                        if(isset($checkbox_array))
                                                        {
                                                            foreach($checkbox_array as $checkbox)
                                                            {
                                                                 if($checkbox->checked == 'checked')
                                                                 {
                                                                    $atleast_one_checkbox_checked = TRUE;
                                                                    break;//break at first checked checkbox
                                                                 }

                                                            }

                                                        }
                                                        if($atleast_one_checkbox_checked != TRUE)
                                                        {
                                                            $curr_element_err++;
                                                        }

                                                    }
                                                }
                                            }
                                            elseif($req_arg_cond == 'selected' and $req_element->tag == 'select')
                                            {   $option_is_selected = NULL;
                                                foreach($req_element->find('option') as $t1)
                                                {
                                                    if($t1->selected == 'selected')
                                                    {
                                                       $option_is_selected = TRUE;
                                                    }
                                                }
                                                if($option_is_selected != TRUE)
                                                {
                                                    $curr_element_err++;
                                                } 
                                            }
                                            else
                                            {
                                                //do nothing
                                            }

                                        }
                                    if($curr_element_err != 0)
                                    {
                                        //$formobj->add_error($ielement->id,$req_errormessage);
                                        array_push($validation_result['errormessages_to_add'],$req_errormessage);
                                    }
                                    else
                                    {
                                        //$formobj->remove_error($ielement->id,$req_errormessage);
                                        array_push($validation_result['errormessages_to_remove'],$req_errormessage);
                                    }

                                }
                                else//if it is array
                                {
                                    //each required element condition is again specified with multiple validations.
                                    //required:
                                    /*{
                                     *      "elementid_1":
                                     *      {
                                     *          "isint":
                                     *          {
                                     *              "error":"elementid_1 is not valid integer"                        *              
                                     *          }
                                     *      }

                                    }*/
                                    $req_element_validation_array = $req_element_condition;
                                    if(isset($req_element_validation_array))
                                    {
                                        foreach($req_element_validation_array as $op => $cond)
                                        {
                                            $eachcond['op'] = $op;
                                            $eachcond['arg'] = isset($cond['arg'])?$cond['arg']:NULL;
                                            $req_errormessage = isset($cond['error'])?$cond['error']:NULL;
                                            $req_array_each_error_count = 0;

                                            if($eachcond['op'] != 'required')//nested 'required's are not supported//just ignore nested required
                                            {
                                                if($eachcond['op'] == 'valid' and $req_element->tag == 'input' )
                                                {

                                                    if(!$formobj->is_element_valid($req_element))
                                                    {

                                                        $req_array_each_error_count++;
                                                    }

                                                }
                                                elseif($eachcond['op'] == 'checked') 
                                                {
                                                    $req_element = $formobj->currentform->find('#'.$req_element_id);
                                                    if(isset($req_element[0]))//above find return array[0] set to null if no element found
                                                    {   
                                                        foreach($req_element as $e)
                                                        $req_element = $e;
                                                        if($req_element->tag == 'input' and $req_element->type == 'checkbox')//if element itself is a checkbox
                                                        {
                                                            if($req_element->checked != 'checked')
                                                            {
                                                            //checkbox is not checked
                                                            $req_array_each_error_count++;
                                                            }
                                                        }                                           
                                                        elseif($req_element->tag == 'fieldset')
                                                        {   //if element is a fieldset which has a set of checkboxes inside it, atleast one checkbox should have been checked for this condition to be valid
                                                            $checkbox_array = $req_element->find('input[type=checkbox]');
                                                            $atleast_one_checkbox_checked = NULL;
                                                            if(isset($checkbox_array))
                                                            {
                                                                foreach($checkbox_array as $checkbox)
                                                                {
                                                                     if($checkbox->checked == 'checked')
                                                                     {
                                                                        $atleast_one_checkbox_checked = TRUE;
                                                                        break;//break at first checked checkbox
                                                                     }

                                                                }

                                                            }
                                                            if($atleast_one_checkbox_checked != TRUE)
                                                            {

                                                                $req_array_each_error_count++;
                                                            }

                                                        }
                                                    }
                                                }
                                                elseif($eachcond['op'] == 'selected' and $req_element->tag == 'select')
                                                {   $option_is_selected = NULL;
                                                    foreach($req_element->find('option') as $t1)
                                                    {
                                                        if($t1->selected == 'selected')
                                                        {
                                                           $option_is_selected = TRUE;
                                                        }
                                                    }
                                                    if($option_is_selected != TRUE)
                                                    {

                                                        $req_array_each_error_count++;
                                                    } 
                                                }
                                                else if($eachcond['op'] == 'compare_element')//to compare two elements
                                                {
                                                    if($eachcond['arg']['operator'])
                                                    {
                                                        //$el_search_string = '#'.$arg['elementid'];
                                                        //$elem = $formobj->currentform->find($el_search_string);

                                                            if($req_element->tag != 'textarea')
                                                            $operand2 = $req_element->value;
                                                            else
                                                            $operand2 = $req_element->innertext;

                                                    }
                                                    $arg1['comparewith'] = $operand2;
                                                    $arg1['operator'] = $eachcond['arg']['operator'];
                                                    if($req_errormessage == null)
                                                    {
                                                        //default error message, of all other condition default messages are set in validator itslef, for this we need to set here
                                                        $req_errormessage = "Should be ".$arg1['operator']." elementid :".$req_element->id;
                                                    }
                                                    $req_result = $this->safeinput_validator->validate('compare',$val,$arg1,$req_errormessage);
                                                    if(!$req_result['status'])
                                                    {

                                                        $req_array_each_error_count++;
                                                    }
                                                    $req_errormessage = $req_result['errormessage'];
                                                }
                                                else
                                                {
                                                    $req_result = $this->validate_element($req_element,$eachcond,$req_errormessage,$formobj);
                                                    if(!$req_result['status'])
                                                    {

                                                        $req_array_each_error_count++;
                                                    }
                                                    $req_errormessage = $req_result['errormessage'];
                                                }
                                                if($req_array_each_error_count != 0)
                                                {
                                                    $curr_element_err++; 
                                                    //$formobj->add_error($ielement->id,$req_errormessage);
                                                    array_push($validation_result['errormessages_to_add'],$req_errormessage);
                                                }
                                                else
                                                {
                                                    //$formobj->remove_error($ielement->id,$req_errormessage);
                                                    array_push($validation_result['errormessages_to_remove'],$req_errormessage);
                                                }
                                            }
                                        }
                                    }                        
                                }
                                if($curr_element_err != 0)
                                {
                                    $req_elements_err++;
                                }

                        }
                    }
                }
                if($req_elements_err != 0)
                {
                    $validation_result['status'] = FALSE;
                    //$validation_result['errormessage'] = $req_errorstack;
                }
                else
                {
                    $validation_result['status'] = TRUE;

                }
            }
            else
            {   //append error label to the element and make border of the element to red if validation fails for that element
                $validation_result = $this->safeinput_validator->validate($cond['op'],$val,$arg,$error);
            }
            
            return $validation_result;
            //returns validation_result array with 'status' and 'errormessage';

    }
    
   
}

?>