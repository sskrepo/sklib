<?php

namespace sskrepo\sklib\form\plugins\defaults;
/**
 * Default class for plugin of type 'REWRITE_ENGINE' to be used with \\sskrepo\\sklib\\form\\form
 */
class REWRITE_ENGINE extends \sskrepo\sklib\form\plugins\abstracts\REWRITE_ENGINE_ABSTRACT
{
    /**
     *Array of Supported Conditions by this REWRITE_PLUGIN
     * @var array  $supported_conditions
     */
    public $supported_conditions;
    /**
     * Construct to initiate supported_conditions
     */
    public function __construct()
    {
        $this->supported_conditions = array('compare_element' => 1,
                                            'creditcard' => 1,
                                            'intl_phone' => 1);
    }
    /**
     * To check if $cond_op is supported, byt this default pluin, if yes return true, else return false
     * @param string $cond_op is just the cond['op'], opreation name for the condition return true(or)false based on a specific condition is supported by this validator
     * @return boolean True if $cond_op is supported by plugin , else false
     */
    public function is_condition_supported($cond_op)
    {
        if(isset($cond_op))
        {
            if(array_key_exists($cond_op,$this->supported_conditions))
            {
                return true;
            }
            else
            return false;
        }
        else
        return false;
    }
    /**
     * 
     * @param array $condition 
     * $condition is a array in below format
     * $condition[$op] = $arg
     * $op is Condition Operation, like isinit, creditcard etc
     * $arg is an array
     * $arg['arg'] - Argument values for $op, for example , if $op is 'creditcard', $arg['arg'] can be $cardnumber
     * @return array $rewritten_cond
     * $rewritten_cond should be in single array or multiple arrays each of same format as $condition, with different $ops and $args in them
     * 
     */
    public function rewrite_cond($condition)
    {
        //$this->supported_conditions = array('compare_element',
        //                                                  'creditcard');
        $rewritten_cond = $condition;
        foreach($condition as $op => $cond_array)
        {
            if($op == 'compare_element')
            {   
                //add dependson to the condition , so that if element with which we are comparing changed, actual element being compared will be refreshed
                if(isset($cond_array['arg']))
                {

                    if(isset($cond_array['arg']['elementid']))
                    $comparewith_elementid = $cond_array['arg']['elementid'];
                    
                    //else//may be this compare_element is coming from inside "required" then do nothing, as any dependency is taken care of by "required" itself

                }
                if(isset($comparewith_elementid))
                {
                    $dependson_array = array( $comparewith_elementid => 1);                     
                    $condition[$op]['dependson'] = $dependson_array;
                }
                $rewritten_cond = $condition;
            }
            elseif($op == 'creditcard')
            {
                //add dependson for card_type_elementid
                if(isset($condition['arg']))
                {

                    if(isset($condition['arg']['card_type_elementid']))
                    $card_type_elementid = $condition['arg']['card_type_elementid'];
                    
                    //else//may be this compare_element is coming from inside "required" then do nothing, as any dependency is taken care of by "required" itself

                }
                if(isset($card_type_elementid))
                {
                    $dependson_array = array( $card_type_elementid => 1);                     
                    $condition['dependson'] = $dependson_array;
                }
                $rewritten_cond = $condition;
                
                //add notempty for cardnumber
                $notemptycond = array();
                $notemptycond = array('error' => 'Credit Card number is Mandatory');
                $rewritten_cond['isnotempty'] = $notemptycond;
            }
            elseif($op == 'intl_phone')
            {   
                //add dependson to the condition , so that if ccelement changed, phone number elemnet will be validate again
                if(isset($cond_array['arg']))
                {

                    if(isset($cond_array['arg']['ccelementid']))
                    $ccelementid = $cond_array['arg']['ccelementid'];                   
                }
                if(isset($ccelementid))
                {
                    $dependson_array = array( $ccelementid => 1);                     
                    $condition[$op]['dependson'] = $dependson_array;
                }
                $rewritten_cond = $condition;
            }
        }
        return $rewritten_cond;
    
    }
           
}

?>