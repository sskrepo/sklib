<?php

namespace sskrepo\sklib\form\plugins\defaults;

/**
 * Default class for plugin of type 'ERROR_HANDLER' to be used with \\sskrepo\\sklib\\form\\form
 */

class ERROR_HANDLER extends \sskrepo\sklib\form\plugins\abstracts\ERROR_HANDLER_ABSTRACT
{
    /**
     * @param array $errormessage_stack An array in format $elementid => $elements_error_array
     * $elements_error_array is in format $i => $errormessage
     * @param simple_html_dom $domobj is a simple_html_dom object where you can traverse entire dom tree of HTML loaded
     * @return void
     */
    public function display_errors($errormessage_stack,$domobj)
    {
        if(is_array($errormessage_stack))
        {
            foreach($errormessage_stack as $elementid => $errormessage_array)
            {
                $ele = $domobj->find('#'.$elementid);
                foreach($ele as $e)//find always returns a array even if find with id;
                foreach($errormessage_array as $i => $errormessage)
                {
                    $this->add_error_message($e,$errormessage);
                }
            }
        }
    }
    public function add_error_message($domelement,$errormessage)
    {
        
        $domelement->style = "border:solid 1px red";
        $domelement->outertext .= "<label id='formerror--label' for='".$domelement->name."'><font color='red' size=2> || ".$errormessage."</font></label>";
        
    }
           
}

?>