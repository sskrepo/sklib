<?php
namespace sskrepo\sklib\url;
class url
{
    public function check_ifnot_prefix_protocol($url, $prefix = 'http://') //default http if not provided
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url))
        {
            $url = $prefix . $url;
        }

        return $url;
    }
    
    public function get_basedomain_url($str = '')
    {
        //From http://stackoverflow.com/questions/3211411/how-can-i-get-the-base-domain-name-from-a-url-using-php-eg-google-com-from-ima
        // $str must be passed WITH protocol. ex: http://domain.com
        //It works for all cases except for a two character domain name passed with a subdomain
        //example customer.me.uk in this case it returns customer.me.uk instead of me.uk

        //$str should have protocol like http:// or https:// or ftp or something ,otherwise
        //parse_url used will error out.
        //So check for it , if not there, add it
        $str = $this->check_ifnot_prefix_protocol($str);

        $url = @parse_url( $str );
        if ( empty( $url['host'] ) ) return;
        $parts = explode( '.', $url['host'] );
        $slice = ( strlen( reset( array_slice( $parts, -2, 1 ) ) ) == 2 ) && ( count( $parts ) > 2 ) ? 3 : 2;
        return implode( '.', array_slice( $parts, ( 0 - $slice ), $slice ) );
    }
    public function current_page_url() 
    {
         $pageURL = 'http';
         if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
         $pageURL .= "://";
         if ($_SERVER["SERVER_PORT"] != "80") {
          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
         } else {
          $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
         }
         return $pageURL;
    }
}

