<?php
//USAGE
//$a["one"]["two"]="1";
//print count_dimension($a);
//result 2
namespace sskrepo\sklib\arrays;

class arrays
{ 
  public function array_dim_count($Array,$count = 0)       //Keep count = 0 in the arguments only, becuase if place it inside function, it will reset to 0 everytime thei function is recursively being accessed.
  { 
    if(is_array($Array))
    { 
      return $this->array_dim_count(current($Array), ++$count);
    } else
    {
      return $count;
    }
  }
}
          
?>