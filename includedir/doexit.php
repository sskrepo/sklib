<?php
//doexit is used to exit the application code while running any time for example if there is a error while connecting database, we simply
//exit the code. This is used instead of directly using exit() , so that in future if we want to handle such exceptions instead of just exiting the code
//we can add the code necessary to handle that exception, like we may want to redirect the user to a page where we give them option to log
//service request with error information or give them info to call us etc
function doexit()
{ exit();

}

?>